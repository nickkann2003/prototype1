package nick.nrk;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

public class AudioController {
    /**
     * Singleton class that handles all audio
     */
    private static Sound doorEnter = Gdx.audio.newSound(Gdx.files.internal("sounds/DoorEnter.wav"));
    private static boolean doorEnterPlayed = false;
    private static Sound iDoorSee = Gdx.audio.newSound(Gdx.files.internal("sounds/ItemDoorSee.wav"));
    private static Sound eDoorSee = Gdx.audio.newSound(Gdx.files.internal("sounds/EnemyDoorSee.wav"));
    private static Sound pDoorSee = Gdx.audio.newSound(Gdx.files.internal("sounds/PuzzleDoorSee.wav"));

    private static Sound playerHit = Gdx.audio.newSound(Gdx.files.internal("sounds/PlayerHit1.wav"));
    private static boolean playerHitPlayed = false;

    private static Sound enemyHit = Gdx.audio.newSound(Gdx.files.internal("sounds/EnemyHit.wav"));
    private static boolean enemyHitPlayed = false;

    private static Sound fire = Gdx.audio.newSound(Gdx.files.internal("sounds/Fire.wav"));
    private static boolean firePlayed = false;

    private static Sound ambience = Gdx.audio.newSound(Gdx.files.internal("sounds/Ambience.wav"));
    private static long ambienceID = ambience.play(0f);


    private static Sound bossDoorSee = Gdx.audio.newSound(Gdx.files.internal("sounds/BossDoorSee.wav"));
    private static long bID = bossDoorSee.play(0f);

    private static Sound reveal2 = Gdx.audio.newSound(Gdx.files.internal("sounds/reveal2.wav"));


    public AudioController() {

    }

    //Refresh function, used to reset all sound played variables
    public static void refresh() {
        doorEnterPlayed = false;
        enemyHitPlayed = false;
        firePlayed = false;
        playerHitPlayed = false;
    }

    //Enemy gets hit
    public static void playEnemyHit() {
        if (!enemyHitPlayed) {
            long id = enemyHit.play(0.75f);
            enemyHit.setPitch(id, 0.5f);
            enemyHitPlayed = true;
        } else {
            long id = enemyHit.play(0.03f);
            enemyHit.setPitch(id, 0.75f);
        }
    }

    public static void playEnemyHitWeak(){
        if (!enemyHitPlayed) {
            long id = enemyHit.play(0.75f);
            enemyHit.setPitch(id, 0.3f);
            enemyHitPlayed = true;
        } else {
            long id = enemyHit.play(0.03f);
            enemyHit.setPitch(id, 0.75f);
        }
    }

    public static void playEnemyHitStrong(){
        if (!enemyHitPlayed) {
            long id = enemyHit.play(0.75f);
            enemyHit.setPitch(id, 0.85f);
            enemyHitPlayed = true;
        } else {
            long id = enemyHit.play(0.03f);
            enemyHit.setPitch(id, 0.75f);
        }
    }

    //Player gets hit
    public static void playPlayerHit() {
        if (!playerHitPlayed) {
            long id = playerHit.play(0.6f);
            playerHit.setPitch(id, 0.6f);
            playerHitPlayed = true;
        } else {
            long id = playerHit.play(0.03f);
            playerHit.setPitch(id, 0.6f);
        }
    }

    //Weapon fired
    public static void playFire() {
        if (!firePlayed) {
            long id = fire.play(0.9f);

        } else {
            long id = fire.play(0.24f);
        }
    }

    //Enter a door
    public static void playDoorEnter() {
        if (!doorEnterPlayed) {
            long id = doorEnter.play(2.4f);
            doorEnter.setPitch(id, 0.35f);
            doorEnterPlayed = true;
        }
    }

    //"DoorSee" audio plays when close to a door and an enterable room is found in that direction
    public static void playItemDoorSee() {
        long id = iDoorSee.play(1.51f);
        iDoorSee.setPitch(id, 1f);
    }

    public static void playEnemyDoorSee() {
        long id = eDoorSee.play(1.5f);
        eDoorSee.setPitch(id, 1f);
    }

    public static void playPuzzleDoorSee() {
        long id = pDoorSee.play(1.5f);
        pDoorSee.setPitch(id, 1f);
    }

    public static void playBossDoorSee() {
        bID = bossDoorSee.play(1.5f);
        bossDoorSee.setLooping(bID, true);
    }

    public static void endBossDoorSee() { //Constant sound requires end queue when contact finishes
        bossDoorSee.setLooping(bID, false);
        bossDoorSee.setVolume(bID, 0);
    }

    public static void playAmbience(){
        ambienceID = ambience.play(7.5f);
        ambience.setPitch(ambienceID, 0.2f);
        ambience.setLooping(ambienceID, true);
    }

    public static void endAmbience(){
            ambience.setLooping(ambienceID, false);
            ambience.stop(ambienceID);
    }

    public static void playReveal2(){
        long id = reveal2.play();
    }

    public static void dispose() {
        doorEnter.dispose();
        iDoorSee.dispose();
        eDoorSee.dispose();
        pDoorSee.dispose();
        enemyHit.dispose();
        bossDoorSee.dispose();
        fire.dispose();
    }
}

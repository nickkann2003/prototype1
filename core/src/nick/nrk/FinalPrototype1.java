package nick.nrk;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Audio;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import nick.nrk.Entities.*;
import nick.nrk.Items.*;
import nick.nrk.Floor.Floor;
import nick.nrk.GSM.GameStateManager;
import nick.nrk.Rooms.BaseRoom;
import nick.nrk.Rooms.BossRoom;
import nick.nrk.Rooms.Room;
import nick.nrk.AudioController;

import java.util.ArrayList;

import static nick.nrk.utils.Constants.PPM;

public class FinalPrototype1 extends ApplicationAdapter {
	public static final float w = 750; //Width of screen
	public static final float h = 500; //Height of screen
	public static SpriteBatch batch; //Sprite Batch
	public static TextureAtlas atlas; //Sprite Batch
	public static World world; //Physics World

	public static BitmapFont font;

	public static GameStateManager gsm; //Game State Manager

	MyInputProcessor inputProcessor; //My custom input processor
	MyContactListener listener; //My custom contact listener

	@Override
	public void create () {
		//Set up basics
		world = new World(new Vector2(0,0), true); //New world
		atlas = new TextureAtlas("textures.atlas"); //Atlas
		batch = new SpriteBatch(); //Batch
		font = new BitmapFont(Gdx.files.internal("fonts/nov.fnt"));
		font.setColor(0.9f, 0.9f, 0.9f, 0.9f);

		//Give world to body factory
		BodyFactory.giveWorld(world); //Give factory the world

		//Create gsm
		gsm = new GameStateManager();

		gsm.start();

		inputProcessor = new MyInputProcessor(this); //Set input processor
		Gdx.input.setInputProcessor(inputProcessor); //Set base processor to mine

		listener = new MyContactListener(this); //Custom contact listener
		world.setContactListener(listener); //Set contact listener for world to my contact listener
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1); //Sets clear color to black
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); //Clears screen with black
		batch.begin();
		gsm.displayState();
		batch.end();
		AudioController.refresh();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		atlas.dispose();
		world.dispose();
		gsm.dispose();
		font.dispose();
		AudioController.dispose();
	} //Disposes of all assets, stopping memory leaks

	public static SpriteBatch getBatch(){ return batch; }
	public static TextureAtlas getAtlas(){ return atlas; }
	public static World getWorld(){return world;}
	public static BitmapFont getFont(){return font;}
	public Player getPlayer(){return gsm.getPlayer();}


	public static void enterRoom(String dir){
		int drc = getDirValue(dir);
		if(drc == -1){
			return;
		}
		gsm.enterRoom(drc);
	}

	public static void seeRoomOnMap(String dir){
		int drc = getDirValue(dir);
		if(drc == -1){
			return;
		}
		gsm.seeRoomOnMap(drc);
	}
	public static void endSeeRoomOnMap(){
		gsm.endSeeRoomOnMap();
	}

	public static void goMenu(){gsm.goMenu();}
	public static void goNextFloor(){
		gsm.goNextFloor();
	}
	public static void goTutorial(){gsm.goTutorial();}
	public static void endGame(int ending){gsm.endGame(ending);}
	public static void shakeUp(){
		gsm.shakeUp();
	}

	private static int getDirValue(String dir){
		if(dir.equals("UP"))
			return 0;
		if(dir.equals("LEFT"))
			return 1;
		if(dir.equals("DOWN"))
			return 2;
		if(dir.equals("RIGHT"))
			return 3;
		return -1;
	} //Turns DIR string into integer

	//Contact handlers
	public static void contactStartEntityEntity(Entity e1, Entity e2){
		EntityCollisionHandler.HandleCollision(e1, e2);
	}
	public static void contactStartEntityObject(Entity e1, Object objectData){
		if(e1 instanceof Player){

			//Check doors
			if (objectData.equals("UP"))
				enterRoom("UP");
			if (objectData.equals("LEFT"))
				enterRoom("LEFT");
			if (objectData.equals("DOWN"))
				enterRoom("DOWN");
			if (objectData.equals("RIGHT"))
				enterRoom("RIGHT");
			if(objectData.equals("NEXT"))
				goNextFloor();

			//Check see doors
			if (objectData.equals("SEEUP"))
				seeRoomOnMap("UP");
			if (objectData.equals("SEELEFT"))
				seeRoomOnMap("LEFT");
			if (objectData.equals("SEEDOWN"))
				seeRoomOnMap("DOWN");
			if (objectData.equals("SEERIGHT"))
				seeRoomOnMap("RIGHT");
		}

		if(e1 instanceof Projectile){
			if(objectData instanceof Vector2){
				EntityCollisionHandler.handleWallCollision((Projectile) e1, (Vector2) objectData);
			}
		}

		if(e1 instanceof BossSlime){
			EntityCollisionHandler.handleBossSlimeWallCollision((BossSlime)e1, (Vector2) objectData);
		}
	}
	public static void contactStartEntitySpawner(Entity e1, Spawner s){
		if(e1 instanceof Player){
			s.alert(e1);
		}
	}
	public static void contactStartPlayerItem(Player p1, Item item){
		EntityCollisionHandler.EntityItemCollision(p1, item);
	}
	public static void contactEndEntityEntity(Entity e1, Entity e2){
		gsm.collideEntities(e1, e2);
	}
	public static void contactEndEntityObject(Entity e1, Object objectData){
		if(e1 instanceof Player){
			//Check see doors
			if (objectData.equals("SEEUP"))
				endSeeRoomOnMap();
			if (objectData.equals("SEELEFT"))
				endSeeRoomOnMap();
			if (objectData.equals("SEEDOWN"))
				endSeeRoomOnMap();
			if (objectData.equals("SEERIGHT"))
				endSeeRoomOnMap();
			AudioController.endBossDoorSee();
		}
	}

	//Input handlers
	public void handleDownInput(int input){
		gsm.handleDownInput(input);
	}
	public void handleUpInput(int input){
		gsm.handleUpInput(input);
	}
	public void handleMouseInput(int x, int y){
		gsm.handleMouseInput(x,y);
	}
	public void handleMouseMotion(int x, int y){
		gsm.handleMouseMotion(x, y);
	}


	//Returns border
	public static Border getBorder(){
		return gsm.getBorder();
	}
}

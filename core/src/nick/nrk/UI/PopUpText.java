package nick.nrk.UI;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import nick.nrk.FinalPrototype1;
import nick.nrk.GSM.PlayState;
import nick.nrk.GSM.State;


/**
 * Pop up text class
 * Handles all pop up text for the tutorial/wherever else needed
 */

public class PopUpText {
    protected SpriteBatch batch;
    protected TextureAtlas atlas;
    protected State currentState;

    protected boolean showing;

    protected float yPos;

    protected BitmapFont f;

    protected String message;
    protected String mDisplayed;
    protected float timePerLetter = 0.05f;

    /*
    * Constructors
     */

    public PopUpText(){
        this.batch = FinalPrototype1.getBatch();
        this.atlas = FinalPrototype1.getAtlas();
        f = FinalPrototype1.getFont();
        showing = true;
    }

    public PopUpText(State state){
        this();
        this.currentState = state;
        if(state instanceof PlayState){
            if(((PlayState) state).getPlayer().getY() <= 250){
                yPos = 1;
            }else{
                yPos = 0;
            }
        }else{
            yPos = 1.5f;
        }
    }

    public PopUpText(String text){
        this();
        this.message = text;
        this.mDisplayed = "";
    }

    public PopUpText(State state, String text){
        this(state);
        this.message = text;
        this.mDisplayed = "";
    }



    /*
    * Main Methods
     */

    public void display(){
        if(showing) {
            batch.setColor(1f, 1f, 1f, 0.4f);
            batch.draw(atlas.findRegion("UI/popUpBox"), 75, 80 + 240 * yPos, 350, 100);
            batch.setColor(Color.WHITE);
            f.draw(batch, mDisplayed, 90, 80 + 85 + 240 * yPos, 320, 8, true);
        }
    }

    public void update(float deltaTime){
        if(showing){
            if(!message.isEmpty()) {
               timePerLetter -= deltaTime;
              if (timePerLetter < 0) {
                  String c = message.substring(0, 1);
                  message = message.substring(1);
                  mDisplayed = mDisplayed + c;
                  timePerLetter = 0.0465f;
                  if(c.equals(".")){
                      timePerLetter = 0.45f;
                  }
                  if(c.equals(".")){
                      timePerLetter = 0.25f;
                  }
                }
            }
            if(!mDisplayed.isEmpty()){
                char[] charArray = mDisplayed.toCharArray();
                for(int i = 0; i < charArray.length; i ++){
                    if(MathUtils.random(100f) < 0.15f){
                        charArray[i] = swapCharAppearance(charArray[i]);
                    }
                }
                mDisplayed = new String(charArray);
            }
        }
    }

    public void adjustPosition(int drc){
        if(drc == 0 || drc == 2){
            yPos = (yPos+1)%2;
        }
    }

    public void reveal(){
        showing = true;
    }

    public void hide(){
        showing = false;
    }

    public void rePurpose(String newMessage){
        showing = false;
        message = newMessage;
        mDisplayed = "";
    }

    protected char swapCharAppearance(char c){
        char newChar = c;
        if(c == 'E'){
            newChar = '3';
        }
        if(c == '3'){
            newChar = 'E';
        }

        if(c == 'S'){
            newChar = '5';
        }
        if(c == '5'){
            newChar = 'S';
        }

        if(c == 'I'){
            newChar = '1';
        }
        if(c == '1'){
            newChar = 'I';
        }

        if(c == 'O'){
            newChar = '0';
        }
        if(c == '0'){
            newChar = 'O';
        }

        if(c == 'T'){
            newChar = '7';
        }
        if(c == '7'){
            newChar = 'T';
        }

        if(c == 'G'){
            newChar = '6';
        }
        if(c == '6'){
            newChar = 'G';
        }

        return newChar;
    }
}

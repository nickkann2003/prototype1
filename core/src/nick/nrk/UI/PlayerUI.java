package nick.nrk.UI;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import nick.nrk.*;
import nick.nrk.Entities.Player;

public class PlayerUI {
    protected SpriteBatch batch;
    protected TextureAtlas atlas;
    Player p;
    protected Texture healthBar;
    protected Sprite hp;
    protected float n;
    protected BitmapFont f;

    protected boolean hideShakeUpCounter;
    protected int shakeUpCounter;

    public PlayerUI(Player p){
        this.atlas = FinalPrototype1.getAtlas();
        this.batch = FinalPrototype1.getBatch();
        this.f = FinalPrototype1.getFont();
        this.p = p;
        healthBar = atlas.findRegion("UI/healthBar").getTexture();
        hp = new Sprite(atlas.findRegion("UI/healthBar"));
        n = (p.getHealth()/p.getMaxHealth());
        shakeUpCounter = 5;
        hideShakeUpCounter = false;
    }
    public void displayUI(){
        batch.draw(atlas.findRegion("UI/inventoryShell"), 500, 0, 250, 500);
        n = p.getHealth()/p.getMaxHealth();
        batch.setColor(0.6f,0.2f,0.1f,0.5f);
        batch.draw(atlas.findRegion("UI/healthBar"), 514, 461);
        batch.setColor(Color.WHITE);

        if(hideShakeUpCounter) {

        } else {
            f.draw(batch, "Shake-Up: " + p.getShakeUpCountdown(), 520, 430);
        }

        //HEALTH BAR
        if(n > 0 && n <= 1){
            batch.setColor(0.05f, 0.85f,0.15f, 1f);
            batch.draw(
                    atlas.findRegion("UI/healthBar").getTexture(),
                    514,
                    461,
                    0,
                    0,
                    224f*n,
                    10,
                    1,
                    1,
                    0,
                    atlas.findRegion("UI/healthBar").getRegionX(),
                    atlas.findRegion("UI/healthBar").getRegionY(),
                    (int)(((float)atlas.findRegion("UI/healthBar").getRegionWidth()) * (float)n),
                    atlas.findRegion("UI/healthBar").getRegionHeight(),
                    false,
                    false
                    );
            batch.setColor(Color.WHITE);
        }else if(n > 1 && n <= 2){
            batch.setColor(0.05f, 0.85f,0.15f, 1f);
            batch.draw(atlas.findRegion("UI/healthBar"), 514, 461);
            batch.setColor(Color.GOLD);
            batch.draw(
                    atlas.findRegion("UI/healthBar").getTexture(),
                    514,
                    461,
                    0,
                    0,
                    224f*(n-1),
                    10,
                    1,
                    1,
                    0,
                    atlas.findRegion("UI/healthBar").getRegionX(),
                    atlas.findRegion("UI/healthBar").getRegionY(),
                    (int)(((float)atlas.findRegion("UI/healthBar").getRegionWidth()) * (float)(n-1)),
                    atlas.findRegion("UI/healthBar").getRegionHeight(),
                    false,
                    false
                    );
            batch.setColor(Color.WHITE);
        }
    }

    public boolean checkHideShakeUpCounter(){
        return hideShakeUpCounter;
    }

    public void setHideShakeUpCounter(boolean set){
        hideShakeUpCounter = set;
    }


}

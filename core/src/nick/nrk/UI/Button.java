package nick.nrk.UI;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import nick.nrk.FinalPrototype1;

/**
 * Button class
 * Used for all menu buttons
 */
public class Button {
    float x; //X representing center of button
    float y; //Y representing center of button
    float w; //Width of the button
    float h; //Height of the button

    boolean hovered; //Checks if the button is being hovered over

    boolean playButton; //Specific boolean to see if this button is a play button

    TextureAtlas.AtlasRegion region; //Region within the atlas that will be displayed as the button

    SpriteBatch batch;
    TextureAtlas atlas;

    public Button(float x, float y, TextureAtlas.AtlasRegion region, float width, float height, boolean playButton){
        this.x = x;
        this.y = y;

        this.playButton = playButton;

        this.w = width;
        this.h = height;

        this.region = region;

        batch = FinalPrototype1.getBatch();
        atlas = FinalPrototype1.getAtlas();
    }

    public void display(){
        if(hovered) {
            batch.draw(region, x - w / 2, y - h / 2, w, h);
            if(playButton)
                batch.draw(atlas.findRegions("player/player_standing").get(0), x+w/2*1.1f, y-h/4);
        }else{
            batch.draw(region, x - (w/1.1f) / 2, y - (h/1.1f) / 2, w/1.1f, h/1.1f);
            if(playButton)
                batch.draw(atlas.findRegion("player/player_sitting_right"), x+w/2*1.1f, y-h/4);
        }
    }

    public boolean checkHovered(float mX, float mY){
        mY = 500-mY; //X and Y are given as coords with a top left 0,0 scheme
        mX = 750 - mX; //While libGDX functions with a bottom left 0,0 system
        //Therefor this conversion is necessary to check hovered buttons
        if(mX > x-w/2 && mX < x+w/2 && mY > y-h/2 && mY < y+h/2){
            hovered = true;
            return true;
        }else{
            hovered = false;
            return false;
        }
    }
}

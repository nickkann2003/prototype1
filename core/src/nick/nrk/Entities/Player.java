package nick.nrk.Entities;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Actor;
import nick.nrk.AudioController;
import nick.nrk.FinalPrototype1;
import nick.nrk.Items.*;
import nick.nrk.Rooms.*;
import nick.nrk.utils.Constants;
import nick.nrk.UI.*;

import java.util.ArrayList;

import static com.badlogic.gdx.math.MathUtils.cos;
import static com.badlogic.gdx.math.MathUtils.sin;
import static java.lang.Math.PI;
import static nick.nrk.utils.Constants.*;

public class Player extends Entity {
    private Room currentRoom; //Reference to current room

    private float timer;

    //Room location variables
    protected float rx;
    protected float ry;

    //Room shake up counter
    protected int shakeUpCounter;

    //Firing variables
    protected float shotCD;
    protected float attackSpeed;
    protected int shotNum;
    protected boolean firing;
    protected int fireDirection;
    protected int bounceNum;
    protected float projectileHealth;
    protected int sizeIncrease; //Amount size will increase on next frame
    protected float projectileRange; //Range time of projectile

    //Player UI
    protected PlayerUI pUI;

    //Player inventory of items
    protected Inventory inventory;

    //Player's type handler
    protected PlayerTypeHandler tHandler;

    public Player(World world){
        super(world); //Supers constructor
        width = 16; //Width
        height = 16; //Height
        baseSpeed = 70; //Base 70ish
        speed = baseSpeed + 15; //Set speed
        maxHealth = 10; //Base 10
        health = maxHealth;

        active = true;

        type = Type.BAS; //Start as basic type

        kbResist = 1;

        //Set X/Y
        x = 50;
        y = 50;

        //Firing variables
        damage = 0.8f; //Base 0.8f
        shotNum = 1; //Base 1
        attackSpeed = 2.2f; //Base 2.2
        shotCD = 1/attackSpeed;
        bounceNum = 0;
        projectileSpeed = 140f;
        projectileRadius = 3;
        projectileHealth = 1;
        projectileRange = 0.65f;

        //Creates the player body
        createPlayerBody();

        //Set all animations for movement directions
        directionalAnimations[0] = new Animation(16/speed, atlas.findRegions("player/player_walking_up"));
        directionalAnimations[1] = new Animation(8/speed, atlas.findRegions("player/player_walking_left"));
        directionalAnimations[2] = new Animation(16/speed, atlas.findRegions("player/player_walking_down"));
        directionalAnimations[3] = new Animation(8/speed, atlas.findRegions("player/player_walking_right"));
        directionalAnimations[4] = new Animation(32/speed, atlas.findRegions("player/player_standing"));

        //Set base animation to right-walking animation
        animation = directionalAnimations[3];

        //Type Handler
        tHandler = new PlayerTypeHandler(this);

        //Inventory
        inventory = new Inventory();

        //UI
        pUI = new PlayerUI(this);

        //Shake up counter
        shakeUpCounter = 6;

        setHideShakeUpCounter(true);
    }

    @Override
    public void display() { //Draws player
        batch.setColor(type.getColor());
        if(knocked) {
            batch.setColor(1f, 0.8f, 0.8f, 0.7f);
        }else if(immune()){
            batch.setColor(0.5f,0.5f,0.5f,0.5f + 2.5f * MathUtils.random(1));
        }
        batch.draw((TextureRegion) animation.getKeyFrame(passedTime, true), x-width/2,y-height/2, width, height);
        batch.setColor(Color.WHITE);

        pUI.displayUI();
        inventory.display();
        tHandler.display();
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        applyForces();
        tHandler.update(deltaTime);
        if(firing){
            fire(fireDirection);
        }
        shotCD -= deltaTime;
    }

    private void updateStats(){ //Updates all player stats based on current items
        setFrameDurations();
    }

    //Creation functions
    private void createPlayerBody(){
        //Create body
        BodyDef bodyDef = new BodyDef(); //Def
        bodyDef.type = BodyDef.BodyType.DynamicBody; //Dynamic
        bodyDef.position.set(x/PPM, y/PPM); //Set pos
        body = world.createBody(bodyDef); //Set body with bodyDef
        body.setUserData(this); //Pass the player itself as user data for the Box2D object
        body.setFixedRotation(true);
        CircleShape shape = new CircleShape(); //Shape of fixture
        shape.setRadius(width/2/PPM); //Set shape
        FixtureDef fixtureDef = new FixtureDef(); //New def
        fixtureDef.shape = shape; //Set def shape
        fixtureDef.density = 1f; //Set def density
        fixtureDef.filter.categoryBits = Constants.CATEGORY_PLAYER;
        fixtureDef.filter.maskBits = Constants.MASK_PLAYER;
        Fixture fixture = body.createFixture(fixtureDef); //Create with def
        shape.dispose(); //Dispose of used object
    } //Creates the player body

    private void createPlayerBody(float w){
        BodyDef bodyDef = new BodyDef(); //Def
        bodyDef.type = BodyDef.BodyType.DynamicBody; //Dynamic
        bodyDef.position.set(x/PPM, y/PPM); //Set pos
        body = world.createBody(bodyDef); //Set body with bodyDef
        body.setUserData(this); //Pass the player itself as user data for the Box2D object
        body.setFixedRotation(true);
        CircleShape shape = new CircleShape(); //Shape of fixture
        shape.setRadius(w/2/PPM); //Set shape
        FixtureDef fixtureDef = new FixtureDef(); //New def
        fixtureDef.shape = shape; //Set def shape
        fixtureDef.density = 1f; //Set def density
        fixtureDef.filter.categoryBits = Constants.CATEGORY_PLAYER;
        fixtureDef.filter.maskBits = Constants.MASK_PLAYER;
        Fixture fixture = body.createFixture(fixtureDef); //Create with def
        shape.dispose(); //Dispose of used object
    }

    //Getters
    public Body getBody(){
        return body;
    }

    //Setters
    public void enterRoom(int RoomX, int RoomY, int dir){
        rx = RoomX*250;
        ry = 250-(RoomY*250);
        int dr = (dir+2)%4; //Flip door variable
        if(dr == 0)
            body.setTransform(body.getPosition().x, (RoomY*250+241)/PPM, 0);
        if(dr == 1)
            body.setTransform((RoomX*250+9)/PPM, body.getPosition().y, 0);
        if(dr == 2)
            body.setTransform(body.getPosition().x, (RoomY*250+9)/PPM, 0);
        if(dr == 3)
            body.setTransform((RoomX*250+241)/PPM, body.getPosition().y, 0);
        applyForces(); //Reapply forces, since they reset upon transformation

        if(!pUI.checkHideShakeUpCounter()) {
            shakeUpCounter -= 1;
            if (shakeUpCounter < 0) {
                FinalPrototype1.shakeUp();
                shakeUpCounter = 6;
            }
        }
    } //Sets the position of the player based on the room they are entering
    public void enterBossRoom(BossRoom r){
        int dr = r.getEntrance();
        if(dr/2 == 0) //Top edge
            body.setTransform((500f/4 + 250*(dr%2))/PPM, (500f - 9)/PPM, 0);
        if(dr/2 == 1) //Left edge
            body.setTransform((9f)/PPM, (500f*3f/4f - 250*(dr%2))/PPM, 0);
        if(dr/2 == 2) //Bottom edge
            body.setTransform((500f/4 + 250*(dr%2))/PPM, (9f)/PPM, 0);
        if(dr/2 == 3) //Right edge
            body.setTransform((500f - 9f)/PPM, (500f*3f/4f - 250*(dr%2))/PPM, 0);
        applyForces();
    }
    public void enterFloor(){
        body.setTransform(125/PPM, 9/PPM, 0);
        setHideShakeUpCounter(false);
        shakeUpCounter = 6;
    } //Function run when player enters a new floor

    //Input handler functions
    public void handleDownInput(int input){
        //MOVEMENT KEYS
        if(input == Input.Keys.D)
            move[3] = true;
        if(input == Input.Keys.S)
            move[2] = true;
        if(input == Input.Keys.A)
            move[1] = true;
        if(input == Input.Keys.W)
            move[0] = true;
        if(input == Input.Keys.Q){
            tHandler.hoverPrev();
        }
        if(input == Input.Keys.E){
            tHandler.hoverNext();
        }
        //SPCAEBAR
        if(input == Input.Keys.SPACE){
            tHandler.hoverNext();
            tHandler.hoverPrev();
            if(shotCD <= 0) {
                tHandler.lockIn();
                type = tHandler.getType();
                fireBurstInMoveDirection(20 + 5f*shotNum);
                shotCD = (1 / attackSpeed) * 3;
                if (type.equals(Type.BAS)) {
                    speed = baseSpeed + 15;
                } else {
                    speed = baseSpeed;
                }
                determineAnimation();
            }
        }

        //ARROW KEYS
        if(input == Input.Keys.UP){
            firing = true;
            fireDirection = 0;
        }
        if(input == Input.Keys.LEFT){
            firing = true;
            fireDirection = 1;
        }
        if(input == Input.Keys.DOWN){
            firing = true;
            fireDirection = 2;
        }
        if(input == Input.Keys.RIGHT){
            firing = true;
            fireDirection = 3;
        }
        determineAnimation();
        applyForces();
    } //Sets movement variables based on Down input
    public void handleUpInput(int input){
        //MOVEMENT KEYS
        if(input == Input.Keys.D)
            move[3] = false;
        if(input == Input.Keys.S)
            move[2] = false;
        if(input == Input.Keys.A)
            move[1] = false;
        if(input == Input.Keys.W)
            move[0] = false;

        //ARROW KEYS
        if(input == Input.Keys.UP && fireDirection == 0){
            firing = false;
        }
        if(input == Input.Keys.LEFT && fireDirection == 1){
            firing = false;
        }
        if(input == Input.Keys.DOWN && fireDirection == 2){
            firing = false;
        }
        if(input == Input.Keys.RIGHT && fireDirection == 3){
            firing = false;
        }

        determineAnimation();
        applyForces();
    } //Sets movement variables based on Up input

    public void handleMouseInput(float x, float y){

    }

    private void applyForces(){
        float xms = 0; //X movespeed
        float yms = 0; //Y movespeed
        if(move[0])
            yms += speed/PPM;
        if(move[1])
            xms -= speed/PPM;
        if(move[2])
            yms -= speed/PPM;
        if(move[3])
            xms += speed/PPM;
        //Account for knockback
        float kx = remainingKnockback.x; //Knockback X
        float ky = remainingKnockback.y; //Knockback Y

        if(kx + ky != 0) {
            if (kx * xms > 0) { //If same direction
                xms = xms / 2 + kx / PPM; //Only apply knockback
            } else { //If different directions
                xms = xms / 3f + kx / PPM; //Apply both
            }
            if (ky * yms > 0) { //Same dir
                yms = yms / 2 + ky / PPM; //Only knockback
            } else { //Different dir
                yms = yms / 3f + ky / PPM; //Apply both
            }
        }

        body.setLinearVelocity(xms, yms);
    } //Handles the application of force to the player body

    public void freeze(){
        for(int i = 0; i < move.length; i ++){
            move[i] = false;
        }
        firing = false;
        applyForces();
    } //Freezes the player

    @Override
    public void applyDamage(float damage){
        super.applyDamage(damage);
        Filter f = body.getFixtureList().get(0).getFilterData();
        f.categoryBits = Constants.CATEGORY_IMMUNE;
        f.maskBits = Constants.MASK_IMMUNE;
        body.getFixtureList().get(0).setFilterData(f);
        immunity = (float)Math.sqrt(damage)/4f + 1.5f;
        if(health < 0 ){
            FinalPrototype1.endGame(0);
        }
    }

    public void resetFilter(){
        Filter f = body.getFixtureList().get(0).getFilterData();
        if(f.categoryBits != Constants.CATEGORY_PLAYER) {
            f.categoryBits = Constants.CATEGORY_PLAYER;
            f.maskBits = Constants.MASK_PLAYER;
            body.getFixtureList().get(0).setFilterData(f);
        }
    }

    public void giveItem(Item item){
        inventory.giveItem(item);
    }

    public void increaseBounceNum(int num){bounceNum += num;}
    public void increaseShotNum(int num){shotNum += num;}
    public void increaseProjectileHealth(float health){projectileHealth += health;}
    public void increaseShotRate(float shotRate){this.attackSpeed += shotRate;}
    public void increaseProjectileRange(float range){this.projectileRange += range;}

    public void setHideShakeUpCounter(boolean set){
        pUI.setHideShakeUpCounter(set);
    }

    public float getShotRate(){return attackSpeed;}
    public int getShakeUpCountdown(){return shakeUpCounter;}

    @Override
    public void increaseSize(float size){

    }

    public Inventory getInventory(){
        return inventory;
    }

    //Animation handler functions
    private void determineAnimation(){
        if(move[1] && !move[3]){
            animation = directionalAnimations[1];
        }else if(!move[1] && move[3]){
            animation = directionalAnimations[3];
        }else if(move[1] && move[3]){
            if(move[0] && !move[2]) {
                animation = directionalAnimations[0];
            }else if(!move[0] && move[2]){
                animation = directionalAnimations[2];
            }else{
                animation = directionalAnimations[4];
            }
        }else{
            if(move[0] && !move[2]) {
                animation = directionalAnimations[0];
            }else if(!move[0] && move[2]){
                animation = directionalAnimations[2];
            }else{
                animation = directionalAnimations[4];
            }
        }
    }//Determines current animation state based on movement booleans
    private void setFrameDurations(){
        directionalAnimations[0].setFrameDuration(16/speed);
        directionalAnimations[1].setFrameDuration(8/speed);
        directionalAnimations[2].setFrameDuration(16/speed);
        directionalAnimations[3].setFrameDuration(8/speed);
        directionalAnimations[4].setFrameDuration(32/speed);
    } //Sets the frame durations for all animations

    protected void fire(int dir){
        float theta = 0;
        if(dir == 0){
            theta = (float) PI/2;
        }else if(dir == 1){
            theta = (float) PI;
        }else if(dir == 2){
            theta = (float) (3*PI/2);
        }else if(dir == 3){
            theta = (float) (2*PI);
        }
        if(shotCD <= 0){
            for (int i = 0; i < shotNum; i++) {
                float xAdj = 0;
                float yAdj = 0;
                //Adjust angle based on movement
                if(move[0])
                    yAdj += 0.35;
                if(move[1])
                    xAdj -= 0.35;
                if(move[2])
                    yAdj -= 0.35;
                if(move[3])
                    xAdj += 0.35;
                if(shotNum > 1) {
                    if(shotNum%2 == 0){
                        theta += 0.025;
                        Projectile p = new Projectile(this, new Vector2(cos(theta - 0.09f * shotNum / 2 + (0.09f * i)) + xAdj, sin(theta - 0.09f * shotNum / 2 + (0.09f * i)) + yAdj), this.damage, projectileRadius, projectileSpeed, bounceNum, projectileHealth, projectileRange);
                    }else {
                        theta += 0.025;
                        Projectile p = new Projectile(this, new Vector2(cos(theta - 0.09f * shotNum / 2 + (0.09f * i)) + xAdj, sin(theta - 0.09f * shotNum / 2 + (0.09f * i)) + yAdj), this.damage, projectileRadius, projectileSpeed, bounceNum, projectileHealth, projectileRange);
                    }
                }else{
                    Projectile p = new Projectile(this, new Vector2(cos(theta) + xAdj, sin(theta) + yAdj), this.damage, projectileRadius, projectileSpeed, bounceNum, projectileHealth, projectileRange);
                }
            }
            AudioController.playFire();
            shotCD = 1/attackSpeed;
        }
    }

    protected void fireBurstInMoveDirection(float numShots){
        for(int i = 0; i < numShots; i ++) {
            float theta = (float) (2f * PI) * (i / (numShots)) + (float)MathUtils.random((float)-PI/10f, (float)PI/10f);
            Projectile p = new Projectile(this, new Vector2(cos(theta), sin(theta)), this.damage/5f, (float)MathUtils.random(3.5f, 6f), (float)MathUtils.random(80f, 140f), bounceNum, (float)MathUtils.random(1.25f, 2.15f), MathUtils.random(-2f, 1f));
        }
    }

    public boolean canKnockToNext(){
        if(remainingKnockback.len() >= 100){
            return true;
        }
        return false;
    }
}

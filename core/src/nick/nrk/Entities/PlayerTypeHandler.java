package nick.nrk.Entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import nick.nrk.FinalPrototype1;

public class PlayerTypeHandler {
    /*
    * Type handler class for player
    * Handles are player control over type changing,
    * As well as visuals to accompany it
     */

    private SpriteBatch batch;
    private TextureAtlas atlas;

    private Type type;
    private Type hover;
    private boolean hovering;
    private Player p;
    private float hTimer;

    public PlayerTypeHandler(Player p){
        this.batch = FinalPrototype1.getBatch();
        this.atlas = FinalPrototype1.getAtlas();
        type = Type.BAS;
        hover = Type.BAS;
        this.p = p;
    }

    public void display(){
        if(hovering){
            //Display hovered type and left/right types
            batch.setColor(hover.getNext().getColor());
            batch.draw(atlas.findRegion("player/typeIndicator"), p.getX()+10, p.getY() -4);
            batch.setColor(hover.getColor());
            batch.draw(atlas.findRegion("player/typeIndicator"), p.getX()-4, p.getY() + 10);
            batch.setColor(hover.getPrev().getColor());
            batch.draw(atlas.findRegion("player/typeIndicator"), p.getX()-18, p.getY() - 4);
            batch.setColor(Color.WHITE);
        }else{
            hover = type;
        }
    }

    public void update(float deltaTime){
        hTimer -= deltaTime;
        if(hTimer <= 0 ){
            hovering = false;
        }
    }

    public void hoverNext(){
        hover = hover.getNext();
        hovering = true;
        hTimer = 2f;
    }

    public void hoverPrev(){
        hover = hover.getPrev();
        hovering = true;
        hTimer = 2f;
    }

    public void lockIn(){
        hovering = false;
        type = hover;
    }

    public Type getType(){
        return type;
    }
}

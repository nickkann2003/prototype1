package nick.nrk.Entities;
/*
 * Abstract Entity class
 * Blueprint for all existing entities within the world
 * Use Box2D physics
 */

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import nick.nrk.FinalPrototype1;

import java.util.LinkedList;

import static nick.nrk.utils.Constants.KBFriction;
import static nick.nrk.utils.Constants.PPM;

public class Entity {
    //Every entity needs a reference to the world it is in
    World world;
    protected TextureAtlas atlas;
    protected SpriteBatch batch;

    protected LinkedList<Entity> contact;

    //Every Entity needs its own position
    float x; //X position of entity, centered
    float y; //Y position of entity, centered
    boolean[] move = new boolean[4]; //Array of booleans representing movement in 4 cardinal directions
    Vector2 movement; //movement vector for AI
    float speed; //Movement speed of entity
    float baseSpeed; //Base speed of entity

    //Every Entity needs to cycle through its animation
    Animation animation;
    Animation[] directionalAnimations = new Animation[5];
    protected float passedTime = 0; //Time passed in game, used for Animations (s)

    //Every entity needs its own dimensions
    float width; //Width of sprite
    float height; //Height of sprite

    //Variables for taking collision
    float maxHealth; //Entity max health
    float health; //Entity current health
    float kbResist; //Entity knockback resistance (Float 0-1);
    float immunity; //Immunity time after being hit
    float range; //Range for AI entities
    boolean knocked; //Boolean, if entity has been knocked back
    boolean hitThisFrame; //Boolean to keep track if entity was already hit this frame
    boolean deathFlag; //If entity is flagged for death during world update frame
    boolean active = false;

    //Collision holders
    Vector2 remainingKnockback; //Vector holding remaining knockback

    //Variables for dealing collision
    float damage; //Damage dealt by entity
    protected float projectileSpeed;
    protected float projectileRadius;
    Type type; //Damage type

    //Every entity needs its own Box2D collider
    Body body;

    //Subclasses can call super(batch, atlas)
    public Entity(World world){
        this.world = world; //set world
        remainingKnockback = new Vector2(0, 0);
        deathFlag = false;
        atlas = FinalPrototype1.getAtlas();
        batch = FinalPrototype1.getBatch();
        movement = new Vector2(0,0);
        projectileSpeed = 50;
        contact = new LinkedList<>();
    }

    //Every entity needs to display itself
    public void display(){
        if(knocked)
            batch.setColor(0.8f, 0.8f, 0.8f, 0.9f);
        batch.draw((TextureRegion)animation.getKeyFrame(passedTime, true), x-(width+2)/2, y-(height+2)/2, width+2, height+2);
        batch.setColor(Color.WHITE);
    } //Displays entity

    //Every entity needs to update itself
    public void update(float deltaTime){
        passedTime += deltaTime; //Increase passed time
        hitThisFrame = false;
        contact.clear();

        x = body.getPosition().x*PPM; //X of player image
        y = body.getPosition().y*PPM; //Y of player image

        float kx = remainingKnockback.x; //Knockback X
        float ky = remainingKnockback.y; //Knockback Y

        //Apply friction to knockback
        if(Math.abs(kx) < KBFriction*deltaTime){
            kx = 0;
        }else{
            if(kx < 0)
                kx += KBFriction*deltaTime;
            if(kx > 0)
                kx -= KBFriction*deltaTime;
        }
        if(Math.abs(ky) < KBFriction*deltaTime){
            ky = 0;
        }else{
            if(ky < 0)
                ky += KBFriction*deltaTime;
            if(ky > 0)
                ky -= KBFriction*deltaTime;
        }
        if(kx == 0 && ky == 0){
            knocked = false;
        }
        remainingKnockback.set(kx, ky);

        //Apply movement vector to AIs
        float xms = 0;
        float yms = 0;
        if(movement != null){
            xms = movement.x;
            yms = movement.y;
            if(knocked){
                xms = xms/4;
                yms = yms/4;
            }
        }

        //Set velocity
        body.setLinearVelocity((xms + remainingKnockback.x)/PPM, (yms + remainingKnockback.y)/PPM);

        //Decrement immunity
        if(immunity > 0){
            immunity -= deltaTime;
        }
        if(immunity <= 0){
            resetFilter();
        }
    } //Updates entity with given time


    //***Collision handling functions***
    public void applyKnockback(Vector2 kb){
        if(immunity <= 0) {
            if (!knocked) {
                remainingKnockback.add(kb.scl(kbResist));
            } else {
                remainingKnockback.add(kb.scl(kbResist).scl(0.65f));
            }
            knocked = true;
        }
        hitThisFrame = true;
    }
    public void applyDamage(float damage){
        health -= damage;
        hitThisFrame = true;
        immunity = (float)Math.sqrt(damage)/4f;
        if(health <= 0){
            deathFlag = true;
        }
    }
    public void contact(Entity e){
        contact.add(e);
    }
    public boolean hasContact(Entity e){
        if(contact.contains(e)){
            return true;
        }else{
            return false;
        }
    }

    public void destroy(){

    }

    //***GETTERS AND SETTERS***
    public float getX(){
        return x;
    }
    public float getY(){
        return y;
    }
    public float getSpeed(){return speed;}
    public float getProjectileSpeed(){return projectileSpeed;}
    public float getProjectileRadius(){return projectileRadius;}
    public float getDamage(){
        return damage;
    }
    public float getRange(){return range;}
    public float getWidth(){return width;}
    public float getHeight(){return height;}
    public float getHealth(){return health;}
    public float getMaxHealth(){return maxHealth;}
    public Vector2 getPos(){return new Vector2(x, y);}
    public Type getType(){
        return type;
    }
    public Body getBody(){return body;}

    public void setSpeed(float s){ this.speed = s;}
    public void setDamage(float d){ this.damage = d;}
    public void setRange(float r){this.range = r;}
    public void setType(Type t){this.type = t;}
    public void setWidth(float w){this.width = w;}
    public void setHeight(float h){this.height = h;}

    public void increaseSpeed(float s){this.baseSpeed += s;}
    public void increaseDamage(float d){this.damage += d;}
    public void increaseMaxHealth(float h){this.maxHealth += h; this.health += h;}
    public void increaseHealth(float h){
        if(this.health + h > maxHealth){
            float newH = h-(maxHealth-health);
            this.health = maxHealth;
            health += newH*0.5f;
        }else {
            this.health += h;
        }
    }
    public void increaseRange(float r){this.range += r;}
    public void increaseSize(float size){this.width += size; this.height += size;}
    public void increaseProjectileSpeed(float s){this.projectileSpeed += s;}
    public void increaseProjectileSize(float r){this.projectileRadius += r;}{}

    public Entity getTarget(){return null;}

    public boolean immune(){
        if(immunity > 0){
            return true;
        }
        return false;
    }

    public boolean isDeathFlagged(){
        return deathFlag;
    }
    public boolean isHitThisFrame(){return hitThisFrame;}
    public boolean isKnocked(){return knocked;}

    public boolean active(){return active;}

    public void resetFilter(){}
}
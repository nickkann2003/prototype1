package nick.nrk.Entities.BossBehaviors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import nick.nrk.Entities.Behaviors.AIBehavior;
import nick.nrk.Entities.Entity;
import nick.nrk.Entities.Player;
import nick.nrk.Entities.Projectile;
import nick.nrk.Entities.Type;
import nick.nrk.FinalPrototype1;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static java.lang.StrictMath.*;

public class SlimeBoss extends AIBehavior{
    protected Entity me;
    protected Entity target;

    protected boolean flipped;

    protected TextureAtlas atlas;
    protected SpriteBatch batch;
    protected Vector2 motion;
    //All behavior variables
    protected float idleTime; //Time idling
    protected Animation idleAnim;
    protected float attackType; //Attack type selected when not idling
    protected float changeTime; //Time left for changing animation
    protected boolean changedYet;
    protected Animation changeAnim;
    protected Type nextType;
    protected Color myColor;
    protected float dashChargeTime; //Time left to charge the dash
    protected Animation dashChargeAnim;
    protected float dashIdleTime; //Time left idling before charge
    protected Animation dashIdleAnim;
    protected float dashTime; //Time left for dashing
    protected Animation dashAnim;
    protected Vector2 angleV;
    protected float shootChargeTime; //Time left charging shoot
    protected Animation shootChargeAnim;
    protected float shootTime; //Time left for shooting
    protected float shootWaveCD;
    protected int waveAlternator;
    protected Animation shootAnim;

    public SlimeBoss(){
        motion = new Vector2(0,0);
        idleTime = 2.5f;
        attackType = -1;
        this.atlas = FinalPrototype1.getAtlas();
        this.batch = FinalPrototype1.getBatch();
        //Custom Animation made of multiple boss animation samples
        ArrayList<TextureRegion> idleAnimArray = new ArrayList<TextureRegion>();
        idleAnimArray.add(atlas.findRegion("boss/slime/bossSlimeBase"));
        idleAnimArray.add(atlas.findRegions("boss/slime/bossSlimeDashCharge").get(0));
        idleAnim = new Animation(1f/3.5f, idleAnimArray.toArray());
        changeAnim = new Animation(1f/5f, atlas.findRegions("boss/slime/bossSlimeChange"));
        dashChargeAnim = new Animation(1f/2.5f, atlas.findRegions("boss/slime/bossSlimeDashCharge"));
        dashIdleAnim = new Animation(1f/6f, atlas.findRegions("boss/slime/bossSlimeDashChargeIdle"));
        dashAnim = new Animation(1f/8f, atlas.findRegions("boss/slime/bossSlimeDash"));
        shootChargeAnim = new Animation(1f/2.5f, atlas.findRegions("boss/slime/bossSlimeShootCharge"));
        shootAnim = new Animation(1f/32f, atlas.findRegions("boss/slime/bossSlimeShoot"));
        shootWaveCD = 0.15f;
        waveAlternator = 1;
    }

    public SlimeBoss(Entity me){
        this();
        this.me = me;
    }

    public SlimeBoss(Entity me, Player p){
        this(me);
        target = p;
    }

    public void display(){

    }

    public void update(float deltaTime){
        idleTime -= deltaTime;
        changeTime -= deltaTime;
        dashChargeTime -= deltaTime;
        dashIdleTime -= deltaTime;
        dashTime -= deltaTime;
        shootChargeTime -= deltaTime;
        shootTime -= deltaTime;

        //TODO: Figure out why this isnt working

        if(target != null) {
            if (changeTime < 0) {
                myColor = me.getType().getColor();
            }

            if (idleTime <= 0) {
                if (attackType == -1) {
                    attackType = MathUtils.random(2);
                    if (!changedYet) {
                        attackType = 0;
                        changedYet = true;
                    }
                    if (attackType == 0) {
                        changeTime = 2f;
                        nextType = me.getType().getRandom(me.getType());
                    }
                    if (attackType == 1) {
                        dashChargeTime = 1f;
                        dashIdleTime = 2f;
                        dashTime = 5f;
                    }
                    if (attackType == 2) {
                        shootChargeTime = 1.5f;
                        shootTime = 4f;
                    }
                }
                if (attackType == 0) {
                    if (changeTime > 0) {
                        Color nc = nextType.getColor();
                        Color cc = me.getType().getColor();
                        myColor = new Color(((changeTime * cc.r) + ((2 - changeTime) * nc.r)) / 2f, ((changeTime * cc.g) + ((2 - changeTime) * nc.g)) / 2f, ((changeTime * cc.b) + ((2 - changeTime) * nc.b)) / 2f, 1f);
                    }
                    if (changeTime <= 0) {
                        idleTime = 0.33f;
                        me.setType(nextType);
                        attackType = -1;
                    }
                }
                if (attackType == 1) {
                    if (dashChargeTime > 0) {

                    } else if (dashIdleTime > 0) {
                        if(dashIdleTime > 0.5f)
                            angleV = new Vector2(target.getX()-me.getX(), target.getY()-me.getY());
                    } else if (dashTime > 0) {
                        motion = new Vector2((10f + 400f * (dashTime/3f))*(float)Math.cos(angleV.angleRad()), (10f + 400f * (dashTime/3f))*(float)Math.sin(angleV.angleRad()));
                    } else {
                        idleTime = 1.25f;
                        attackType = -1;
                        motion = new Vector2(0,0);
                    }
                }
                if (attackType == 2) {
                    if (shootChargeTime > 0) {

                    } else if (shootTime > 0) {
                        if (shootWaveCD > 0) {
                            shootWaveCD -= deltaTime;
                        } else {
                            for (int i = 0; i < 30; i++) {
                                float theta = (i + (waveAlternator / 3f)) / (30f) * 360;
                                Projectile p = new Projectile(me, new Vector2((float) (25f * cos(toRadians(theta))), (float) (25f * sin(toRadians(theta)))), me.getDamage(), 3f, me.getProjectileSpeed(), 0, 1);
                            }
                            shootWaveCD = 0.15f;
                            waveAlternator = (waveAlternator + 1) % 3;
                        }
                    } else {
                        idleTime = 1.75f;
                        attackType = -1;
                    }
                }
            }
        }else{
            System.out.println("LOST PLAYER");
        }
    }

    public Vector2 getMotion(){
        return motion;
    }

    @Override
    public TextureRegion getCurrentAnimation(){
    TextureRegion region;
        if(idleTime > 0){
           return (TextureRegion) idleAnim.getKeyFrame(idleTime, true);
        }else{
            if(changeTime > 0)
                return (TextureRegion) (changeAnim.getKeyFrame(changeTime, true));
            if(dashChargeTime > 0)
                return (TextureRegion) (dashChargeAnim.getKeyFrame(1-dashChargeTime, false));
            if(dashIdleTime > 0)
                return (TextureRegion) (dashIdleAnim.getKeyFrame(dashIdleTime, true));
            if(dashTime > 0)
                return (TextureRegion) (dashAnim.getKeyFrame(dashTime, true));
            if(shootChargeTime > 0)
                return (TextureRegion) (shootChargeAnim.getKeyFrame(1.5f-shootChargeTime, false));
            if(shootTime > 0)
                return (TextureRegion) (shootAnim.getKeyFrame(shootTime, true));
            return (TextureRegion) (idleAnim.getKeyFrame(0));
        }
    }

    @Override
    public Vector2 getCurrentMotion(){
        return motion;
    }

    public Color getMyColor(){
        if(myColor != null)
            return myColor;
        if(nextType != null)
            return nextType.getColor();
        return new Color(1,1,1,1);
    }

    @Override
    public void alert(Entity e1){
        //mmmmmmmmmmm
    }

    public void giveTarget(Player p){
        this.target = p;
    }

    public void hitWall(boolean hor){
        if(angleV != null) {
            if (hor) {
                angleV.x = -angleV.x;
            } else {
                angleV.y = -angleV.y;
            }
        }
    }
    public Vector2 getAngleV(){
        if(angleV != null){
            return angleV;
        }else{
            return new Vector2(0,0);
        }
    }

    public boolean isFlipped(){
        if(Math.cos(motion.angleRad()) < 0){
            flipped = true;
        }else{
            flipped = false;
        }
        return flipped;
    }

    public void kill(){
        motion = new Vector2(0,0);
        idleTime = 5;
        dashTime = 0;
        dashIdleTime = 0;
        dashChargeTime = 0;
        changeTime = 0;
        shootTime = 0;
        shootChargeTime = 0;
    }

}

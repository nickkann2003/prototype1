package nick.nrk.Entities;
/*
* Enemy class
* Will be extended to represent different enemy types
 */

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import nick.nrk.BodyFactory;
import nick.nrk.Entities.Behaviors.AIBehavior;
import nick.nrk.Items.Item;

import java.util.ArrayList;
import java.util.List;

public class Enemy extends Entity{

    protected boolean isAgro;
    protected Entity target;
    protected AIBehavior bhvr;
    protected List<Enemy> container; //Every enemy has a refernce to its own container (for deletion)

    public Enemy(World world) {
        super(world);
    }
    public Enemy(World world, Vector2 pos, EArchtype enemyID){
        this(world);

        this.maxHealth = enemyID.getHealth();
        this.health = maxHealth;
        this.x = pos.x;
        this.y = pos.y;
        this.damage = enemyID.getDamage();
        this.type = enemyID.getType();
        this.width = enemyID.getWidth();
        this.height = enemyID.getHeight();
        this.speed = enemyID.getSpeed();
        this.kbResist = enemyID.getKBResist();
        this.range = enemyID.getRange();
        String animID = enemyID.getAnimationString();
        bhvr = enemyID.getBhvr().recreate(animID, speed, this);
        this.body = createBody();
        projectileSpeed = speed*2.5f;
    }

    public Enemy(World world, Vector2 pos, EArchtype enemyID, List<Enemy> container){
        this(world, pos, enemyID);
        this.container = container;
    }

    protected Body createBody(){
        return BodyFactory.createEnemyBody(x, y, width, height, this);
    }

    @Override
    public void display(){
        if(knocked) {
            batch.setColor(0.8f, 0.8f, 0.8f, 0.9f);
        }else{
            batch.setColor(type.getColor());
        }
        batch.draw(bhvr.getCurrentAnimation(), x-(width+2)/2, y-(height+2)/2, width+2, height+2);
        batch.setColor(Color.WHITE);
    }

    @Override
    public void update(float deltaTime){
        super.update(deltaTime);
        bhvr.update(deltaTime);
        movement = bhvr.getCurrentMotion();
    }

    public void activate(){
        body.setActive(true);
        active = true;
    }

    public void deactivate(){
        isAgro = false;
        body.setLinearVelocity(0,0);
        body.setActive(false);
        active = false;
        bhvr.deactivate();
    }

    public void alert(Entity target){
        if(!isAgro) {
            this.isAgro = true;
            this.target = target;
            if(target instanceof Projectile) { //If the target is a projectile
                this.target = ((Projectile)target).getParent(); //Gets its creator
            }
            while(this.target instanceof Projectile){ //While target is still a projectile (in case of projectiles creating other projectiles)
                this.target = ((Projectile)target).getParent(); //Get its parent
            }
            bhvr.alert(this.target);
            if(container != null) {
                for (Enemy e : container) {
                    e.alert(this.target);
                }
            }
        }
    }

    @Override
    public Entity getTarget(){
        return target;
    }

    public void destroy(){
        world.destroyBody(body);
    }

}

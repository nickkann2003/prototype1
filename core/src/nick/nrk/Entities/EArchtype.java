package nick.nrk.Entities;

import com.badlogic.gdx.math.MathUtils;
import nick.nrk.Entities.Behaviors.*;

import static jdk.nashorn.internal.objects.NativeMath.random;
import static nick.nrk.utils.Constants.tier1EnemyTypes;
import static nick.nrk.utils.Constants.tier2EnemyTypes;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Enumerated type for enemies
 * Each enemy is going to hold its stats, for easy creation
 */
public enum EArchtype {
    //Training Dummys
    DUMMY(8f, 0.5f, 14f, 14f, 100, Type.BAS, "enemy/base/enemy", 0, 5f, 0.05f, new AIBehavior()),
    DUMMYF(8f, 0.5f, 14f, 14f, 100, Type.FIR, "enemy/base/enemy", 0, 5f, 0.05f, new AIBehavior()),
    DUMMYW(8f, 0.5f, 14f, 14f, 100, Type.WTR, "enemy/base/enemy", 0, 5f, 0.05f, new AIBehavior()),
    DUMMYN(8f, 0.5f, 14f, 14f, 100, Type.NAT, "enemy/base/enemy", 0, 5f, 0.05f, new AIBehavior()),
    DUMMYE(8f, 0.5f, 14f, 14f, 100, Type.ERT, "enemy/base/enemy", 0, 5f, 0.05f, new AIBehavior()),

    //Tier 1 Enemies (Populated 0-25)
    BASIC(4f,1,14,14, 100, Type.WTR, "enemy/melee/enemy", 1, 50, 1f, new Melee()),
    MELEE(2.5f, 1f, 6, 6, 50,Type.NAT, "enemy/base/enemy", 2, 70, 1.5f, new Charger()),
    RANGED2(15, 2.5f, 30, 30, 35, Type.FIR, "enemy/melee/enemy", 3, 25, 0.3f, new Melee()),
    TURRET1(8, 2, 22, 22, 250, Type.ERT, "enemy/base/enemy", 4, 1, 0.25f, new Turret()),
    SPRAYER(5, 1.25f, 14, 14, 120, Type.WTR, "enemy/base/enemy", 5, 60,2.5f, new Homing()),
    MELEE2(7.5f, 2.2f, 10, 10, 100, Type.NAT, "enemy/base/enemy", 6, 60, 0.8f, new Ranged()),
    HIGHTIER(10, 2, 14, 14, 75, Type.ERT, "enemy/base/enemy", 8, 80, 0.4f, new Charger()),
    RANGED(4f,4f,10,10,150, Type.FIR, "enemy/base/enemy", 10, 45, 1f, new Homing()),

    //Tier 2 Enemies (Populated 25-100)
    SWARMER(1.5f, 0.5f, 6,6 35, Type.WTR, "enemy/base/enemy", 2, 80, 0.2f, new Charger()),
    BASIC2(8f, 2.5f, 14, 14, 100, Type.FIR, "enemy/melee/enemy", 8, 75, 0.75f, new Melee()),
    TURRET2(16, 5f, 30, 30, 250, Type.FIR, "enemy/base/enemy", 15, 1, 0.6f, new Turret()),
    TANK(35, 6f, 46, 46, 25, Type.ERT, "enemy/melee/enemy", 18, 17.5f, 0.15f, new Melee()),
    SCROBLIN(50, 20, 14, 14, 100, Type.NAT, "enemy/base/enemy", 25, 180, 1.2f, new Charger()),

    //Final Enemy (Representative of a cap, keep bumping up its strength to keep it at the bottom)
    FINAL(500,100,62,62,200,Type.BAS, "enemy/base/enemy", 100, 10, 0.2f, new Homing());

    private final float maxHealth;
    private final float damage;
    private final float width;
    private final float height;
    private final float speed;
    private final float kbResist;
    private final float range;
    private final Type type;
    //String representing location of animation
    //All animations will be the same after the location string (ex. _walking_left, _walking_right
    private final String animationString;
    //AI Behaviour, each enemy type keeps an empty version of its behavior,
    //That its successors can then recreate with their own stats
    private final AIBehavior bhvr;
    //Integer representing value of enemy in terms of difficulty
    //(ex. Room dif 5, Enemy dif 1 = 5 enemies spawned)
    private final int strength;

    EArchtype(float maxHealth, float damage, float width, float height, float range, Type type, String animationString, int strength, float speed, float kbResist, AIBehavior bhvr){
        this.maxHealth = maxHealth;
        this.damage = damage;
        this.width = width;
        this.height = height;
        this.type = type;
        this.animationString = animationString;
        this.strength = strength;
        this.speed = speed;
        this.kbResist = kbResist;
        this.bhvr = bhvr;
        this.range = range;
    }

    //I DID A RECURSION, NEAT
    public static LinkedList<EArchtype> populate(int diff){
        if(diff <= 0){
            return new LinkedList<EArchtype>();
        }else if(diff >= 25 && diff <= 100) {
            return populateTier2(diff);
        }else{
            int rDiff = diff;
            LinkedList<EArchtype> list = new LinkedList<>();
            EArchtype added = getRandom(diff);
            rDiff -= added.getStrength();
            list.add(added);
            list.addAll(populate(rDiff));
            return list;
        }
    }

    //Populate function for higher difficulty (25-100)
    public static LinkedList<EArchtype> populateTier2(int diff){
        if(diff <= 15){
            return new LinkedList<EArchtype>();
        }else{
            LinkedList<EArchtype> list = new LinkedList<>();
            EArchtype added = getRandomTier2(diff);
            list.add(added);
            list.addAll(populateTier2(diff-added.getStrength()));
            return list;
        }
    }


    private static EArchtype getRandom(int diff){
        int cap = 0;
        EArchtype selected = null;
        while((cap < tier1EnemyTypes.size()-1) && tier1EnemyTypes.get(cap+1).getStrength() <= diff) {
            cap++;
        }
        while(selected == null) {
            if (cap == 0) {
                selected = BASIC;
            }else {
                selected = tier1EnemyTypes.get(MathUtils.random(cap));
            }
        }
        return selected;
    }

    private static EArchtype getRandomTier2(int diff){
        int cap = 0;
        EArchtype selected = null;
        while((cap < tier2EnemyTypes.size()-1) && tier2EnemyTypes.get(cap+1).getStrength() <= diff) {
            cap++;
        }
        while(selected == null) {
            if (cap == 25) {
                selected = SCROBLIN;
            }
            if (MathUtils.random(cap * cap) < cap) {
                cap = cap / 2;
            } else {
                selected = tier2EnemyTypes.get(cap);
            }
        }
        return selected;
    }

    public float getHealth(){
        return maxHealth;
    }
    public float getDamage(){
        return damage;
    }
    public float getWidth(){
        return width;
    }
    public float getHeight(){
        return height;
    }
    public float getRange(){return range;}
    public Type getType(){
        return type;
    }
    public String getAnimationString(){
        return animationString;
    }
    public int getStrength(){
        return strength;
    }
    public float getSpeed(){
        return speed;
    }
    public float getKBResist(){
        return kbResist;
    }
    public AIBehavior getBhvr(){return bhvr;}
}

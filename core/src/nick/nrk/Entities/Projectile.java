package nick.nrk.Entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import nick.nrk.BodyFactory;
import nick.nrk.Entities.Behaviors.AIBehavior;
import nick.nrk.Entities.Behaviors.ProjectileBehaviorPathed;
import nick.nrk.Entities.Behaviors.ProjectileBehaviorTargeted;
import nick.nrk.FinalPrototype1;
import nick.nrk.ProjectileHandler;
import com.badlogic.gdx.math.MathUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Projectile extends Entity {
    protected Entity parent;
    protected Vector2 path;
    protected Entity target;
    protected AIBehavior behavior;
    protected int bounces;
    protected LinkedList<Entity> hitted;
    protected Entity addNext;
    protected float damageNext;
    protected float invTime;

    /**
     * USE LINKED LIST FOR STORAGE
     * Extends entity for collision purposes
     **/


    public Projectile(World world) {
        super(world);
        maxHealth = 1;
        health = 1;
        invTime = 3f;
        hitted = new LinkedList<Entity>();
        damageNext = 0;
    }

    public Projectile(float damage, float radius, float speed){
        this(FinalPrototype1.getWorld());
        this.damage = damage;
        this.width = radius*2;
        this.height = radius*2;
        this.speed = speed;
        animation = new Animation(0.1f, atlas.findRegions("projectile/projectile"));
        ProjectileHandler.createProjectile(this);
        bounces = 1;
    }

    public Projectile(Entity parent, Vector2 path, float damage, float radius, float speed, int bounces){
        this(damage, radius, speed);
        this.parent = parent;
        this.type = parent.getType();
        this.path = path;
        if(parent instanceof Enemy){
            body = BodyFactory.createEnemyProjectile(parent.getX(), parent.getY(), radius, this);
        }else if(parent instanceof Player){
            body = BodyFactory.createPlayerProjectile(parent.getX(), parent.getY(), radius, this);
        }
        behavior = new ProjectileBehaviorPathed("projectile/projectile", parent.getProjectileSpeed(), this, path);
        this.bounces = bounces;
    }

    public Projectile(Entity parent, Vector2 path, float damage, float radius, float speed, int bounces, float health){
        this(damage, radius, speed);
        this.parent = parent;
        this.type = parent.getType();
        this.path = path;
        if(parent instanceof Enemy){
            body = BodyFactory.createEnemyProjectile(parent.getX(), parent.getY(), radius, this);
        }else if(parent instanceof Player){
            body = BodyFactory.createPlayerProjectile(parent.getX(), parent.getY(), radius, this);
        }
        behavior = new ProjectileBehaviorPathed("projectile/projectile", speed, this, path);
        this.bounces = bounces;
        this.maxHealth = health;
        this.health = health;
    }

    public Projectile(Entity parent, Vector2 path, float damage, float radius, float speed, int bounces, float health, float invTime){
        this(parent, path, damage, radius, speed, bounces, health);
        this.invTime = invTime;
    }

    public Projectile(Entity parent, Entity target, float damage, float radius, float speed){
        this(damage, radius, speed);
        this.parent = parent;
        this.type = parent.getType();
        this.target = target;
        if(parent instanceof Enemy){
            body = BodyFactory.createEnemyProjectile(parent.getX(), parent.getY(), radius, this);
        }else if(parent instanceof Player){
            body = BodyFactory.createPlayerProjectile(parent.getX(), parent.getY(), radius, this);
        }
        behavior = new ProjectileBehaviorTargeted("projectile/projectile", parent.getProjectileSpeed(), this);
        this.bounces = 0;
    }

    public Projectile(Entity parent, Entity target, Vector2 vInitial, float damage, float radius, float speed){
        this(damage, radius, speed);
        this.parent = parent;
        this.type = parent.getType();
        this.target = target;
        if(parent instanceof Enemy){
            body = BodyFactory.createEnemyProjectile(parent.getX(), parent.getY(), radius, this);
        }else if(parent instanceof Player){
            body = BodyFactory.createPlayerProjectile(parent.getX(), parent.getY(), radius, this);
        }
        behavior = new ProjectileBehaviorTargeted("projectile/projectile", parent.getProjectileSpeed(), target, vInitial, this);
        this.bounces = 0;
    }

    public Projectile(Entity parent, Entity target, Vector2 vInitial, float damage, float radius, float speed, float accFactor){
        this(damage, radius, speed);
        this.parent = parent;
        this.type = parent.getType();
        this.target = target;
        if(parent instanceof Enemy){
            body = BodyFactory.createEnemyProjectile(parent.getX(), parent.getY(), radius, this);
        }else if(parent instanceof Player){
            body = BodyFactory.createPlayerProjectile(parent.getX(), parent.getY(), radius, this);
        }
        behavior = new ProjectileBehaviorTargeted("projectile/projectile", parent.getProjectileSpeed(), target, vInitial, this, accFactor);
        this.bounces = 0;
    }

    public Projectile(Entity parent, Entity target, Vector2 vInitial, float damage, float radius, float speed, float accFactor, float health){
        this(parent, target, vInitial, damage, radius, speed, accFactor);
        this.maxHealth = health;
        this.health = health;
    }

    public Projectile(Entity parent, Entity target, Vector2 vInitial, float damage, float radius, float speed, float accFactor, float health, float invTime){
        this(parent, target, vInitial, damage, radius, speed, accFactor, health);
        this.invTime = invTime;
    }

    @Override
    public void display() {
        float w = (float)(Math.cbrt(health)/Math.cbrt(maxHealth))*(width-0.4f) + 0.4f;
        float h = (float)(Math.cbrt(health)/Math.cbrt(maxHealth))*(height-0.4f) + 0.4f;
        if(w > width || w < 0.4f)
            w = 0.4f;
        if(h > height || h < 0.4f)
            h = 0.4f;
        batch.setColor(type.getColor());
        batch.draw(behavior.getCurrentAnimation(), x-(w+2)/2, y-(h+2)/2, w+2, h+2);
        batch.setColor(Color.WHITE);
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        behavior.update(deltaTime);
        movement = behavior.getCurrentMotion();
        if(health <= 0){
            deathFlag = true;
        }
        if(deathFlag){
            world.destroyBody(body);
        }
        if(null != addNext){
            hitted.add(addNext);
            addNext = null;
        }

        if(damageNext > 0){
            health -= damageNext;
            damageNext = 0;
        }
        invTime -= deltaTime;
        if(invTime < 0){
            health -= deltaTime*(maxHealth*1.2f);
        }
    }

    public void applyDamage(float damage, Entity dealer){
        damageNext = damage;
        addNext = dealer;
    }


    public void activate(){
        active = true;
        body.setActive(true);
    } //Activates projectile
    public void deactivate(){
        active = false;
        body.setActive(false);
    } //Deactivates projectile

    //Destroys projectile in world
    public void destroy(){
        world.destroyBody(body);
        deathFlag = true;
    }

    //Bounce functions
    public void bounceY(){
        if(bounces <= 0){
            deathFlag = true;
        }
        if(path != null){
            path.y = -path.y;
            bounces -= 1;
        }
        hitted = new LinkedList<Entity>();
        invTime += 0.75f + 0.3f * bounces;
    }
    public void bounceX(){
        if(bounces <= 0){
            deathFlag = true;
        }
        if(path != null){
            path.x = -path.x;
            bounces -= 1;
        }
        hitted = new LinkedList<Entity>();
        invTime += 0.75f + 0.3f * bounces;
    }


    //Returns parent entity
    public Entity getParent(){
        return this.parent;
    }
    /*
    * A projectile is an entity
     */

    public boolean canHit(Entity e1){
        for(Entity e : hitted){
            if(e.equals(e1)){
                return false;
            }
        }
        return true;
    }
}

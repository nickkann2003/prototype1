package nick.nrk.Entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Colors;
import com.badlogic.gdx.math.MathUtils;

public enum Type {
    WTR, FIR, NAT, ERT, BAS;

    public float compare(Type t){
        if(this == WTR){
            if(t == FIR || t == BAS)
                return 0.5f;
            if(t == ERT)
                return 2f;
            return 1f;
        }
        if(this == FIR){
            if(t == NAT || t == BAS)
                return 0.5f;
            if(t == WTR)
                return 2f;
            return 1f;
        }
        if(this == NAT){
            if(t == ERT || t == BAS)
                return 0.5f;
            if(t == FIR)
                return 2f;
            return 1f;
        }
        if(this == ERT){
            if(t == WTR || t == BAS)
                return 0.5f;
            if(t == NAT)
                return 2f;
            return 1f;
        }
        if(this == BAS){
            if(t == BAS){
                return 1f;
            }else{
                return 2f;
            }
        }
        if(t == BAS){
            if(this == BAS){
                return 1f;
            }else{
                return 2f;
            }
        }
        return 1f;
    }

    public Color getColor(){
        if(this == WTR){
            return new Color(0.42f, 0.42f, 0.94f, 1f);
        }
        if(this == FIR){
            return new Color(0.94f, 0.42f, 0.42f, 1f);
        }
        if(this == NAT){
            return new Color(0.42f, 0.94f, 0.42f, 1f);
        }
        if(this == ERT){
            return new Color(0.67f, 0.42f, 0.42f, 1f);
        }
        return Color.WHITE;
    }

    public Type getNext(){
        if(this == WTR){
            return FIR;
        }else if(this == FIR){
            return NAT;
        }else if(this == NAT){
            return ERT;
        }else if(this == ERT){
            return BAS;
        }else if(this == BAS){
            return WTR;
        }
        return BAS;
    }

    public Type getPrev(){
        if(this == WTR){
            return BAS;
        }else if(this == FIR){
            return WTR;
        }else if(this == NAT){
            return FIR;
        }else if(this == ERT){
            return NAT;
        }else if(this == BAS){
            return ERT;
        }
        return BAS;
    }

    public Type getRandom(){
        int r = MathUtils.random(3);
        if(r == 0){
            return Type.WTR;
        }else if(r == 1){
            return Type.FIR;
        }else if(r == 2){
            return Type.NAT;
        }else if(r == 3){
            return Type.ERT;
        }
        return Type.BAS;
    }

    public Type getRandom(Type t){
        Type next = getRandom();
        while(t.compare(next) == 1){
            next = getRandom();
        }
        return next;
    }
}

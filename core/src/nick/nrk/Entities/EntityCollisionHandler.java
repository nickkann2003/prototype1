package nick.nrk.Entities;

import com.badlogic.gdx.math.Vector2;
import nick.nrk.AudioController;
import nick.nrk.Items.Item;

import static java.lang.StrictMath.cos;
import static java.lang.StrictMath.sin;

public final class EntityCollisionHandler {
    /*
     * This class handle entity collision
     * In charge of damage, I-Frames, knockback etc
     */

    /**
     *
     * @param e1 Entity 1
     * @param e2 Entity 2
     *           Performs necessary functions when two entities collide
     *           Applies damage and knockback
     */
    public static void HandleCollision(Entity e1, Entity e2){
        /*
            While player is immune
            Add Player function to take in entities it will collide with
            Inside player update, when immunity ends, run handle collision for all entities in that list
            then clear the list

             OR

             Look into changing mask to disable collision with entities
         */

        boolean somethingHit = false;

        //Perform knockback
        if(!(e1 instanceof Enemy && e2 instanceof Enemy) && !((e1.immune() && e1 instanceof Player) || (e2.immune() && e2 instanceof Player)) && (e1.active() && e2.active())){ //Not enemy-enemy collision, neither is immune, both are active
            float angle = determineAngle(e1, e2);
            float inverse = angle + (float) Math.PI;
            //knockback
            if(!(e1.hasContact(e2)) && !(e2.hasContact(e1))) {
                if (!(e1.isHitThisFrame() && e1 instanceof Player)) {
                    if (!(e1 instanceof Projectile)) {
                        if((!(e2 instanceof Projectile)) || ((e2 instanceof Projectile) && ((Projectile) e2).canHit(e1))) {
                            e1.applyKnockback(new Vector2((float) (50 * (1 + Math.sqrt(e2.getDamage())) * cos(inverse)), (float) (50 * (1 + Math.sqrt(e2.getDamage())) * sin(inverse))));
                            e1.applyDamage(e2.getDamage() / e2.getType().compare(e1.getType()));
                            e1.contact(e2);
                            somethingHit = true;
                        }
                    } else {
                        if(!(e2 instanceof Projectile)) {
                            if (((Projectile) e1).canHit(e2)) {
                                ((Projectile) e1).applyDamage(e2.getDamage(), e2);
                                e1.contact(e2);
                                somethingHit = true;
                            }
                        }else{
                            if (((Projectile) e1).canHit(e2)) {
                                ((Projectile) e1).applyDamage(e2.getHealth(), e2);
                                e1.contact(e2);
                                somethingHit = true;
                            }
                        }
                    }
                }
                if (!(e2.isHitThisFrame() && e2 instanceof Player)) {
                    if (!(e2 instanceof Projectile)) {
                        if((!(e1 instanceof Projectile)) || ((e1 instanceof Projectile) && ((Projectile) e1).canHit(e2))) {
                            e2.applyKnockback(new Vector2((float) (50 * (1 + Math.sqrt(e1.getDamage())) * (cos(angle))), (float) (50 * (1 + Math.sqrt(e1.getDamage())) * (sin(angle)))));
                            e2.applyDamage(e1.getDamage() / e1.getType().compare(e2.getType()));
                            e2.contact(e1);
                            somethingHit = true;
                        }
                    } else {
                        if(!(e1 instanceof Projectile)) {
                            if (((Projectile) e2).canHit(e1)) {
                                ((Projectile) e2).applyDamage(e1.getDamage(), e1);
                                e2.contact(e1);
                                somethingHit = true;
                            }
                        }else{
                            if (((Projectile) e2).canHit(e1)) {
                                ((Projectile) e2).applyDamage(e1.getHealth(), e1);
                                e2.contact(e1);
                                somethingHit = true;
                            }
                        }
                    }
                }
                float cmp = 1; //compare mult
                if (e1 instanceof Enemy) {
                    ((Enemy) e1).alert(e2);
                    cmp = e1.getType().compare(e2.getType());
                }
                if (e2 instanceof Enemy) {
                    ((Enemy) e2).alert(e1);
                    cmp = e2.getType().compare(e1.getType());
                }
                if(somethingHit) {
                    if (e1 instanceof Player || e2 instanceof Player) {
                        AudioController.playPlayerHit();
                    } else {
                        if(cmp == 1) {
                            AudioController.playEnemyHit();
                        }else if(cmp > 1){
                            AudioController.playEnemyHitWeak();
                        }else if(cmp < 1){
                            AudioController.playEnemyHitStrong();
                        }
                    }
                }
            }
        }
    }

    //Collision handler for entities and items
    public static void EntityItemCollision(Player p1, Item item){
        item.pickUp(p1, p1.getInventory().size());
    }

    public static void handleWallCollision(Projectile p, Vector2 cLoc){
        if(cLoc.x == -1){
            p.bounceY();
        }else if(cLoc.y == -1){
            p.bounceX();
        }
    }

    public static void handleBossSlimeWallCollision(BossSlime e, Vector2 cLoc){
        if(cLoc.x == -1){
            e.hitWall(false);
        }else if(cLoc.y == -1){
            e.hitWall(true);
        }
    }

    /**
     *
     * @param e1 Entity 1
     * @param e2 Entity 2
     * @return Angle between the two positions of the entities, perspective of e1
     */
    private static float determineAngle(Entity e1, Entity e2){
        Vector2 connector = new Vector2(e2.getX() - e1.getX(), e2.getY()  - e1.getY()); //Vector connecting two objects
        return connector.angleRad();
    }
}

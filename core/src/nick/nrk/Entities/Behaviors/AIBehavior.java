package nick.nrk.Entities.Behaviors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import nick.nrk.Entities.Entity;
import nick.nrk.FinalPrototype1;

import java.util.ArrayList;

/**
 * Parent class for all other AI Behavior classes
 * Behavior class will also handle animations,
 * Since each behavior is going to need to know its specific animations
 */

public class AIBehavior {
    TextureAtlas atlas;
    SpriteBatch batch;
    Entity target;
    Entity me;
    float speed;
    float targetRange;

    ArrayList<Animation> allAnims;
    Animation anim;
    float passed;

    Vector2 motion;
    boolean agro;

    //No args constructor for enumerated types
    public AIBehavior(){

    }

    //Constructor with args
    public AIBehavior(String animID, float speed, Entity me){
        this.atlas = FinalPrototype1.getAtlas();
        this.batch = FinalPrototype1.getBatch();
        this.speed = speed;
        allAnims = new ArrayList<>();
        allAnims.add(new Animation(16f/speed, atlas.findRegions(animID+"_walking_up")));
        allAnims.add(new Animation(16f/speed, atlas.findRegions(animID+"_walking_left")));
        allAnims.add(new Animation(16f/speed, atlas.findRegions(animID+"_walking_down")));
        allAnims.add(new Animation(16f/speed, atlas.findRegions(animID+"_walking_right")));
        allAnims.add(new Animation(16f/speed, atlas.findRegions(animID+"_standing")));
        anim = allAnims.get(4);
        this.me = me;
        passed = 0;
    }
    public AIBehavior(String animID, float speed, Entity me, float targetRange){
        this(animID, speed, me);
        this.targetRange = targetRange;
    }

    public void update(float deltaTime){
        passed += deltaTime;
    }

    public Vector2 getCurrentMotion(){
        return motion;
    }

    public TextureRegion getCurrentAnimation(){
        double theta = 0;
        if(target != null && motion != null) {
            theta = (new Vector2(target.getX() - me.getX(), target.getY() - me.getY())).angleRad();
            theta = theta + (Math.PI / 4);
        }else{
            return (TextureRegion) (allAnims.get(4).getKeyFrame(passed, true));
        }
        if(Math.cos(theta) > 0){
            if(Math.sin(theta) > 0){
                anim = allAnims.get(3);
            }else{
                anim = allAnims.get(2);
            }
        }else{
            if(Math.sin(theta) > 0){
                anim = allAnims.get(0);
            }else{
                anim = allAnims.get(1);
            }
        }
        return (TextureRegion) (anim.getKeyFrame(passed, true));
    }

    public void alert(Entity target){
        this.target = target;
        agro = true;
    }

    public void deactivate(){
        motion = null;
        agro = false;
    }

    public float getSpeed(){
        return speed;
    }

    public AIBehavior recreate(String animID, float speed, Entity me){
        return new AIBehavior(animID, speed, me);
    }

    protected void sitInTargetRange(){
        Vector2 v = new Vector2(target.getX() - me.getX(), target.getY() - me.getY()); //Vector of distance between two
        if(v.len() > targetRange-5 && v.len() < targetRange + 5){ //If enemy is in sweet spot around target range
            motion = new Vector2(0,0); //Stop moving
        }else if(v.len() < targetRange){ //If distance is shorter than target range
            float t = v.angleRad();
            //Motion vector backs enemy up slowly to its target range
            motion = new Vector2(0.75f*speed * (float) Math.cos(t-Math.PI), 0.75f*speed * (float) Math.sin(t-Math.PI));
        }else{ //If enemy is further than its target range
            float t = v.angleRad();
            //Motion has enemy run towards player
            motion = new Vector2(speed * (float) Math.cos(t), speed * (float) Math.sin(t));
        }
    }

    //Hit wall function, future AIs will use
    public void hitWall(Vector2 cLoc){

    }

}

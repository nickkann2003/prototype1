package nick.nrk.Entities.Behaviors;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import nick.nrk.Entities.Entity;
import nick.nrk.Entities.Projectile;

public class Homing extends AIBehavior{
    float sTimer = 3f;
    float cd = 0.1f;
    int shotsLeft = 15;

    public Homing(){
        super();
    }

    public Homing(String animID, float speed, Entity me) {
        super(animID, speed, me);
        targetRange = me.getRange();
    }

    public void update(float deltaTime){
        passed += deltaTime;
        if(agro){
            sitInTargetRange();
                if(sTimer <= 0){
                    if(shotsLeft <= 0){
                        sTimer = 3f;
                        cd = 0.05f;
                        shotsLeft = 15;
                    }else{
                        if(cd <= 0){
                            Projectile p = new Projectile(me, me.getTarget(), new Vector2(MathUtils.random(-1f, 1f), MathUtils.random(-1f, 1f)), me.getDamage(), 2f, me.getProjectileSpeed(), 2, 0.2f);
                            cd = 0.1f;
                            shotsLeft -= 1;
                        }
                        cd -= deltaTime;
                    }
                }
            sTimer -= deltaTime;
        }
    }

    @Override
    public AIBehavior recreate(String animID, float speed, Entity me){
        return new Homing(animID, speed, me);
    }
}

package nick.nrk.Entities.Behaviors;

import nick.nrk.Entities.Entity;
import nick.nrk.Entities.Projectile;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.MathUtils;

public class Turret extends AIBehavior{
    protected float shotCD;

    public Turret(){super();}

    public Turret(String animID, float speed, Entity me){
        super(animID, speed, me);
        shotCD = 1/speed;
    }


    @Override
    public void update(float deltaTime){
        passed += deltaTime;
        shotCD -= deltaTime;
        if(shotCD <= 0){
            Vector2 theta = new Vector2(target.getX()-me.getX(), target.getY()-me.getY());
            float ang = theta.angleRad() + MathUtils.random( (float)-Math.PI/25, (float)Math.PI/25 );
            //Entity parent, Vector2 path, float damage, float radius, float speed, int bounces, float health
            Projectile p = new Projectile(me, new Vector2(10f*(float)Math.cos(ang), 10f*(float)Math.sin(ang)), me.getDamage(), 4f, 115, 0, 1.25f);
            shotCD = 1/speed;
        }
    }

    public AIBehavior recreate(String animID, float speed, Entity me){
        return new Turret(animID, speed, me);
    }
}

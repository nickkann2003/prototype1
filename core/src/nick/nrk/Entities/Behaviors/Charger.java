package nick.nrk.Entities.Behaviors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Filter;
import nick.nrk.Entities.Entity;
import nick.nrk.utils.Constants;

public class Charger extends AIBehavior {
    /**
     * Charger AI
     * Sits at target range
     * When timer is up, stands in place charging for a moment
     * Then sprints towards target (accelerates towards player) for certain time
     */
    protected Animation idleAnim;
    protected Animation dashAnim;

    float cooldown; //Cooldown on charge
    float chargeTimer; //Time left until charge goes off
    float chargeLeft; //Time left in the charge
    Vector2 vel; //Velocity of the charge
    Vector2 acc; //Acceleration of the charge
    Vector2 dist; //Distance between enemy and target


    public Charger(){
        super();
    }

    public Charger(String animID, float speed, Entity me) {
        super(animID, speed, me);
        targetRange = speed/3f + me.getRange();
        vel = new Vector2(0,0);
        dist = new Vector2(0,0);
        acc = new Vector2(0,0);
        idleAnim = new Animation(1/4f, atlas.findRegions("enemy/charger/chargerIdle"));
        dashAnim = new Animation(1/8f, atlas.findRegions("enemy/charger/chargerDash"));
    }

    //Override update function for specific AI behavior
    public void update(float deltaTime){
        dist.x = target.getX() - me.getX();
        dist.y = target.getY() - me.getY();

        //If CD is done
        if(cooldown <= 0){
            //If charge is still charing
            if(chargeTimer > 0){
                motion = new Vector2(0.00001f * (target.getX()-me.getX()),0.00001f * (target.getY()-me.getY())); //Idle
                vel.x = speed * (float)Math.cos(dist.angleRad());
                vel.y = speed * (float)Math.sin(dist.angleRad());
                passed += deltaTime; //Double up on animation passing
                chargeTimer -= deltaTime * MathUtils.random(0.85f, 1.15f);

                //If charge is not charging
            }else{
                //If enemy is still charging
                if(chargeLeft > 0){
                    setChargingFilter();
                    acc.x = dist.x * 0.1f * (speed/25f);
                    acc.y = dist.y * 0.1f * (speed/25f);

                    if(acc.len() < 50){
                        acc.scl(acc.len()/50);
                    }

                    vel.add(acc);
                    vel = new Vector2(speed * 2.45f * (float)Math.cos(vel.angleRad()), speed * 2.45f * (float)Math.sin(vel.angleRad()));
                    motion = vel;
                    chargeLeft -= deltaTime;
                }else{
                    cooldown = MathUtils.random(2f, 6f);
                    resetFilter();
                }
            }

            //If CD is not done
        }else{
            sitInTargetRange();
            motion = motion.scl(0.4f);
            cooldown -= deltaTime;
            chargeTimer = 1.25f;
            chargeLeft = 3.5f;
        }
        passed += deltaTime;
    }

    @Override
    public TextureRegion getCurrentAnimation() {
        if(chargeTimer < 0){
            return (TextureRegion) dashAnim.getKeyFrame(passed, true);
        }else{
            return (TextureRegion) idleAnim.getKeyFrame(passed, true);
        }
    }

    //Recreate function for Charger behavior
    public AIBehavior recreate(String animID, float speed, Entity me){
        return new Charger(animID, speed, me);
    }

    private void setChargingFilter(){
        Filter f = me.getBody().getFixtureList().get(0).getFilterData();
        f.categoryBits = Constants.CATEGORY_ENEMY_PROJECTILE;
        f.maskBits = Constants.MASK_ENEMY_PROJECTILE;
        me.getBody().getFixtureList().get(0).setFilterData(f);
    }

    private void resetFilter(){
        Filter f = me.getBody().getFixtureList().get(0).getFilterData();
        f.categoryBits = Constants.CATEGORY_ENEMY;
        f.maskBits = Constants.MASK_ENEMY;
        me.getBody().getFixtureList().get(0).setFilterData(f);
    }

}

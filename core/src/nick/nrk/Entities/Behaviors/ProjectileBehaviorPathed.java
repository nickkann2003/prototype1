package nick.nrk.Entities.Behaviors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import nick.nrk.Entities.Entity;
import nick.nrk.FinalPrototype1;

public class ProjectileBehaviorPathed extends AIBehavior{
    Vector2 path;
    public ProjectileBehaviorPathed(String animID, float speed, Entity me) {
        this.atlas = FinalPrototype1.getAtlas();
        this.batch = FinalPrototype1.getBatch();
        this.speed = speed;
        this.me = me;
        anim = new Animation(10f/speed, atlas.findRegions(animID));
    }

    public ProjectileBehaviorPathed(String animID, float speed, Entity me, Vector2 path){
        this(animID, speed, me);
        this.path = path;
    }

    public void update(float deltaTime){
        passed += deltaTime;
        motion = new Vector2(speed * (float)Math.cos(path.angleRad()), speed * (float)Math.sin(path.angleRad()));
    }

    @Override
    public TextureRegion getCurrentAnimation(){
        return (TextureRegion) anim.getKeyFrame(passed, true);
    }
}

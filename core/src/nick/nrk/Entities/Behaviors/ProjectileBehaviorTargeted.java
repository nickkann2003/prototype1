package nick.nrk.Entities.Behaviors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import nick.nrk.Entities.Entity;
import nick.nrk.FinalPrototype1;

public class ProjectileBehaviorTargeted extends AIBehavior{

    protected Entity target;
    Vector2 acc;
    protected Vector2 trj;
    protected Vector2 dist;
    protected float accFactor;

    public ProjectileBehaviorTargeted(String animID, float speed, Entity me) {
        this.atlas = FinalPrototype1.getAtlas();
        this.batch = FinalPrototype1.getBatch();
        this.speed = speed;
        this.me = me;
        anim = new Animation(10f/speed, atlas.findRegions(animID));
    }

    public ProjectileBehaviorTargeted(String animID, float speed, Entity target, Vector2 vInitial, Entity me){
        this(animID, speed, me);
        this.target = target;
        this.trj = vInitial;
        accFactor = 4;
    }

    public ProjectileBehaviorTargeted(String animID, float speed, Entity target, Vector2 vInitial, Entity me, float accFactor){
        this(animID, speed, target, vInitial, me);
        this.target = target;
        this.trj = vInitial;
        this.accFactor = accFactor;
    }

    public void update(float deltaTime){
        passed += deltaTime;
        dist = new Vector2(target.getX() - me.getX(), target.getY() - me.getY());

        if(motion != null)
            trj = motion;

        acc = new Vector2(speed * (float)Math.cos(dist.angleRad()) * deltaTime * accFactor, speed * (float)Math.sin(dist.angleRad()) * deltaTime * accFactor);

        float l = dist.len()/50;
        if(l < 1){
            acc.scl(l);
        }

        trj.add(acc);

        motion = new Vector2(speed * (float)Math.cos(trj.angleRad()), speed * (float)Math.sin(trj.angleRad()));
    }

    @Override
    public TextureRegion getCurrentAnimation(){
        return (TextureRegion) anim.getKeyFrame(passed, true);
    }

}

package nick.nrk.Entities.Behaviors;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import nick.nrk.Entities.Entity;

public class Melee extends AIBehavior {
    /**
     * Melee AI
     * Sits at its target range (based on size and speed)
     * Has a timer that counts down based on its proximity to the player
     * When the timer hits 0 it charges with increased speed
     */
    boolean charging; //Is entity charging its target
    float cTimer; //Effectively a cooldown for the charge

    public Melee(){
        super();
    }

    public Melee(String animID, float speed, Entity me) {
        super(animID, speed, me);
        targetRange = speed/3f + me.getRange();
        cTimer = 0.2f;
    }

    //Override update function for specific AI behavior
    public void update(float deltaTime){
        //Increment time passed
        passed += deltaTime;
        if(agro){ //If it is agro'd to player
            Vector2 v = new Vector2(target.getX() - me.getX(), target.getY() - me.getY()); //Vector of distance between two
            float theta = v.angleRad(); //Angle between two, in radians
                if(cTimer <= 0) { //If the charge timer is empty
                    if (MathUtils.random(100) < 20) { //20% chance of charging
                        charging = true; //Set charging to true
                    }
                    cTimer = 10f/speed; //Reset charge timer, timer decreased based on enemy move speed
                }
                if(me.isKnocked()){
                    if(cTimer < 10f/speed){
                        //cTimer = 10f/speed;
                    }
                }
            if(target.immune()){ //If the target becomes immune
                //Motion vector becomes a random jitter back and forth, leaning towards the target
                motion = new Vector2((speed * MathUtils.random(-0.3f, 0.6f)) * (float) Math.cos(theta), (speed * MathUtils.random(-0.3f, 0.6f)) * (float) Math.sin(theta));
                charging = false; //It stops charging
                cTimer = 50f/speed; //Charge timer resets
            }else { //If target is not immune
                if(charging) { //If this enemy is charging
                    //Motion set to target with increased speed
                    motion = new Vector2(1.7f*speed * (float) Math.cos(theta), 1.7f*speed * (float) Math.sin(theta));
                }else{ //If not charging
                    if(v.len() > targetRange-5 && v.len() < targetRange + 5){ //If enemy is in sweet spot around target range
                        cTimer -= deltaTime; //cTimer decreased by full delta time
                        motion = new Vector2(0,0); //Stop moving
                    }else if(v.len() < targetRange){ //If distance is shorter than target range
                        float t = v.angleRad();
                        cTimer -= deltaTime/2f; //cTimer slightly decreased by time change
                        //Motion vector backs enemy up slowly to its target range
                        motion = new Vector2(0.75f*speed * (float) Math.cos(t-Math.PI), 0.75f*speed * (float) Math.sin(t-Math.PI));
                    }else{ //If enemy is further than its target range
                        float t = v.angleRad();
                        cTimer -= deltaTime/1.5f; //cTimer decently decreased
                        //Motion has enemy run towards player
                        motion = new Vector2(speed * (float) Math.cos(t), speed * (float) Math.sin(t));
                    }
                }
            }
        }else{ //If not agro
            motion = null; //Set motion to null
            anim = allAnims.get(4); //idle animation
        }
    }

    //Recreate function for melee behavior
    public AIBehavior recreate(String animID, float speed, Entity me){
        return new Melee(animID, speed, me);
    }

}

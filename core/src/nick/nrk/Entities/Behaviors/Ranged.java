package nick.nrk.Entities.Behaviors;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import nick.nrk.Entities.Entity;
import nick.nrk.Entities.Projectile;

public class Ranged extends AIBehavior{
    float sTimer = 3f;

    public Ranged(){
        super();
    }

    public Ranged(String animID, float speed, Entity me) {
        super(animID, speed, me);
        targetRange = me.getRange();
    }

    public void update(float deltaTime){
        passed += deltaTime;
        if(agro) {
            sitInTargetRange();
            if (sTimer <= 0) {
                if(MathUtils.random(100) < 30) {
                    Projectile p = new Projectile(me, new Vector2(target.getX() - me.getX(), target.getY() - me.getY()), me.getDamage(), 6, 55, 1, 10);
                    sTimer = 3f;
                }else{
                    sTimer = 0.15f;
                }
            }
            sTimer -= deltaTime;
        }
    }

    @Override
    public AIBehavior recreate(String animID, float speed, Entity me){
        return new Ranged(animID, speed, me);
    }
}

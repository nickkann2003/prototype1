package nick.nrk.Entities;

/*
* Spawner class that handles spawning of enemies in a room
* Spawns enemies and handles detection location
* Requires a sensor Box2D object to detect player in agro area
 */


import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import nick.nrk.BodyFactory;
import nick.nrk.Entities.Enemy;
import nick.nrk.FinalPrototype1;
import nick.nrk.Rooms.EnemyRoom;
import nick.nrk.Rooms.Room;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Spawner {
    private float x; //x pos
    private float y; //y pos

    private int difficulty; //Difficulty of mobs
    private int numEnemies; //Number of enemies
    private String mobType; //type of mob, based on str/type

    protected EnemyRoom myRoom;

    private World world;

    private List<Enemy> enemies; //All enemies

    private List<Body> layout;

    private List<EArchtype> lineup;

    boolean active;


    public Spawner(float x, float y, int difficulty, EArchtype enemyID, EnemyRoom r){
        this.x = x;
        this.y = y;
        this.difficulty = difficulty;
        this.world = FinalPrototype1.getWorld();
        enemies = new LinkedList<>();
        layout = new ArrayList<>();
        numEnemies = difficulty/enemyID.getStrength();
        for(int i = 0; i < numEnemies; i ++){
            enemies.add(new Enemy(world, new Vector2(x + MathUtils.random(-18-enemyID.getWidth(),18+enemyID.getWidth()), y + MathUtils.random(-18-enemyID.getHeight(), 18+enemyID.getHeight())), enemyID, enemies));
            enemies.get(i).deactivate();
        }
        layout.add(BodyFactory.createSensorBody(x, y, 70, 70, this));
        myRoom = r;
    }

    public Spawner(float x, float y, int difficulty, EnemyRoom r){
        this.x = x;
        this.y = y;
        this.difficulty = difficulty;
        this.world = FinalPrototype1.getWorld();
        enemies = new LinkedList<>();
        layout = new ArrayList<>();
        lineup = EArchtype.populate(difficulty);
        for(EArchtype eID : lineup){
            enemies.add(new Enemy(world, new Vector2(x + MathUtils.random(40+eID.getWidth(),210-eID.getWidth()), y + MathUtils.random(40+eID.getHeight(), 210-eID.getHeight())), eID, enemies));
            r.incrementNumEnemies();
        }
        layout.add(BodyFactory.createSensorBody(x, y, 250, 250, this));
        myRoom = r;
        if(difficulty == -1) {
            enemies.clear();
            enemies.add(new Enemy(world, new Vector2(myRoom.getX() * 250 + 125, myRoom.getY() * 250 + 125), EArchtype.DUMMY));
        }
    }

    public void display() {
        for(Enemy e : enemies){
            e.display();
        }
    }

    public void update(float deltaTime){
        for(Iterator<Enemy> itr = enemies.iterator(); itr.hasNext();){
            Enemy e = itr.next();
            e.update(deltaTime);
            if(e.isDeathFlagged()){
                e.destroy();
                itr.remove();
                myRoom.decrementNumEnemies();
                myRoom.checkClear();
            }
        }
    }

    //Add enemies
    public void addEnemy(Enemy e){
        enemies.add(e);
    }

    public void addEnemy(Vector2 loc, EArchtype eID){
        Enemy e = new Enemy(world, loc, eID, enemies);
        enemies.add(e);
        e.activate();
    }

    public void activate(){
        for(Body b : layout){
            b.setActive(true);
        }
        for(Enemy e : enemies){
            e.activate();
        }
    }

    public void deactivate(){
        for(Body b : layout){
            b.setActive(false);
        }
        for(Enemy e : enemies){
            e.deactivate();
        }
        active = false;
    }

    public boolean checkClear(){
        if(enemies.isEmpty()){
            return true;
        }else{
            return false;
        }
    }


    //When player collides with sensor or interacts with enemy, activate alert function
    public void alert(Entity target){
        if(!active){
            active = true;
            for(Enemy e : enemies){
                e.alert(target);
            }
        }
    }

    public void destroy(){
        for(Body b : layout){
            world.destroyBody(b);
        }
        for(Enemy e : enemies){
            e.destroy();
        }
    }
}

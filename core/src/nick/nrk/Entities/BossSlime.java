package nick.nrk.Entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import nick.nrk.Entities.BossBehaviors.SlimeBoss;

import java.util.List;

import static nick.nrk.utils.Constants.PPM;

public class BossSlime extends Enemy {
    protected Entity target;
    protected boolean dying;
    protected float killTimer; //Timer for death animation
    public BossSlime(World world) {
        super(world);
        this.maxHealth = 100;
        this.health = maxHealth;
        this.x = 250;
        this.y = 250;
        this.damage = 3f;
        this.type = Type.BAS;
        this.width = 64;
        this.height = 64;
        this.speed = 40;
        this.kbResist = 0;
        this.range = 50;
        bhvr = new SlimeBoss(this);
        this.body = createBody();
        projectileSpeed = speed*2.5f;
        deactivate();
        killTimer = 2.5f;
    }

    public BossSlime(World world, Player p) {
        this(world);
        bhvr = new SlimeBoss(this, p);
        target = p;
    }

    @Override
    public void display(){
        if(!dying) {
            if (knocked) {
                Color c = type.getColor();
                batch.setColor(c.r - 0.15f, c.g - 0.15f, c.b - 0.15f, 0.9f);
            } else {
                batch.setColor(((SlimeBoss) bhvr).getMyColor());
            }
            if (!((SlimeBoss) bhvr).isFlipped()) {
                batch.draw(bhvr.getCurrentAnimation(), x - (width + 2) / 2, y - (height + 2) / 2, width + 2, height + 2);
            } else {
                batch.draw(bhvr.getCurrentAnimation(), x + 32, y - 32, 0, 0, 64, 64, 1, -1, 180);
            }
            batch.setColor(Color.WHITE);
        }else{
            ((SlimeBoss)bhvr).kill();
            batch.setColor(((SlimeBoss)bhvr).getMyColor());
            float sz = 64 * ((killTimer+1.5f)/4f);
            batch.draw(bhvr.getCurrentAnimation(), x + MathUtils.random(-2, 2) - sz/2, y + MathUtils.random(-2,2) - sz/2, sz, sz);
            batch.setColor(Color.WHITE);
        }
    }

    @Override
    public void update(float deltaTime){
        super.update(deltaTime);

        if(deathFlag){
            dying = true;
        }

        if(dying){
            killTimer -= deltaTime;
        }

        //MOVEMENT OVERWRITE TO SET SPECIFIC SPEEDS/LOCATIONS
        float xms = 0;
        float yms = 0;
        movement = bhvr.getCurrentMotion();
        if(movement != null){
            xms = movement.x;
            yms = movement.y;
            if(knocked){
                xms = xms/4;
                yms = yms/4;
            }
        }
        //Set velocity
        body.setLinearVelocity((xms + remainingKnockback.x)/PPM, (yms + remainingKnockback.y)/PPM);
    }

    public void giveTarget(Player p){
        this.target = p;
        ((SlimeBoss)bhvr).giveTarget(p);
    }

    public void hitWall(boolean hor){
        ((SlimeBoss)bhvr).hitWall(hor);
    }

    public Vector2 getAngleV(){
        return ((SlimeBoss)bhvr).getAngleV();
    }

    public boolean mustKill(){
        if(dying){
            if(killTimer < 0){
                return true;
            }
        }
        return false;
    }


}

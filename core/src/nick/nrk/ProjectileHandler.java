package nick.nrk;

import nick.nrk.Entities.Projectile;
import nick.nrk.Rooms.Room;

/*
* Handles creation of projectiles within current room
 */
public final class ProjectileHandler {
    protected static Room currentRoom;

    public ProjectileHandler(){
        currentRoom = null;
    }

    public static void createProjectile(Projectile p){
        currentRoom.addProjectile(p);
    }

    public static void removeProjectile(Projectile p){
        currentRoom.removeProjectile(p);
        p.destroy();
    }

    public static void setCurrentRoom(Room newRoom){
        currentRoom = newRoom;
    }
}

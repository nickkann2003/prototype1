package nick.nrk;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

public class MyInputProcessor implements InputProcessor {
    FinalPrototype1 game;
    public MyInputProcessor(FinalPrototype1 game){
        this.game = game;
    }
    public boolean keyDown (int keycode) {
        game.handleDownInput(keycode);
        return false;
    }

    public boolean keyUp (int keycode) {
        game.handleUpInput(keycode);
        return false;
    }

    public boolean keyTyped (char character) {
        return false;
    }

    public boolean touchDown (int x, int y, int pointer, int button) {
        game.handleMouseInput(x, y);
        return false;
    }

    public boolean touchUp (int x, int y, int pointer, int button) {
        return false;
    }

    public boolean touchDragged (int x, int y, int pointer) {
        return false;
    }

    public boolean mouseMoved (int x, int y) {
        game.handleMouseMotion(x, y);
        return false;
    }

    public boolean scrolled (int amount) {
        return false;
    }
}

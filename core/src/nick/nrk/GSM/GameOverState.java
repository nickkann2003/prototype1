package nick.nrk.GSM;

import com.badlogic.gdx.graphics.Color;
import nick.nrk.FinalPrototype1;
import nick.nrk.UI.Button;

public class GameOverState extends State{
    protected int ending;
    protected Button restart;

    public GameOverState(){
        super();
    }

    public GameOverState(int ending){
        this();
        this.ending = ending;
        restart = new Button(750f/2, 500f/2, atlas.findRegion("menu/playButton"), 64, 32, false);
    }

    @Override
    public void display(){
        batch.setColor(Color.BLACK);
        batch.draw(atlas.findRegion("placeholder"), -1, -1, 752, 502);
        batch.setColor(Color.WHITE);
        restart.display();
    }

    @Override
    public void update(float dTime){

    }

    @Override
    public void handleDownInput(int input){

    }

    @Override
    public void handleMouseInput(int x, int y){
        if(restart.checkHovered(x, y)){
            FinalPrototype1.goMenu();
        }
    }

    @Override
    public void handleMouseMotion(int x, int y){

    }

    @Override
    public void dispose(){

    }

    @Override
    public void destroyState(){

    }
}

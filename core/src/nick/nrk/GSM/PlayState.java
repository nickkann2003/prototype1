package nick.nrk.GSM;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import nick.nrk.*;
import nick.nrk.Entities.Player;
import nick.nrk.Floor.Floor;
import nick.nrk.Rooms.BossRoom;
import nick.nrk.Rooms.Room;

import static com.badlogic.gdx.math.MathUtils.random;
import static java.lang.StrictMath.cbrt;
import static nick.nrk.utils.Constants.PPM;

public class PlayState extends State {
    protected Room currentRoom; //Room player is in
    protected Floor currentFloor; //Floor player is on
    protected Map currentMap; //Map of the floor the player is on
    protected Border border;
    protected Player p;

    protected int diff;

    protected Box2DDebugRenderer b2dr;
    protected OrthographicCamera cam;
    protected Matrix4 debugMatrix;

    //Switching room variables
    protected float roomCD;
    protected boolean switching;
    protected Room nextRoom;
    protected int nextDir;

    //Switching floors variables

    public PlayState() {
        super();
    }

    public PlayState(int difficulty){ //Constructor that takes difficulty
        super();
        int numrooms = difficulty/3; //Rooms is diff/3
        if(numrooms > 16){ //Effective until 16 rooms
            numrooms = (int) (16 + cbrt((difficulty-48)/3)); //Then each additional rooms is cbrt(diff/3)
        }
        this.border = new Border(world);
        this.currentFloor = new Floor(numrooms, difficulty, border); //New floor
        this.currentRoom = currentFloor.getBaseRoom(0,0); //Set current room
        p = new Player(world);
        this.currentRoom.enter(p); //Enter it
        this.currentMap = new Map(currentFloor); //Create map
        this.diff = difficulty;
        p.enterFloor();
        ProjectileHandler.setCurrentRoom(currentRoom);

        //Room CD
        roomCD = 0.5f;

        //Debug renderer
        cam = new OrthographicCamera();
        debugMatrix = new Matrix4(cam.combined);
        b2dr = new Box2DDebugRenderer();
        debugMatrix.scale(1/PPM/2, 1/PPM/2, 1f);
        cam.position.set(250,250,0);
        cam.update();

        AudioController.endAmbience();
        AudioController.playAmbience();
    }

    public PlayState(PlayState previous){
        super();
        int prev = previous.getDiff();
        this.diff = (int) (prev*1.9f + 16);
        this.p = previous.p;
        int numrooms = diff/3; //Rooms is diff/3
        if(numrooms > 4){ //Effective until 8 rooms
            numrooms = (int) (4 + cbrt((diff-12)/3)); //Then each additional rooms is cbrt(diff/3)
        }
        while(numrooms > 16){
            numrooms = numrooms/4;
        }
        /*
        * LEVEL GEN INFO:
        * PLAN:
        * Cap out number of rooms per quadrant at 4
        * Continue using current room map sprite
        * When you beat a floor and the number of rooms would be greater than 16, then condense rooms back to 4 at a much higher difficulty
        *
         */
        this.border = previous.getBorder();
        border.cross();
        this.currentFloor = new Floor(numrooms, diff, border); //New floor
        this.currentRoom = currentFloor.getBaseRoom(0,0); //Set current room
        currentRoom.enter(p);
        p.enterFloor();
        this.currentMap = new Map(currentFloor); //Create map
        ProjectileHandler.setCurrentRoom(currentRoom);

        previous.destroyState();

        //Room CD
        roomCD = 0.5f;

        //Debug renderer
        cam = new OrthographicCamera();
        debugMatrix = new Matrix4(cam.combined);
        b2dr = new Box2DDebugRenderer();
        debugMatrix.scale(1/PPM/2, 1/PPM/2, 1f);
        cam.position.set(250,250,0);
        cam.update();

        AudioController.endAmbience();
        AudioController.playAmbience();
    }

    @Override
    public void display(){
        //b2dr.render(world, debugMatrix);
        currentRoom.drawBackground(); //Run backgrounds
        p.display(); //Run player
        currentRoom.drawBorder(); //Run borders
        currentMap.display();

    }

    @Override
    public void update(float dTime){
        world.step(1/60f, 6,2);
        p.update(dTime); //Update player
        currentRoom.update(dTime); //Update room

        roomCD -= dTime; //Decrement roomCD

        currentFloor.checkCleared();

        //If switching rooms
        if(switching) {
            currentMap.switchRoom(currentRoom.getX() + 2 * currentRoom.getY());
            currentRoom.exit(); //Exit current room
            currentRoom = nextRoom; //Switch to next room
            currentRoom.enter(p); //Enter it
            if(currentRoom instanceof BossRoom){ //If the current room is a bossroom
                p.enterBossRoom((BossRoom) currentRoom); //Pass it to specific player funciton, casting it
            }else { //If its not a boss room
                p.enterRoom(currentRoom.getX(), currentRoom.getY(), nextDir); //Proceed as normal
            }
            roomCD = 0.2f; //Activate cooldown
            switching = false; //Stop switching
            if(currentRoom instanceof BossRoom) {
                currentMap.switchRoom(4);
                currentMap.disable();
            }else {

            }
            AudioController.playDoorEnter();
            ProjectileHandler.setCurrentRoom(currentRoom);
            currentMap.setCurrentHidden(currentRoom.getQuadrant());
        }
    }

    @Override //Handles keyboard down input
    public void handleDownInput(int input){
        p.handleDownInput(input);
        currentMap.handleDownInput(input);
        if(input == Input.Keys.ESCAPE){
            FinalPrototype1.gsm.pause();
        }
    }

    @Override //Handles keyboard up input
    public void handleUpInput(int input){
        p.handleUpInput(input);
        currentMap.handleUpInput(input);
    }

    @Override //Handles mouse clicked input
    public void handleMouseInput(int x, int y){
        currentMap.handleMouseInput(x, y);
        p.handleMouseInput(x, y);
    }

    @Override
    public void prepSwitch(){ //Runs right before room is switched out of the stack
        currentRoom.exit();
    }

    public void enterRoom(int drc){
        if(roomCD <= 0){
            switching = true;
            nextRoom = currentFloor.getNextRoom(drc, currentRoom.getX(), currentRoom.getY());
            nextDir = drc;
        }
    }

    //Reveals the next area on the map
    public void seeRoomOnMap(int drc){
        currentMap.showRoomInDirection(drc, currentRoom.getX(), currentRoom.getY());
    }

    //Stops revealing next area on the map
    public void endSeeRoomOnMap(){
        currentMap.showCurrentRoom(currentRoom.getX() + currentRoom.getY()*2);
    }

    public int getDiff(){
        return diff;
    }

    public Border getBorder(){
        return border;
    }

    public Player getPlayer(){return p;}

    @Override
    public void destroyState(){
        currentFloor.destroyFloor();
    }

    @Override
    public void dispose(){
        b2dr.dispose();
    }

    @Override
    public void freeze(){
        p.freeze();
    }

    public void shakeUp(){
        currentFloor.shakeUp();
    }
}

package nick.nrk.GSM;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import nick.nrk.FinalPrototype1;

public class PauseState extends State {

    public PauseState() {
        super();
    }

    @Override
    public void display(){
        batch.setColor(0.2f, 0.2f, 0.2f, 0.3f);
        batch.draw(atlas.findRegion("room/room_background"), 0, 0, 750, 500);
        batch.setColor(Color.WHITE);
    }

    @Override
    public void update(float dTime){

    }

    @Override
    public void handleDownInput(int input){
        if(input == Input.Keys.ESCAPE){
            FinalPrototype1.gsm.unpause();
        }
    }

    @Override
    public void handleUpInput(int input){

    }

    @Override
    public void handleMouseInput(int x, int y){

    }
}

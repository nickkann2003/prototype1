package nick.nrk.GSM;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import nick.nrk.FinalPrototype1;
import nick.nrk.UI.Button;

import static nick.nrk.utils.Constants.w;
import static nick.nrk.utils.Constants.h;

public class MenuState extends State {

    float x = 0;
    float y = 0;
    float xsp = 40;
    float ysp = 40;

    Button playButton;

    boolean info; //Boolean to keep track of displaying either main menu or info menu
    Button infoButton;

    Button backButton;

    Button quitButton;

    public MenuState(){
        playButton = new Button(w/2, h/2+48, atlas.findRegion("menu/playButton"), 64, 32, true);
        infoButton = new Button(w/2, h/2, atlas.findRegion("menu/infoButton"), 64, 32, false);
        backButton = new Button(w/2+4,h/2-256/2, atlas.findRegion("menu/backButton"), 64, 32, false);
        quitButton = new Button(w/2, h/2-48, atlas.findRegion("menu/backButton"), 64, 32, false);
        info = false;
    }

    @Override //Displaying the parts of the menu
    public void display(){

        for(float i = y+504; i > y-1000; i -= 496) {
            batch.draw(atlas.findRegion("menu/menuBackground"), x, i-4);
            batch.draw(atlas.findRegion("menu/menuBackground"), x - 750, i-4);
            batch.draw(atlas.findRegion("menu/menuBackground"), x + 750, i-4);
        }
        if(!info) {
            playButton.display();
            infoButton.display();
            quitButton.display();

        }else{
            batch.draw(atlas.findRegion("menu/instructions"), w/2-352/2, h/2-352/2, 352, 352);
            backButton.display();
        }
    }

    @Override //Prepping switch (Destroy references)
    public void prepSwitch(){ //Prepares the state to be switched out of the stack

    }

    @Override //Any animations within the menu
    public void update(float dTime){

        //Handles the "infinite looping effect" of the menu background
        x -= dTime*xsp;
        y += dTime*ysp;
        if(x > w && xsp < 0)
            x = 0;
        if(x < 0 && xsp > 0)
            x = w;
        if(y < 10 && ysp < 0)
            y = h+6;
        if(y > h-10 && ysp > 0)
            y = -6;
    }

    @Override //Handle keyboard down input
    public void handleDownInput(int input){
        //Handles speed changes based on key presses
        if(input == Input.Keys.W)
            ysp = -40;
        if(input == Input.Keys.A)
            xsp = -40;
        if(input == Input.Keys.S)
            ysp = 40;
        if(input == Input.Keys.D)
            xsp = 40;
    }

    @Override //Handle keyboard up input
    public void handleUpInput(int input){

    }

    @Override //Handle clicking buttons
    public void handleMouseInput(int x, int y){
        if(!info) {
            if (playButton.checkHovered(x, y))
                FinalPrototype1.goNextFloor();
            if (infoButton.checkHovered(x, y))
                FinalPrototype1.goTutorial();
            if(quitButton.checkHovered(x, y)){
                Gdx.app.exit();
            }
        }else{
            if(backButton.checkHovered(x, y))
                info = false;
        }
    }

    @Override
    public void handleMouseMotion(int x, int y){
        if(!info) {
            playButton.checkHovered(x, y);
            infoButton.checkHovered(x, y);
            quitButton.checkHovered(x, y);
        }else{
            backButton.checkHovered(x, y);
        }
    }
}

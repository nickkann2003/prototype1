package nick.nrk.GSM;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import nick.nrk.*;
import nick.nrk.Entities.Player;
import nick.nrk.Floor.TutorialFloor;
import nick.nrk.UI.PopUpText;

import static nick.nrk.utils.Constants.PPM;

public class TutorialState extends PlayState{
    /**
     * TUTORIAL STATE
     * A simplified version of a PlayState that is accompanied by
     * Helpful text pop-ups explaining the different features of the game
     */

    protected SpriteBatch batch;
    protected TextureAtlas atlas;
    protected BitmapFont font;

    protected float passed;

    protected PopUpText test;

    protected PopUpText mainText;

    protected int currentMessage;

    //Variables to track progress
    protected int roomsEntered;
    protected boolean enteredItemRoom;
    protected boolean enteredEnemyRoom;
    protected boolean defeatedFirst;
    protected boolean changedType;
    protected boolean defeatedSecond;
    protected boolean defeatedLast;

    public TutorialState(){
        super();
        passed = 0;

        this.border = new Border(world);
        border.cross();
        p = new Player(world);
        this.diff = 0;
        this.currentFloor = new TutorialFloor(0, 0, border, this);
        this.currentRoom = currentFloor.getBaseRoom(0, 0);
        p.setHideShakeUpCounter(true);
        currentRoom.enter(p);
        p.enterRoom(0,0,0);
        this.currentMap = new Map(currentFloor);
        ProjectileHandler.setCurrentRoom(currentRoom);

        cam = new OrthographicCamera();
        debugMatrix = new Matrix4(cam.combined);
        b2dr = new Box2DDebugRenderer();
        debugMatrix.scale(1/PPM/2, 1/PPM/2, 1f);
        cam.position.set(250,250,0);
        cam.update();

        AudioController.endAmbience();
        AudioController.playAmbience();

        mainText = new PopUpText(this, "WELCOME TO DISORIENTED! TO START, TRY WALKING AROUND A BIT");
        setMessage(0);
        currentMessage = 0;

        //Progress Trackers
        roomsEntered = 0;
    }

    /*
    * Display and Update
     */
    @Override
    public void display() {
        super.display();
        mainText.display();
    }

    @Override
    public void update(float dTime) {
        super.update(dTime);
        mainText.update(dTime);
        passed += dTime;
        if(roomsEntered == 4 && currentMessage == 0){
            setMessage(1);
            ((TutorialFloor)currentFloor).revealItemRoom();
            passed = 0;
        }
        if(currentMessage == 1 && (roomsEntered >= 8 || passed >= 17.5)){
            setMessage(2);
            roomsEntered = 0;
            passed = 0;
        }
        if(roomsEntered == 1 && currentMessage == 3){
            setMessage(4);
            roomsEntered = 0;
        }
        if(currentMessage == 5 && passed >= 7){
            setMessage(6);
            ((TutorialFloor)currentFloor).revealEnemyRoom();
            passed = 0;
            roomsEntered = 0;
        }
        if(currentMessage == 7 && passed > 7.25){
            setMessage(8);
        }

        if(currentMessage == 9 && passed > 8.25f){
            setMessage(10);
            passed = 0;
        }
        if(currentMessage == 10 && passed > 8.25){
            setMessage(11);
        }
        if(currentMessage == 12 && passed > 8f){
            setMessage(13);
            passed = 0;
        }
        if(currentMessage == 13 && passed > 8f){
            setMessage(14);
            passed = 0;
        }
        if(currentMessage == 15 && passed > 7.25f){
            setMessage(16);
            passed = 0;
        }
        if(currentMessage == 16 && passed > 6.5f){
            setMessage(17);
            passed = 0;
        }
        if(currentMessage == 18 && passed > 8){
            setMessage(19);
            passed = 0;
        }
        if(currentMessage == 19 && passed > 8.3f){
            setMessage(20);
            passed = 0;
        }
    }

    /*
    * Input Handler Functionss
     */
    @Override
    public void handleDownInput(int input) {
        super.handleDownInput(input);
    }

    @Override
    public void handleUpInput(int input) {
        super.handleUpInput(input);
    }

    @Override
    public void handleMouseInput(int x, int y) {
        super.handleMouseInput(x, y);
    }

    //Enter Room Override
    @Override
    public void enterRoom(int drc) {
        super.enterRoom(drc);
        mainText.adjustPosition(drc);
    }

    //Set current tutorial message
    /*
    * 0 - Intro Message
    * 1 - TAB Instructions
    * 2 - Door instructions
    * 3 - Room Found
    * 4 - Map Revealed/Re-enter room
    * 5 -
    * 6 -
    * 7
    * 8
    * 9
    *
    * PUSH NOTIFICATIONS: 10-19
    * 10 - Push to use Entrances
    * 11
     */
    public void setMessage(int index){
        mainText.hide();
        if(index == 0){
            mainText.rePurpose("WELCOME. CURRENTLY, YOU ARE NOWHERE. MAYBE IF YOU WANDER YOU MIGHT END UP SOMEWHERE");
        }
        if(index == 1){
            mainText.rePurpose("THAT DOESN'T SEEM TO BE WORKING. TRY HOLDING [TAB] AS YOU WALK, IT WILL SHOW YOU THE ROOMS AROUND YOU");
        }
        if(index == 2){
            mainText.rePurpose("DO YOU JUST WALK INTO WALLS HOPING TO FIND A DOOR? [TAB] SHOWS YOU THE ENTRANCE TO ROOMS ASWELL");
        }
        if(index == 3){
            mainText.rePurpose("WELL DONE, YOU FOUND AN ITEM ROOM. COLLECT THE ITEM IN THE CENTER BY WALKING INTO IT, THEN EXIT THROUGH ANY DOOR");
        }
        if(index == 4){
            mainText.rePurpose("HOLDING [TAB] NOW WILL REVEAL THE COLOR OF THE ROOM YOU JUST FOUND. TRY MAKING YOUR WAY BACK INTO THE ROOM");
        }
        if(index == 5){
            mainText.rePurpose("GOOD. REMEMBER, THERE IS ONLY ONE WAY INTO EVERY ROOM, BUT YOU CAN EXIT FROM ANY DIRECTION");
        }
        if(index == 6){
            mainText.rePurpose("NOW, USE [TAB] TO FIND THE NEXT ROOM");
        }
        if(index == 7){
            mainText.rePurpose("INCASE THE RED WASN'T OBVIOUS ENOUGH, THIS ROOM IS DANGEROUS. USUALLY, YOU WOULD FIND A NUMBER OF FOES IN A ROOM LIKE THIS");
        }
        if(index == 8){
            mainText.rePurpose("LUCKILY FOR YOU, THIS ONE JUST HAS A TRAINING DUMMY IN IT. SHOOT THE DUMMY UNTIL IT DIES");
        }
        if(index == 9){
            mainText.rePurpose("WELL DONE. ENEMIES CAN COME IN A VARIETY OF TYPES, AND YOU WILL HAVE TO CHANGE YOUR TYPE IN ORDER TO DEAL WITH THEM");
        }
        if(index == 10){
            mainText.rePurpose("PRESSING [Q] OR [E] WILL CYCLE THROUGH AVAILABLE TYPES, AND PRESSING [SPACE] WILL LOCK IN THE ONE ABOVE YOUR HEAD");
        }
        if(index == 11){
            mainText.rePurpose("TRY LOCKING IN WATER, THE BLUE TYPE, NOW");
        }
        if(index == 12){
            mainText.rePurpose("CHANGING TYPES SENDS OUT A BURST OF PROJECTILES, BUT PREVENTS YOU FROM SHOOTING AND CHANGING FOR A SHORT TIME");
        }
        if(index == 13){
            mainText.rePurpose("THESE DUMMYS HAVE THE SAME HEALTH, BUT THE BROWN ONE TAKES REDUCED DAMAGE FROM WATER, WHILE THE RED ONE TAKES INCREASED DAMAGE FROM IT");
        }
        if(index == 14){
            mainText.rePurpose("COMPARE THE AMOUNT OF PROJECTILES IT TAKES TO DEFEAT EACH");
        }
        if(index == 15){
            mainText.rePurpose("CHANGING YOUR TYPE IS ESSENTIAL TO SURVIVAL. IT CAN INCREASE THE DAMAGE YOU DEAL, WHILE DECREASING THE DAMAGE YOU TAKE");
        }
        if(index == 16){
            mainText.rePurpose("ON THE FLIP SIDE, SELECTING THE WRONG TYPE CAN MAKE EVEN SIMPLE ENCOUNTERS DEADLY");
        }
        if(index == 17){
            mainText.rePurpose("TRY EXPERIMENTING WITH THE 4 MAIN TYPES NOW");
        }
        if(index == 18){
            mainText.rePurpose("WELL DONE, YOU SEEM READY TO GO. ONCE YOU HAVE CLEARED EVERY AVAILABLE ROOM ON A GIVEN FLOOR, THE FINAL CHAMBER UNLOCKS");
            FinalPrototype1.gsm.pause();
        }
        if(index == 19){
            mainText.rePurpose("HOLDING TAB WILL REVEAL THE ENTRANCE TO THE FINAL ROOM. UPON ENTERING, THE REST WILL BE UP TO YOU");
        }
        if(index == 20){
            mainText.rePurpose("IF YOU SUCCEED, YOU WILL BE REWARDED, AND AN ENTRANCE WILL OPEN AT THE TOP OF THE ROOM, ALLOWING YOU TO CONTINUE YOUR DELVE");
            FinalPrototype1.gsm.unpause();
        }
        mainText.reveal();
        currentMessage = index;
    }

    public void incrementEntered(){
        roomsEntered += 1;
    }

    public void enterItemRoom(){
        if(!enteredItemRoom)
            setMessage(3);
        roomsEntered = 0;
        enteredItemRoom = true;
        if(currentMessage == 4){
            setMessage(5);
            passed = 0;
        }
    }

    public void enterEnemyRoom(){
        if(!enteredEnemyRoom) {
            setMessage(7);
            passed = 0;
            enteredEnemyRoom = true;
        }
    }

    public void enterBossRoom(){
        mainText.rePurpose("how did you find this?");
        mainText.hide();
    }

    public void defeatedFirst(){
        if(!defeatedFirst){
            setMessage(9);
            passed = 0;
            defeatedFirst = true;
        }
    }

    public void defeatedSecond(){
        if(!defeatedSecond){
            setMessage(15);
            passed = 0;
        }
    }

    public void defeatedLast(){
        if(!defeatedLast){
            defeatedLast = true;
            passed = 0;
            setMessage(18);
        }
    }

    public void changedType(){
        if(!changedType){
            changedType = true;
            passed = 0;
            setMessage(12);
        }
    }
}

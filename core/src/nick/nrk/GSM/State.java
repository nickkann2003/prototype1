package nick.nrk.GSM;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.physics.box2d.World;
import nick.nrk.FinalPrototype1;

/**
 * Class representing a state of the game
 * Ex. Menu, Playing, Paused
 * Private variables can be accessed by all instances of a given class
 * Can pass a previous state into a new state and create the new state based on the one that was just cleared
 */
public class State {
    protected SpriteBatch batch;
    protected TextureAtlas atlas;
    protected World world;

    public State(){
        batch = FinalPrototype1.getBatch(); //Batch
        atlas = FinalPrototype1.getAtlas(); //Atlas
        world = FinalPrototype1.getWorld(); //World
    }


    public void display(){
        //Default display image, shown for debug if a base state is displayed
        batch.draw(atlas.findRegion("player_sitting_left"), 0, 0, 750, 500);
    }

    public void prepSwitch(){ //Prepares the state to be switched out of the stack

    }

    public void update(float dTime){

    }

    public void handleDownInput(int input){

    }

    public void handleUpInput(int input){

    }

    public void handleMouseInput(int x, int y){

    }

    public void handleMouseMotion(int x, int y){

    }

    public void dispose(){

    }

    public void destroyState(){

    }

    public void freeze(){

    }
}

package nick.nrk.GSM;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import nick.nrk.AudioController;
import nick.nrk.Border;
import nick.nrk.Entities.Entity;
import nick.nrk.Entities.EntityCollisionHandler;
import nick.nrk.Entities.Player;
import nick.nrk.FinalPrototype1;

import java.util.Stack;

/***
 * Game State Manager class
 * This class will handle displaying current state,
 * Switching states and other stuff
 */
public class GameStateManager {
    private Stack<State> gameStates; //Stack of game states
    private boolean switchingFloors = false;
    private float floorSwitchTimer = 0; //Timer for switching floor animation

    protected SpriteBatch batch;
    protected TextureAtlas atlas;

    //Ending variables
    protected boolean ending;
    protected float endTimer;

    public GameStateManager(){
        gameStates = new Stack<State>();
        this.batch = FinalPrototype1.getBatch();
        this.atlas = FinalPrototype1.getAtlas();
        ending = false;
    }

    //Displays the current State
    public void displayState(){//Public function to display the current state
        if(!gameStates.isEmpty()){ //Check if the stack is empty
            if(gameStates.peek() instanceof PauseState){ //If top state is a PausedState
                PauseState p = (PauseState) gameStates.pop(); //Hold top state
                if(!gameStates.isEmpty()){ //Make sure there is still another state
                    State s = gameStates.peek();
                    gameStates.push(p);
                    if(s instanceof TutorialState){
                        s.update(Gdx.graphics.getDeltaTime());
                    }
                    s.display(); //Only display the next state
                    if(!switchingFloors) {
                        p.update(Gdx.graphics.getDeltaTime()); //Update popped state
                    }
                    p.display(); //Display popped state
                }
            }else{ //Game state is not a PausedState
                State temp = gameStates.peek(); //Grab reference to top state
                if(!switchingFloors)
                    temp.update(Gdx.graphics.getDeltaTime()); //Update it
                temp.display(); //Display it
            }
        }
        if(floorSwitchTimer >= 0){
            floorSwitchTimer -= Gdx.graphics.getDeltaTime();
        }
        if(endTimer >= 0){
            endTimer -= Gdx.graphics.getDeltaTime();
        }
        //Check floor switching
        if(switchingFloors){
            batch.setColor(0f,0f,0f,(2.89f-floorSwitchTimer)/2.89f);
            batch.draw(atlas.findRegion("placeholder"), -100, -100, 1900, 1400);
            batch.setColor(Color.WHITE);
            
            gameStates.peek().freeze();

            if(floorSwitchTimer <= 0) {
                performFloorSwitch();
                switchingFloors = false;
            }
        }

        //Check if the game is ending
        if(ending){
            batch.setColor(0f,0f,0f,(2f-endTimer)/2f);
            batch.draw(atlas.findRegion("placeholder"), -100, -100, 1900, 1400);
            batch.setColor(Color.WHITE);
            gameStates.peek().freeze();

            if(endTimer <= 0) {
                while (!gameStates.empty()) {
                    State s = gameStates.pop();
                    s.destroyState();
                }
                gameStates.push(new GameOverState(0));
                ending = false;
            }
        }
    }

    //Public function that handles keyboard down input
    public void handleDownInput(int key){
        if(!switchingFloors)
            gameStates.peek().handleDownInput(key);
    }
    //Public function that handles keyboard release input
    public void handleUpInput(int key){
        if(!switchingFloors)
            gameStates.peek().handleUpInput(key);
    }

    //Public function that handles mouse input
    public void handleMouseInput(int x, int y){
        if(!switchingFloors)
            gameStates.peek().handleMouseInput(x,y);
    }
    //Public function handles motion of the mouse
    public void handleMouseMotion(int x, int y){
        if(!switchingFloors)
            gameStates.peek().handleMouseMotion(x, y);
    }

    //Public function to collide two entities
    public void collideEntities(Entity e1, Entity e2){
        if(!ending && gameStates.peek() instanceof PlayState) {
            EntityCollisionHandler.HandleCollision(e1, e2);
        }
    }

    //Public function that handles entering a room
    public void enterRoom(int drc){ //Takes a direction
        State temp = gameStates.peek(); //Get top state
        if(temp instanceof PlayState){ //Check if it is a PlayState
            ((PlayState)temp).enterRoom(drc); //Cast it and then enter room
        }
    }

    //Public functions that handles room revealing
    public void seeRoomOnMap(int drc){
        State temp = gameStates.peek();
        if(temp instanceof PlayState){
            ((PlayState)temp).seeRoomOnMap(drc);
        }
    }
    //Ends vision of the next room in direction
    public void endSeeRoomOnMap(){
        State temp;
        if(!gameStates.empty()) {
            temp = gameStates.peek();
            if (temp instanceof PlayState) {
                ((PlayState) temp).endSeeRoomOnMap();
            }
        }
    }


    //Goes to the next floor of the playState
    public void goNextFloor(){
        switchingFloors = true;
        floorSwitchTimer = 2.89f;
        AudioController.playReveal2();
    }

    //Performs manual functions of the floor switch
    private void performFloorSwitch(){
        State previous;
        if(!gameStates.isEmpty()) {  //If stack is not empty
            previous = gameStates.pop(); //Set previous
        }else{
            gameStates.push(new PlayState(12)); //Push a new PlayState of difficulty 1
            return;
        }
        previous.prepSwitch();
        if(previous instanceof PlayState){ //If it is a PlayState (Not a menu state or other)
            gameStates.push(new PlayState((PlayState)previous)); //Push a new state, taking previous as param
        }else{ //If it is not a PlayState
            gameStates.push(new PlayState(12)); //Push a new PlayState of difficulty 1
        }
    }

    //Returns the border if current state is a PlayState
    public Border getBorder(){
        State temp = gameStates.peek();
        if(temp instanceof PlayState){
            return ((PlayState)temp).getBorder();
        }
        return null;
    }

    //Dispose
    public void dispose(){
        while(!gameStates.isEmpty()){
            gameStates.pop().dispose();
        }
    }

    //Starts the GSM from a menu state
    public void start(){
        gameStates.push(new MenuState());
    }

    //Pushes a pause state onto the stack
    public void pause(){
        gameStates.push(new PauseState());
    }

    //Removes the top state if it is a pause state
    public void unpause(){
        if(gameStates.peek() instanceof PauseState){
            gameStates.pop();
        }
    }

    //Sends game to Game End screen
    public void endGame(int ending){
        if(!this.ending) {
            this.ending = true;
            endTimer = 2f;
        }
    }

    //Sends game to Menu Screen
    public void goMenu(){
        while(!gameStates.empty()){
            gameStates.pop().destroyState();
        }
        gameStates.push(new MenuState());
        AudioController.endAmbience();
    }

    //Sends the game to a tutorial state
    public void goTutorial(){
        while(!gameStates.empty()){
            gameStates.pop().destroyState();
        }
        gameStates.push(new TutorialState());
    }

    public void shakeUp(){
        if(gameStates.peek() instanceof PlayState){
            ((PlayState)gameStates.peek()).shakeUp();
        }
    }

    public Player getPlayer(){
        if(gameStates.peek() instanceof PlayState){
            return ((PlayState) gameStates.peek()).getPlayer();
        }else{
            return null;
        }
    }
}

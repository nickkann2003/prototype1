package nick.nrk.Rooms;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import nick.nrk.Entities.EArchtype;
import nick.nrk.Entities.Player;
import nick.nrk.Entities.Spawner;
import nick.nrk.Entities.Type;
import nick.nrk.FinalPrototype1;
import nick.nrk.GSM.TutorialState;

import java.util.ArrayList;

public class TutorialEnemyRoom extends EnemyRoom{

    protected TutorialState myState;
    protected float passed;
    protected float numLeft; //Tracker for dummys defeated, used for continuing tutorial
    Player p;

    public TutorialEnemyRoom(){
        super(FinalPrototype1.getWorld());
        spawns = new ArrayList<>();
        spawns.add(new Spawner(250*roomX, 250*roomY, -1, this));
        spawns.get(0).deactivate();
        roomColor = eRoomColor; //Tint red
        entrance = 0;
        passed = 0;
    }

    public TutorialEnemyRoom(int x, int y, int ent){
        this();
        this.entrance = ent;
        this.roomX = x;
        this.roomY = y;
    }

    public TutorialEnemyRoom(World world) {
        super(world);
    }

    public TutorialEnemyRoom(World world, int x, int y) {
        super(world, x, y);
        spawns = new ArrayList<>();
        spawns.add(new Spawner(250*roomX, 250*roomY, -1, this));
        spawns.get(0).deactivate();
        roomColor = eRoomColor; //Tint red
        entrance = 0;
        enemiesRemaining = 7;
        numLeft = 3;
    }

    public TutorialEnemyRoom(World world, int x, int y, Player p) {
        this(world, x, y);
        this.p = p;
    }

    public TutorialEnemyRoom(World world, int x, int y, Player p, TutorialState state) {
        this(world, x, y, p);
        this.myState = state;
    }

    public TutorialEnemyRoom(World world, int x, int y, int difficulty) {
        super(world, x, y, difficulty);
    }


    @Override
    public void update(float deltaTime){
        super.update(deltaTime);
        passed += deltaTime;
        if(numLeft == 3 && enemiesRemaining == 6 && p.getType().equals(Type.WTR) && passed >= 16){
            spawns.get(0).addEnemy(new Vector2(roomX*250 + 50, roomY*250+125), EArchtype.DUMMYE);
            spawns.get(0).addEnemy(new Vector2(roomX*250 + 200, roomY*250+125), EArchtype.DUMMYF);
            myState.changedType();
            numLeft = 2;
        }
        if(numLeft == 2 && passed >= 15 && enemiesRemaining == 4){
            spawns.get(0).addEnemy(new Vector2(roomX*250 + 75, roomY*250+175), EArchtype.DUMMYE);
            spawns.get(0).addEnemy(new Vector2(roomX*250 + 175, roomY*250+175), EArchtype.DUMMYW);
            spawns.get(0).addEnemy(new Vector2(roomX*250 + 175, roomY*250+75), EArchtype.DUMMYF);
            spawns.get(0).addEnemy(new Vector2(roomX*250 + 75, roomY*250+75), EArchtype.DUMMYN);
            numLeft = 1;
        }
    }

    @Override
    public void incrementNumEnemies(){
        //Adding more enemies does not increment number of enemies, will always be X
    }

    @Override
    public void increaseNumEnemies(int num){
        super.increaseNumEnemies(num);
    }

    @Override
    public void decrementNumEnemies(){
        super.decrementNumEnemies();
        if(enemiesRemaining ==6)
            myState.defeatedFirst();
        if(enemiesRemaining == 4)
            myState.defeatedSecond();
        if(enemiesRemaining == 0)
            myState.defeatedLast();
        passed = 0;
    }

    @Override
    public void decreaseNumEnemies(int num){
        super.decreaseNumEnemies(num);
    }
}

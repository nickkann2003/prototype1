package nick.nrk.Rooms;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.World;
import nick.nrk.Entities.Player;
import nick.nrk.Items.Item;

public class ItemRoom extends Room {
    protected Color iRoomColor = new Color(0.1f, 0.6f, 0.1f, 1f);
    protected Item item;

    public ItemRoom(World world) {
        super(world);
        roomColor = iRoomColor; //Tint red
    }

    public ItemRoom(World world, int x, int y) {
        super(world, x, y);
        roomColor = iRoomColor; //Tint red
        item = Item.randomize(roomX, roomY);
        item.deactivate();
    }

    public ItemRoom(World world, int x, int y, int difficulty) {
        super(world, x, y, difficulty); //Super constructor
        roomColor = iRoomColor; //Tint red
        item = Item.randomize(roomX, roomY);
        item.deactivate();
    }

    @Override
    public void enter(Player p){
        super.enter(p);
        item.activate();
    }

    @Override
    public void exit(){
        super.exit();
        item.deactivate();
    }

    @Override
    public void drawBorder(){
        super.drawBorder();
        item.display();
    }

    @Override
    public void update(float deltaTime){
        super.update(deltaTime);
        if(!cleared){
            checkClear();
        }
        item.update(deltaTime);
    }
}

package nick.nrk.Rooms;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.*;
import nick.nrk.BodyFactory;
import nick.nrk.Entities.Player;
import nick.nrk.Entities.Projectile;
import nick.nrk.ProjectileHandler;

public class BaseRoom extends Room {
    public BaseRoom(World world, int x, int y){ //Base constructor, initializes walls to correct locations
        super(world, x, y); //Call super of constructor
        cleared = true;
    }

    public BaseRoom(World world, int x, int y, int difficulty){ //Constructor that takes difficulty but ignores it
        super(world, x, y, difficulty);
        cleared = true;
    }

    @Override
    public void drawBackground(){
        super.drawBackground();
        batch.setColor(1,1,1,0.35f);
        batch.draw(atlas.findRegion("room/instructions1"), roomX*250+20, (roomY)*250+140);
        batch.draw(atlas.findRegion("room/instructions2"), roomX*250 + 140, roomY*250 + 140);
        batch.draw(atlas.findRegion("room/instructions3"), roomX*250 + 20, roomY*250 + 20);
        batch.draw(atlas.findRegion("room/instructions4"), roomX*250 + 140, roomY*250 + 20);
        batch.setColor(Color.WHITE);
    }

    @Override
    protected void createLayout(){ //Creates the physical layout within the Box2D world
        layout.addAll(BodyFactory.createDoorsArrayList(roomX, roomY));
        layout.addAll(BodyFactory.createRoomMapSensors(roomX, roomY));
    }

    public void enter(Player p){
        super.enter(p);
    }

    public void exit(){
        super.exit();
        for(Projectile p : projectiles){
            p.destroy();
        }
        projectiles.clear();
    }
}

package nick.nrk.Rooms;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.World;
import nick.nrk.Rooms.Room;

public class PuzzleRoom extends Room {
    protected Color pRoomColor = new Color(0.1f, 0.1f, 0.5f, 1f);

    public PuzzleRoom(World world) {
        super(world);
        roomColor = pRoomColor; //Tint red
    }

    public PuzzleRoom(World world, int x, int y) {
        super(world, x, y);
        roomColor = pRoomColor; //Tint red
    }

    public PuzzleRoom(World world, int x, int y, int difficulty) {
        super(world, x, y, difficulty); //Super constructor
        roomColor = pRoomColor; //Tint red
    }

    public void update(float deltaTime){
        super.update(deltaTime);
        if(!cleared){
            checkClear();
        }
    }
}

package nick.nrk.Rooms;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.Body;
import nick.nrk.AudioController;
import nick.nrk.BodyFactory;
import nick.nrk.Border;
import nick.nrk.Entities.BossSlime;
import nick.nrk.Entities.Player;
import nick.nrk.Entities.Projectile;
import nick.nrk.FinalPrototype1;
import nick.nrk.Items.Item;

import static nick.nrk.utils.Constants.roomHeight;
import static nick.nrk.utils.Constants.roomWidth;

public class BossRoom extends Room {
    private int difficulty;
    private Border border;
    protected BossSlime boss;
    protected Item loot;

    /*
    *   0     1
    * 2           6
    *
    * 3           7
    *   4     5
     */

    public BossRoom(World world, Border b) {
        super(world);
        border = b;
        entrance = 4;
        entrance = MathUtils.random(7);
        boss = new BossSlime(world);
        loot = Item.randomize(0.5f,0.5f);
        loot.deactivate();
    }

    public BossRoom(World world, int x, int y) {
        super(world, x, y);
        this.roomX = 0;
        this.roomY = 0;
    }

    public BossRoom(World world, int x, int y, int difficulty) {
        super(world, x, y, difficulty);
    }

    public boolean checkEnter(int dir, int roomX, int roomY){ //Checks entrance of boss room, has to consider current room location
        if(dir == 0){ //Going up
            if(entrance == 4 && roomX == 0 && roomY == 1) //In top left, going up, entrance in bottom left
                return true;
            if(entrance == 5 && roomX == 1 && roomY == 1) //In top right, going up, entrance in bottom right
                return true;
        }else if(dir == 1){ //Going left
            if(entrance == 6 && roomX == 0 && roomY == 1) //In top left, going left, entrance top right
                return true;
            if(entrance == 7 && roomX == 0 && roomY == 0) //In bottom left, going left, entrance bottom right
                return true;
        }else if(dir == 2){ //Going down
            if(entrance == 0 && roomX == 0 && roomY == 0) //In bottom left, going down, entrance top left
                return true;
            if(entrance == 1 && roomX == 1 && roomY == 0) //In bottom right, going down, entrance top right
                return true;
        }else if(dir == 3){ //Going right
            if(entrance == 2 && roomX == 1 && roomY == 1) //In top right, going right, entrance top left
                return true;
            if(entrance == 3 && roomX == 1 && roomY == 0) //In bottom right, going right, entrance bottom left
                return true;
        }
        return false;
    }

    @Override
    public void drawBackground(){
        batch.setColor(0.5f, 0.4f, 0.6f, 1f);
        batch.draw(atlas.findRegion("room/room_background"), roomX, roomY, roomWidth*2, roomHeight*2);
        batch.setColor(Color.WHITE);
        if(!cleared)
            boss.display();
        for(Projectile p : projectiles){
            p.display();
        }
        if(cleared) {
            batch.draw(atlas.findRegion("room/door"), 300, 500, 0, 0, 100, 40, 1, 1, 180);
            loot.display();
        }
    }

    @Override
    public void drawBorder(){
        batch.draw(atlas.findRegion("room/room_border"), roomX, roomY, roomWidth*2, roomHeight*2);
    }

    @Override
    public void update(float deltaTime){
        super.update(deltaTime);
        if(!cleared) {
            boss.update(deltaTime);
            if (boss.mustKill()) {
                world.destroyBody(boss.getBody());
                boss = null;
                clearRoom();
                loot.activate();
            }
        }
        loot.update(deltaTime);
    }

    @Override
    public void enter(Player p){
        border.clear();
        activateLayout();
        boss.activate();
        timesEntered ++;
        AudioController.endBossDoorSee();
        boss.giveTarget(p);
    }

    @Override
    public void exit(){
        border.cross();
        deactivateLayout();
    }

    @Override //Override for the creation of the layout
    protected void createLayout(){
        layout.addAll(BodyFactory.createBossExitDoor());
    }

    @Override //Overrides the clear room function
    public void clearRoom(){
        if(!cleared) {
            cleared = true;
            layout.addAll(BodyFactory.createBossExitDoor());
        }
    }

    public int getEntrance(){return entrance;}
}

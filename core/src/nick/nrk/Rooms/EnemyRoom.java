package nick.nrk.Rooms;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.World;
import nick.nrk.Entities.EArchtype;
import nick.nrk.Entities.Player;
import nick.nrk.Entities.Spawner;
import nick.nrk.FinalPrototype1;

import java.util.ArrayList;

import static com.badlogic.gdx.math.MathUtils.random;

public class EnemyRoom extends Room {
    protected ArrayList<Spawner> spawns;
    protected int dif;
    protected int enemiesRemaining;
    protected Color eRoomColor = new Color(0.6f, 0.1f, 0.1f, 1f);

    public EnemyRoom(World world) {
        super(world);
    }

    public EnemyRoom(World world, int x, int y) {
        super(world, x, y);
        roomColor = eRoomColor; //Tint red
    }

    public EnemyRoom(World world, int x, int y, int difficulty) {
        super(world, x, y, difficulty); //Super constructor
        spawns = new ArrayList<>();
        enemiesRemaining = 0;
        spawns.add(new Spawner(250*roomX, 250*roomY, difficulty, this));
        spawns.get(0).deactivate();
        roomColor = eRoomColor; //Tint red
    }

    public void drawBackground(){
        super.drawBackground();
        for(Spawner s : spawns){
            s.display();
        }
    }

    public void update(float deltaTime){
        super.update(deltaTime);
        for(Spawner s : spawns){
            s.update(deltaTime);
        }
    }

    @Override
    public void checkClear(){
        if(enemiesRemaining <= 0){
            clearRoom();
        }
    }

    public void enter(Player p){
        super.enter(p);
        for(Spawner s : spawns){
            s.activate();
            s.alert(p);
        }
    }

    public void exit(){
        super.exit();
        for(Spawner s : spawns){
            s.deactivate();
        }
    }

    public void destroyRoom(){
        super.destroyRoom();
        for(Spawner s : spawns){
            s.destroy();
        }
    }

    //Increment enemies num
    public void incrementNumEnemies(){
        enemiesRemaining += 1;
    }

    //Increase numEnemies by amount
    public void increaseNumEnemies(int num){
        enemiesRemaining += num;
    }

    //Increment enemies num
    public void decrementNumEnemies(){
        enemiesRemaining -= 1;
    }

    //Increase numEnemies by amount
    public void decreaseNumEnemies(int num){
        enemiesRemaining -= num;
    }
}

package nick.nrk.Rooms;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import nick.nrk.BodyFactory;
import nick.nrk.Entities.Player;
import nick.nrk.Entities.Projectile;
import nick.nrk.FinalPrototype1;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static nick.nrk.utils.Constants.roomHeight;
import static nick.nrk.utils.Constants.roomWidth;

public class Room {
    protected boolean cleared; //Every room needs to know if it is cleared (base room is always cleared)
    //With calculations for room slection, BaseRooms are not included in room array, they will always fall in lowest priority

    protected boolean canEnter; //Boolean stating if the room can be entered or not

    protected int timesEntered = 0; //Num times entered room

    protected Sprite roomMapSprite;
    protected Sprite roomDoorSprite;

    protected boolean currentRoom = false;

    protected Color roomColor;

    protected int difficulty; //Difficulty variable to determine chellenge of room
    //Base Rooms always have a difficulty of zero, Puzzle rooms will have scaling difficulty:
    // Ex. 1-5 puzzle 1, 5-10 puzzle 2
    //Loot rooms difficulty determines strength of items

    protected World world; //Reference to world

    protected int roomX; //X coord, left to right
    protected int roomY; //Y coord, bottom to top

    protected int entrance;

    protected ArrayList<Body> layout; //Array of all obstacles in the room
    protected int pattern; //Pattern for layout of obstacles in room

    protected SpriteBatch batch;
    protected TextureAtlas atlas;

    protected List<Projectile> projectiles;
    protected List<Projectile> adding;

    public Room(World world){ //Most basic constructor
        this.world = world;
        this.batch = FinalPrototype1.getBatch();
        this.atlas = FinalPrototype1.getAtlas();
        canEnter = true;
        layout = new ArrayList<Body>(); //Array list for layout
        pattern = MathUtils.random(2); //Get a random int for layout of room
        entrance = MathUtils.random(3); //Random int for entrance

        roomColor = Color.WHITE; //Set tint to blank

        roomMapSprite = new Sprite(atlas.findRegion("room/room_map_background")); //Map background

        TextureAtlas.AtlasRegion doorType;
        if(entrance%2 == 0) {
            doorType = atlas.findRegion("room/map_door_hr");
            roomDoorSprite = new Sprite(doorType);
        }else{
            doorType = atlas.findRegion("room/map_door_vr");
            roomDoorSprite = new Sprite(doorType);
        }
        roomDoorSprite = new Sprite(doorType); //Map door
        projectiles = new LinkedList<>();
        adding = new LinkedList<>();
    }

    public Room(World world, int x, int y){
        this(world);
        roomX = x;
        roomY = y;
        createLayout();
        deactivateLayout();
    }

    public Room(World world, int x, int y, int difficulty){
        this(world,x ,y);
        this.difficulty = difficulty;
    }

    //Draw functions
    public void drawBackground(){ //Draws the room's background
        //Set background color of room to a watered-down version of its map color
        batch.setColor(roomColor.r/2f+0.34f, roomColor.g/2f+0.34f, roomColor.b/2f+0.34f, 1f);
        batch.draw(atlas.findRegion("room/room_background"), roomX*roomWidth, roomY*roomHeight);
        batch.setColor(Color.WHITE);
        for(Projectile p : projectiles){
            p.display();
        }
        //Left
        batch.draw(atlas.findRegion("room/door"), roomX*250, roomY*250 + 150, 0, 0, 50, 20, 1, 1, 270);
        //Top
        batch.draw(atlas.findRegion("room/door"), roomX*250 + 150, roomY*250 + 250, 0, 0, 50, 20, 1, 1, 180);
        //Right
        batch.draw(atlas.findRegion("room/door"), roomX*250 + 250, roomY*250 + 100, 0, 0, 50, 20, 1, 1, 90);
        //Bottom
        batch.draw(atlas.findRegion("room/door"), roomX*250 + 100, roomY*250, 50, 20);

    }
    public void drawBorder(){ //Draws the room's border
        //batch.draw(atlas.findRegion("room/room_border"), roomX*roomWidth, roomY*roomHeight);
        batch.draw(atlas.findRegion("room/room_border"), roomX*roomWidth, roomY*roomHeight);
    }

    //Drawing on map
    public void displayOnMap(int numRooms){
        if(canEnter) {
            if (timesEntered > 0) { //If room has been entered
                roomMapSprite.setColor(roomColor.r, roomColor.g, roomColor.b, 0.45f); //Tint to room color
            } else { //Room has not been entered
                roomMapSprite.setColor(0.6f, 0.6f, 0.6f, 0.3f); //Tint to gray
            }

            if (currentRoom) { //If current room is this room
                //Tint to a lighter color than normal room color
                Color c = new Color(roomColor.r + 0.2f, roomColor.g + 0.2f, roomColor.b + 0.2f, 0.6f);
                roomMapSprite.setColor(c);
                //roomDoorSprite.setColor(c);
                roomMapSprite.draw(batch); //Draw map sprite
                //roomDoorSprite.draw(batch); //Draw door sprite
                batch.draw(atlas.findRegions("player/player_standing").get(0), roomMapSprite.getX() + 33.5f, roomMapSprite.getY() + 33.5f, 8, 8);
            } else {
                roomMapSprite.draw(batch); //Draw map sprite
                //roomDoorSprite.draw(batch); //Draw door sprite
            }
        }
    } //Displays the room and door on the map at the integer (x,y) coordinates

    public void primeMap(float x, float y, int numRooms){
        TextureAtlas.AtlasRegion doorType;
        if(entrance%2 == 0) {
            roomMapSprite = new Sprite(atlas.findRegion("room/room_map_background"));
            if(entrance == 2){
                roomMapSprite.flip(false, true);
            }
        }else{
            roomMapSprite = new Sprite(atlas.findRegion("room/room_map_background"));
            if(entrance == 1){
                roomMapSprite.rotate(90);
            }else{
                roomMapSprite.rotate(-90);
            }
        }
        //Better to set sprites location prior to display, than to set it every frame of display
        roomMapSprite.setPosition((roomX*250) + x - 37.5f, (roomY*250) + y - 37.5f);
        //roomDoorSprite.setPosition(mDoorXLoc() + (roomX*250) + x - 46.5f, mDoorYLoc() + (roomY*250) + y - 37.5f);
    } //Sets the sprites location to prep it to appear on map

    protected int mDoorXLoc(){
        if(entrance == 0)
            return 28;
        if(entrance == 1)
            return -7;
        if(entrance == 2)
            return 28;
        if(entrance == 3)
            return 62;
        return 0;
    } //Function that gets the space between the left edge and the door on a map display
    protected int mDoorYLoc(){
        if(entrance == 0)
            return 69;
        if(entrance == 1)
            return 34;
        if(entrance == 2)
            return 0;
        if(entrance == 3)
            return 34;
        return 0;
    } //Function that returns the space between the bottom edge and the door on a map display

    //Update function
    public void update(float deltaTime){ //Update function for room
        List<Projectile> flagged = new LinkedList<>();
        if(!adding.isEmpty()){
            for(Projectile p : adding){
                p.activate();
                projectiles.add(p);
            }
            adding.clear();
        }
        if(!projectiles.isEmpty()){
            for(Projectile p : projectiles) {
                p.update(deltaTime);
                if (p.isDeathFlagged()) {
                    flagged.add(p);
                }
            }
            if(!flagged.isEmpty()){
                for(Projectile p : flagged){
                    projectiles.remove(p);
                }
            }
        }
    }

    //Handles room passage
    public void enter(Player p){ //Runs upon entering the room
        activateLayout();
        timesEntered ++;
        currentRoom = true;
        for(Projectile proj : projectiles){
            proj.activate();
        }
    }
    public void exit(){ //Runs upon exiting the room
        deactivateLayout();
        for(Projectile p : projectiles){
            p.deactivate();
        }
        if(cleared){
            clearProjectiles();
        }
        currentRoom = false;
    }

    public void checkClear(){
        clearRoom();
    }

    public void setCanEnter(boolean set){
        canEnter = set;
    }

    public boolean canEnter(){
        return canEnter;
    }

    //Handles room activity
    protected void activateLayout(){
        for(Body b : layout){
            b.setActive(true);
        }
    } //Sets physics bodies of room to active
    protected void deactivateLayout(){
        for(Body b : layout){
            b.setActive(false);
        }
    } //Sets physics bodies of room to inactive
    protected void createLayout(){ //Creates the physical layout within the Box2D world
        layout.addAll(BodyFactory.createDoorsArrayList(roomX, roomY));
        layout.addAll(BodyFactory.createRoomMapSensors(roomX, roomY));
    } //Creates the layout of this room

    public void destroyRoom(){
        clearProjectiles();
        for(Body b : layout){
            world.destroyBody(b);
        }
    }

    public void addProjectile(Projectile p){
        adding.add(p);
    }
    public void removeProjectile(Projectile p){
        projectiles.remove(p);
    }
    public void clearProjectiles(){
        for(Projectile p : projectiles){
            p.destroy();
        }
        projectiles.clear();
    }

    public boolean checkEnter(int dir){
        if(canEnter) {
            if ((dir + 2) % 4 == entrance) {
                return true;
            }
        }
            return false;
    } //Check if the room can be entered, given the direction of exit

    //Clears the room
    public void clearRoom() {
        if (!cleared) {
            cleared = true;
            clearProjectiles();
            roomColor = new Color(roomColor.r / 2f + 0.33f, roomColor.g / 2f + 0.33f, roomColor.b / 2f + 0.33f, 1f);
            timesEntered = 1;
        }
    }

    public void shakeUp(){
        this.entrance = MathUtils.random(3);
    }


    //Getters
    public int getX(){
        return roomX;
    } //Get RoomX
    public int getY(){
        return roomY;
    } //Get RoomY
    public int getTimesEntered(){
        return timesEntered;
    } //Get times entered
    public boolean isCleared(){
        return cleared;
    } //Boolean to check if the room is cleared
    public int getQuadrant(){return roomX + 2*roomY;}
}

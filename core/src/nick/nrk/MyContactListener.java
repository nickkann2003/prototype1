package nick.nrk;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import nick.nrk.Entities.Entity;
import nick.nrk.Entities.Player;
import nick.nrk.Entities.Spawner;
import nick.nrk.Items.Item;

public class MyContactListener implements ContactListener {

    FinalPrototype1 game;

    public MyContactListener(FinalPrototype1 game){
        this.game = game;
    }

    @Override
    public void beginContact(Contact contact) {
        //System.out.println("Contact"); //debug
        Fixture fa = contact.getFixtureA();
        Fixture fb = contact.getFixtureB();
        Object datA = fa.getBody().getUserData();
        Object datB = fb.getBody().getUserData();

        if(null != datA && null != datB) {
            if(datA instanceof Entity){ //New and improved contact handling
                if(datB instanceof Entity)
                    game.contactStartEntityEntity((Entity) datA, (Entity) datB);
                if(datB instanceof String || datB instanceof Vector2)
                    game.contactStartEntityObject((Entity) datA, datB);
                if(datB instanceof Spawner)
                    game.contactStartEntitySpawner((Entity) datA, (Spawner) datB);
            }

            if(datB instanceof Entity){ //New and improved contact handling
                if(datA instanceof Entity)
                    game.contactStartEntityEntity((Entity) datB, (Entity) datA);
                if(datA instanceof String || datA instanceof Vector2)
                    game.contactStartEntityObject((Entity) datB, datA);
                if(datA instanceof Spawner)
                    game.contactStartEntitySpawner((Entity) datB, (Spawner) datA);
            }
        }

        if (datA instanceof Player) {
            if(datB instanceof Item){
                game.contactStartPlayerItem((Player) datA, (Item) datB);
            }
        }

        if(datA instanceof Item){
            if(datB instanceof Player){
                game.contactStartPlayerItem((Player) datB, (Item) datA);
            }
        }
    }

    @Override
    public void endContact(Contact contact) {
        Fixture fa = contact.getFixtureA();
        Fixture fb = contact.getFixtureB();
        Object datA = fa.getBody().getUserData();
        Object datB = fb.getBody().getUserData();

        if(null != datA && null != datB) {
            if(datA instanceof Entity){ //New and improved contact handling
                if(datB instanceof Entity)
                    game.contactEndEntityEntity((Entity) datA, (Entity) datB);
                if(datB instanceof Vector2 || datB instanceof String)
                    game.contactEndEntityObject((Entity) datA, datB);
            }

            if(datB instanceof Entity){ //New and improved contact handling
                if(datA instanceof Entity)
                    game.contactEndEntityEntity((Entity) datB, (Entity) datA);
                if(datA instanceof Vector2 || datA instanceof String)
                    game.contactEndEntityObject((Entity) datB, datA);
            }
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    /*
    * Figure out what this does
    * Does it activate AFTER step?
     */
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}

package nick.nrk.Items;

import nick.nrk.Entities.Player;
import nick.nrk.FinalPrototype1;

public class ProjectileSpeedBuff extends Item {

    public ProjectileSpeedBuff(){
        super();
        image = atlas.findRegion("items/projectileSpeedBuff");
    }

    public ProjectileSpeedBuff(float roomX, float roomY){
        super(roomX, roomY);
        //System.out.println(roomX);
        image = atlas.findRegion("items/projectileSpeedBuff");
    }

    public void pickUp(Player p, int loc){
        this.invLoc = loc;
        p.increaseProjectileSpeed(20f);
        p.increaseDamage(0.1f);
        p.giveItem(this);
        onGround = false;
    }
}

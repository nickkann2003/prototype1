package nick.nrk.Items;

import nick.nrk.Entities.Player;
import nick.nrk.FinalPrototype1;

public class ShotRateBuff extends Item {

    public ShotRateBuff(){
        super();
        image = atlas.findRegion("items/shotRateBuff");
    }

    public ShotRateBuff(float roomX, float roomY){
        super(roomX, roomY);
        //System.out.println(roomX);
        image = atlas.findRegion("items/shotRateBuff");
    }

    public void pickUp(Player p, int loc){
        this.invLoc = loc;
        p.increaseShotRate(0.45f);
        p.increaseProjectileSpeed(p.getProjectileSpeed()*0.05f);
        p.increaseDamage(-(p.getDamage()*0.1f));
        p.giveItem(this);
        onGround = false;
    }
}

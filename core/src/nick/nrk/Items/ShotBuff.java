package nick.nrk.Items;

import nick.nrk.Entities.Player;
import nick.nrk.FinalPrototype1;

public class ShotBuff extends Item {

    public ShotBuff(){
        super();
        image = atlas.findRegion("items/shotBuff");
    }

    public ShotBuff(float roomX, float roomY){
        super(roomX, roomY);
        //System.out.println(roomX);
        image = atlas.findRegion("items/shotBuff");
    }

    public void pickUp(Player p, int loc){
        this.invLoc = loc;
        p.increaseShotNum(1);
        p.increaseShotRate(-p.getShotRate()*0.25f);
        p.giveItem(this);
        onGround = false;
    }
}

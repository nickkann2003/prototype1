package nick.nrk.Items;

import nick.nrk.Entities.Player;
import nick.nrk.FinalPrototype1;

public class DamageBuff extends Item {

    public DamageBuff(){
        super();
        image = atlas.findRegion("items/damageBuff");
    }

    public DamageBuff(float roomX, float roomY){
        super(roomX, roomY);
        //System.out.println(roomX);
        image = atlas.findRegion("items/damageBuff");
    }

    public void pickUp(Player p, int loc){
        this.invLoc = loc;
        p.increaseDamage(0.33f);
        p.giveItem(this);
        onGround = false;
    }
}

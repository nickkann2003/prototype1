package nick.nrk.Items;

import nick.nrk.Entities.Player;
import nick.nrk.FinalPrototype1;

public class ProjectileBounceBuff extends Item {

    public ProjectileBounceBuff(){
        super();
        image = atlas.findRegion("items/projectileBounceBuff");
    }

    public ProjectileBounceBuff(float roomX, float roomY){
        super(roomX, roomY);
        //System.out.println(roomX);
        image = atlas.findRegion("items/projectileBounceBuff");
    }

    public void pickUp(Player p, int loc){
        this.invLoc = loc;
        p.increaseBounceNum(1);
        p.increaseProjectileSpeed(-8f);
        p.increaseProjectileHealth(0.2f);
        p.giveItem(this);
        onGround = false;
    }
}

package nick.nrk.Items;

import nick.nrk.Entities.Player;
import nick.nrk.FinalPrototype1;

public class HealthBuff
        extends Item {

    public HealthBuff
            (){
        super();
        image = atlas.findRegion("items/healthBuff");
    }

    public HealthBuff
            (float roomX, float roomY){
        super(roomX, roomY);
        //System.out.println(roomX);
        image = atlas.findRegion("items/healthBuff");
    }

    public void pickUp(Player p, int loc){
        this.invLoc = loc;
        p.increaseMaxHealth(7.5f);
        p.increaseSize(4f);
        p.increaseDamage(0.05f);
        p.giveItem(this);
        onGround = false;
    }
}

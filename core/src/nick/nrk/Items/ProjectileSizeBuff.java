package nick.nrk.Items;

import nick.nrk.Entities.Player;
import nick.nrk.FinalPrototype1;

public class ProjectileSizeBuff extends Item {

    public ProjectileSizeBuff(){
        super();
        image = atlas.findRegion("items/projectileSizeBuff");
    }

    public ProjectileSizeBuff(float roomX, float roomY){
        super(roomX, roomY);
        //System.out.println(roomX);
        image = atlas.findRegion("items/projectileSizeBuff");
    }

    public void pickUp(Player p, int loc){
        this.invLoc = loc;
        p.increaseProjectileSize(1.8f);
        p.increaseShotRate(-p.getShotRate()*0.15f);
        p.increaseProjectileHealth(1f);
        p.increaseProjectileSpeed(-p.getProjectileSpeed()*0.115f);
        p.giveItem(this);
        onGround = false;
    }
}

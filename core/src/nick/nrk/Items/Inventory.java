package nick.nrk.Items;

import java.util.*;

/**
 * Keeps track of all items either owned or not owned by player
 * Handles generation of random items for all item rooms
 * Can pickUp items
 * Can generate new random unowned items
 */
public class Inventory {
    private List<Item> inventory; //Inventory of all owned items
    private Set<Item> unowned; //Set of unowned items

    public Inventory(){
        inventory = new ArrayList<>();
    }

    public void display(){
        for(int i = 0; i < inventory.size(); i ++){
            inventory.get(i).display();
        }
    }

    public void giveItem(Item i){
        inventory.add(i);
    }

    public int size(){
        return inventory.size();
    }
}

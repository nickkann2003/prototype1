package nick.nrk.Items;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import nick.nrk.BodyFactory;
import nick.nrk.Entities.Player;
import nick.nrk.FinalPrototype1;
import nick.nrk.utils.Constants;

import static nick.nrk.utils.Constants.PPM;

/**
 * Item class
 * All Items extend the main Item class
 */
public abstract class Item {
    protected SpriteBatch batch;
    protected TextureAtlas atlas;
    protected World world;

    protected Body body;

    protected int invLoc;

    protected boolean onGround; //Is item on the ground or in player inventory
    protected Vector2 pos; //Position of item, whether in inventory or on ground
    protected TextureRegion image;

    public Item(){
        this.batch = FinalPrototype1.getBatch();
        this.atlas = FinalPrototype1.getAtlas();
        this.world = FinalPrototype1.getWorld();
        image = atlas.findRegion("player/player_sitting_right");
        pos = new Vector2(0,0);
        onGround = true;
        invLoc = 0;
    }

    public Item(float roomX, float roomY){
        this();
        pos = new Vector2( roomX*250 + 125 , roomY*250 + 125 );
        body = BodyFactory.createItemBody(pos.x, pos.y, this);
    }

    //Displays the item, either on the ground or in inventory
    public void display(){
        if(onGround) {
            batch.draw(image, body.getPosition().x*PPM-16, body.getPosition().y*PPM-16, 32, 32);
        }else{
            batch.draw(image, Constants.INVENTORY_X + 36 * (invLoc%6), Constants.INVENTORY_Y - 36 * (invLoc/6), 32, 32);
        }
    }

    //Update function, available for if item has an animation
    //Also handles item destruction
    public void update(float deltaTime){
        if(!onGround && body != null){
            world.destroyBody(body);
            body = null;
        }
    }

    public void activate(){
        if(null != body)
            body.setActive(true);
    }
    public void deactivate(){
        if(null != body)
            body.setActive(false);
    }

    //Picks up the item and applies the stats to the given player
    //ABSTRACT
    public abstract void pickUp(Player p, int loc);

    //Returns a new random subclass of this super class
    //Takes room X and Y to properly initialize the item
    public static Item randomize(float roomX, float roomY){
        int itemLoc = 0;
        int numItems = 9;
        itemLoc = MathUtils.random(numItems-1);
        return getAtLoc(itemLoc, roomX, roomY);
    }

    //Get Item corresponding to specific index
    //Takes roomX and roomY to properly initialize item
    protected static Item getAtLoc(int loc, float roomX, float roomY){
        if(loc == 0) {
            return new DamageBuff(roomX, roomY);
        }
        if(loc == 1){
            return new SpeedBuff(roomX, roomY);
        }
        if(loc == 2){
            return new HealthBuff(roomX, roomY);
        }
        if(loc == 3){
            return new BandageBuff(roomX, roomY);
        }
        if(loc == 4){
            return new ShotBuff(roomX, roomY);
        }
        if(loc == 5){
            return new ProjectileSpeedBuff(roomX, roomY);
        }
        if(loc == 6){
            return new ProjectileSizeBuff(roomX, roomY);
        }
        if(loc == 7){
            return new ProjectileBounceBuff(roomX, roomY);
        }
        if(loc == 8){
            return new ShotRateBuff(roomX, roomY);
        }
        return new DamageBuff();
    }

    //Set X position
    public void setRoomX(int x){
        pos.x = x*250 + 125;
    }

    //Set Y position
    public void setRoomY(int y){
        pos.y = y*250 + 125;
    }

    //Set the items position
    public void setPos(int roomX, int roomY){
        pos.x = roomX*250 + 125;
        pos.y = roomY*250 + 125;

    }
}

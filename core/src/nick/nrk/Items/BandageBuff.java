package nick.nrk.Items;

import nick.nrk.Entities.Player;
import nick.nrk.FinalPrototype1;

public class BandageBuff extends Item {

    public BandageBuff(){
        super();
        image = atlas.findRegion("items/bandageBuff");
    }

    public BandageBuff(float roomX, float roomY){
        super(roomX, roomY);
        //System.out.println(roomX);
        image = atlas.findRegion("items/bandageBuff");
    }

    public void pickUp(Player p, int loc){
        this.invLoc = loc;
        p.increaseHealth(9);
        p.increaseMaxHealth(3.5f);
        p.giveItem(this);
        onGround = false;
    }
}

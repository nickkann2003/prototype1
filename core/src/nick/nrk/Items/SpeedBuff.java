package nick.nrk.Items;

import nick.nrk.Entities.Player;
import nick.nrk.FinalPrototype1;

public class SpeedBuff extends Item {

    public SpeedBuff(){
        super();
        image = atlas.findRegion("items/speedBuff");
    }

    public SpeedBuff(float roomX, float roomY){
        super(roomX, roomY);
        //System.out.println(roomX);
        image = atlas.findRegion("items/speedBuff");
    }

    public void pickUp(Player p, int loc){
        this.invLoc = loc;
        p.increaseSpeed(15f);
        p.increaseProjectileSpeed(5f);
        p.giveItem(this);
        onGround = false;
    }
}

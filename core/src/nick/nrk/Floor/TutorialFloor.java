package nick.nrk.Floor;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.World;
import nick.nrk.Border;
import nick.nrk.Entities.Enemy;
import nick.nrk.Entities.Player;
import nick.nrk.Entities.Projectile;
import nick.nrk.FinalPrototype1;
import nick.nrk.GSM.TutorialState;
import nick.nrk.Rooms.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class TutorialFloor extends Floor {

    protected TutorialState myState;

    public TutorialFloor() {
        super();
        rooms[0] = new Room[0];
        rooms[1] = new Room[1];
        rooms[2] = new Room[1];
        rooms[3] = new Room[0];
        rooms[2][0] = new ItemRoom(world, 0, 1);
    }

    public TutorialFloor(int numRooms) {
        this();
    }

    public TutorialFloor(int numRooms, int difficulty) {
        this();
    }

    public TutorialFloor(int numRooms, int difficulty, Border b) {
        this();
        bossRoom = new BossRoom(world, b);
        for(int i = 0; i < 4; i ++){
            for(int j = 0; j < rooms[i].length; j ++){
                rooms[i][j].setCanEnter(false);
            }
        }
    }

    public TutorialFloor(int numRooms, int difficulty, Border b, TutorialState state){
        super();
        rooms[0] = new Room[0];
        rooms[1] = new Room[1];
        rooms[2] = new Room[1];
        rooms[3] = new Room[0];
        rooms[2][0] = new ItemRoom(world, 0, 1);

        myState = state;
        rooms[1][0] = new TutorialEnemyRoom(world, 1, 0, myState.getPlayer(), myState);

        bossRoom = new BossRoom(world, b);


        for(int i = 0; i < 4; i ++){
            for(int j = 0; j < rooms[i].length; j ++){
                rooms[i][j].setCanEnter(false);
            }
        }
    }

    public void revealItemRoom(){
        rooms[2][0].setCanEnter(true);
    }

    public void revealEnemyRoom(){
        rooms[1][0].setCanEnter(true);
    }

    @Override
    public Room getBaseRoom(int x, int y) {
        myState.incrementEntered();
        return super.getBaseRoom(x, y);
    }

    @Override
    public Room getNextRoom(int dir, int roomX, int roomY) {
        myState.incrementEntered();
        Room r = super.getNextRoom(dir, roomX, roomY);
        if(r instanceof ItemRoom){
            myState.enterItemRoom();
        }
        if(r instanceof EnemyRoom){
            myState.enterEnemyRoom();
        }
        if(r instanceof BossRoom){
            myState.enterBossRoom();
        }
        return r;
    }
}

package nick.nrk.Floor;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.World;
import nick.nrk.Border;
import nick.nrk.Entities.Player;
import nick.nrk.Entities.Projectile;
import nick.nrk.FinalPrototype1;
import nick.nrk.Rooms.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class Floor {
    /*
    * Floor classes
    * Container for all rooms
    * Handles entrance and exit of rooms
    * Handles creation of rooms
     */

    protected int numRooms; //Number of rooms on the floor

    protected int difficulty; //Integer variable stores the difficulty of rooms in the floor

    protected Room[][] rooms = new Room[4][];
    protected BossRoom bossRoom;
    protected BaseRoom[] baseRooms = new BaseRoom[4];

    protected World world; //World
    protected SpriteBatch batch; //Batch
    protected TextureAtlas atlas; //Atlas

    protected boolean cleared; //Boolean keeps track of if all rooms are cleared or not

    protected List<Projectile> cleanup;

    //Base constructor
    public Floor(){ //Most basic constructor, initializes base variables
        world = FinalPrototype1.getWorld();
        batch = FinalPrototype1.getBatch();
        atlas = FinalPrototype1.getAtlas();
        for(int i = 0; i < baseRooms.length; i ++){
            baseRooms[i] = new BaseRoom(world, i%2, i/2);
        }
        cleanup = new LinkedList<Projectile>();
    }

    //Constructor with num rooms
    public Floor(int numRooms){ //Constructor, takes number of rooms
        this();
        this.numRooms = numRooms; //Set num rooms
    }

    //Constructor with roms + difficulty
    public Floor(int numRooms, int difficulty){ //Constructor, takes number of rooms and difficulty
        this(numRooms);
        this.difficulty = difficulty;
    }

    //Constructor with rooms + difficulty
    public Floor(int numRooms, int difficulty, Border b){ //Constructor, takes number of rooms and difficulty
        this(numRooms);
        this.difficulty = difficulty;
        bossRoom = new BossRoom(world, b);
        for(int i = 0; i < numRooms%4; i ++){ //All arrays below the % are +1
            rooms[i] = new Room[numRooms/4+1];
        }
        for(int i = 3; i >= numRooms%4; i --){ //All arrays after % are normal
            rooms[i] = new Room[numRooms/4];
        }
        createFloor();
    }

    //CREATE FLOOR FUNCTION
    //Handles the creation of the floor based on the parameters received in the constructor
    private void createFloor(){
        //THIS CODE DETERMINES THE FREQUENCY OF ROOMS
        int numItemRooms = (numRooms/8) + 1; //1 + 1 per 8
        int numEnemyRooms = (int) (9*numRooms/10f); //9 per 10

        //Fixes rounding errors
        numEnemyRooms += numRooms - (numItemRooms); //Fixes rounding errors

        //Variables used for room creation
        int remainingRooms = numRooms; //Rooms remaining to be made
        float iChance = (1f*numItemRooms)/(1f*remainingRooms); //Chance of item room
        float eChance = (1f*numEnemyRooms)/(1f*remainingRooms); //Chance of enemy room
        float r = MathUtils.random(1f); //Random number

        //Floor creation loop
        for(int i = 0; i < 4; i ++){ //Loop over first array
            for(int j = 0; j < rooms[i].length; j ++) { //Loop over rooms in selected array
                if (r < iChance) { //If RNG decides item room
                    rooms[i][j] = new ItemRoom(world, i%2, i/2, difficulty/(numRooms*6/10));
                    numItemRooms -= 1;
                    remainingRooms -= 1;
                }else { //If RNG decides enemy room
                    rooms[i][j] = new EnemyRoom(world, i%2, i/2, difficulty/(numRooms*6/10));
                    numEnemyRooms -= 1;
                    remainingRooms -= 1;
                }
                //Reset chances
                iChance = 1f*numItemRooms/remainingRooms;
                eChance = 1f*numEnemyRooms/remainingRooms;
                r = MathUtils.random(1f);
            }
        }
    }

    //Getters
    public boolean isCleared(){return cleared;}
    public BossRoom getBossRoom(){return bossRoom;}
    public Room[][] getRooms(){
        return rooms;
    }
    public Room getBaseRoom(int x, int y){
        return baseRooms[x+(2*y)];
    }


    public Room getNextRoom(int dir, int roomX, int roomY){
        checkCleared();
        if(cleared){
            if(bossRoom.checkEnter(dir, roomX, roomY)){
                return bossRoom;
            }
        }
        int arrayLoc = 0; //Gets location of room in array
        if(dir == 0 || dir == 2)
            arrayLoc = roomX + 2*((roomY+1)%2); //If its up or down, get room in dir
        if(dir == 1 || dir == 3)
            arrayLoc = ((roomX+1)%2 + roomY*2); //If its left or right, get room in dir
        ArrayList<Room> found = new ArrayList<>(); //Arraylist of found rooms
        for(Room r : rooms[arrayLoc]) { //Loop over all rooms in array at loc
            if (r.canEnter()) { //If the room can be entered
                if (r.checkEnter(dir)) { //Check if room can be entered
                    if (found.isEmpty()) { //If array is empty
                        found.add(r); //Add first found room
                    } else { //Array isnt empty
                        if (!found.get(0).isCleared()) {
                            if (!r.isCleared()) {
                                if (r.getTimesEntered() == found.get(0).getTimesEntered())
                                    found.add(r);
                                if (r.getTimesEntered() < found.get(0).getTimesEntered()) {
                                    found.clear();
                                    found.add(r);
                                }
                            }
                        } else {
                            if (r.isCleared()) {
                                if (r.getTimesEntered() == found.get(0).getTimesEntered())
                                    found.add(r);
                                if (r.getTimesEntered() < found.get(0).getTimesEntered()) {
                                    found.clear();
                                    found.add(r);
                                }
                            } else {
                                found.clear();
                                found.add(r);
                            }
                        }
                    }
                }
            }
        }
        if (!found.isEmpty()) { //If rooms were found
            return found.get(MathUtils.random(found.size() - 1)); //return random room from array
        }
        return findBaseRoom(dir, roomX, roomY); //Return baseroom
    } //Returns a random room in the given direction of the given X/Y coords,
    // returns a base room if nothing is found

    //Private assist functions
    private Room findBaseRoom(int dir, int roomX, int roomY){
        if(dir == 0 || dir == 2)
            return baseRooms[roomX+2*((roomY+1)%2)];
        if(dir == 1 || dir == 3)
            return baseRooms[(roomX+1)%2+roomY*2];
        return baseRooms[0];
    } //Finds a baseRoom for the given direction of the give X/Y coords

    //Checks if all rooms have been cleared
    public void checkCleared(){
        if(!cleanup.isEmpty()){
            for(Projectile p : cleanup){
                p.update(1);
            }
        }
        for(int i = 0; i < 4; i ++){
            for(Room r : rooms[i]){
                if(!r.isCleared())
                    return;
            }
        }
        cleared = true;
    }

    public void destroyFloor(){
        for(int i = 0; i < 4; i ++){
            for(int j = 0; j < rooms[i].length; j ++){
                rooms[i][j].destroyRoom();
            }
        }
        bossRoom.destroyRoom();
    }

    public void addCleanup(Collection<Projectile> E){
        cleanup.addAll(E);
    }

    public void shakeUp(){
        for(int i = 0; i < 4; i ++){
            for(Room r : rooms[i]){
                r.shakeUp();
            }
        }
    }
}

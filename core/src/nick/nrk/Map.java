package nick.nrk;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import jdk.nashorn.internal.ir.annotations.Ignore;
import nick.nrk.Entities.Enemy;
import nick.nrk.Floor.Floor;
import nick.nrk.Rooms.*;

public class Map {
    /*
    * Map class
    * Handles the displaying of the rooms of the floor
    * Displays in the right panel
     */

    protected Floor floor; //Current floor
    protected Room[][] rooms; //Array of all rooms
    protected int currentQuadrant; //Int holding current displayed quadrant
    protected boolean selectedQuadrant; //Boolean telling if the player manually selected a quadrant or not
    protected boolean ignoreVision; //Boolean telling if vision of next room should be ignored or not
    protected BossRoom bRoom; //Boss room
    protected boolean floorCleared = false; //Is the floor cleared
    protected boolean showAll;
    protected int previousQuadrant;
    protected int hiddenQuadrant;
    protected boolean disabled; //Whether player can use map or not

    protected SpriteBatch batch;
    protected TextureAtlas atlas;

    protected Vector2[] mapButtons;

    public Map(Floor floor){
        this.floor = floor;
        this.rooms = floor.getRooms();
        this.bRoom = floor.getBossRoom();
        this.batch = FinalPrototype1.getBatch();
        this.atlas = FinalPrototype1.getAtlas();
        showAll = false;
        currentQuadrant = 0; //Quadrant to display map pieces
        //Array representing X,Y locations of each quadrant button
        mapButtons = new Vector2[] {new Vector2(507, 480), new Vector2(528, 480),
                                    new Vector2(549, 480), new Vector2(570, 480),
                                    new Vector2(591, 480)};
        for(int i = 0; i < rooms[currentQuadrant].length; i ++){ //Prime all maps to start
            int numRooms = rooms[currentQuadrant].length;
            rooms[currentQuadrant][i].primeMap(getRoomMapX(i, numRooms), getRoomMapY(i, numRooms), numRooms);
        }
        previousQuadrant = 0;
    }

    //Main display function of the map
    public void display(){ //Display function of the map
        currentQuadrant = 4;
        if(!disabled) {
            if (showAll) {
                if (hiddenQuadrant != 4) {
                    for (int i = 0; i < 4; i++) {
                        prepQuadrant(i);
                        for (int j = 0; j < rooms[i].length; j++) {
                                rooms[i][j].displayOnMap(rooms[i].length);
                        }
                    }
                    batch.setColor(1f, 1f, 1f, 0.5f);
                    if (floor.isCleared()) //Draws door to the boss room if the floor is cleared
                        batch.draw(atlas.findRegion("map/map_boss_icon"), bossDoorXLoc(bRoom.getEntrance()), bossDoorYLoc(bRoom.getEntrance()), 64, 64);
                    batch.setColor(Color.WHITE);
                }
            } else if (currentQuadrant != 4) {
                for (int i = 0; i < rooms[currentQuadrant].length; i++) {
                    Room r = rooms[currentQuadrant][i];
                    r.displayOnMap(1);
                }
            } else {

            }
            for (int i = 0; i < 5; i++) { //Loop over all icons
                //Display inactive icons
                //batch.draw(atlas.findRegions("map/map_icon_inactive").get(i), 507 + 21 * i, 480);
            }
            //Display active for selected icon
            if (selectedQuadrant) {
                batch.setColor(0f, 0.7f, 0.7f, 0.6f); //Set color to yellow
                ignoreVision = true;
            } else {
                batch.setColor(0f, 0.7f, 0.7f, 0.2f);
            }
            //batch.draw(atlas.findRegions("map/map_icon_active").get(currentQuadrant), 507 + 21 * currentQuadrant, 480);
            batch.setColor(Color.WHITE); //Reset color
        }
    }

    //Public function used to switch map display as rooms are entered
    public void switchRoom(int room){
        if(!selectedQuadrant) {
            currentQuadrant = room;
            prepCurrentQuadrant();
        }
    }

    //Public function displays current room on the map
    public void showCurrentRoom(int room){
        /*
        if(!selectedQuadrant) {
            currentQuadrant = room;
            prepCurrentQuadrant();
            ignoreVision = false;
            AudioController.endBossDoorSee();
        }
         */
    }

    //Handles showing the rooms in the next quadrant given direction and location
    public void showRoomInDirection(int drc, int roomX, int roomY) {
        int rX = roomX;
        int rY = roomY;
        int availNum = -1;
            if (!ignoreVision) { //Only check rooms if ignore vision is not true
                if (drc == 1 || drc == 3)
                    rX = (rX + 1) % 2;
                if (drc == 0 || drc == 2)
                    rY = (rY + 1) % 2;
                currentQuadrant = rX + 2 * rY;
                prepCurrentQuadrant();

                if (floor.isCleared() && bRoom.checkEnter(drc, roomX, roomY)) { //Is boss room
                    currentQuadrant = 4;
                    prepCurrentQuadrant();
                    AudioController.playBossDoorSee();
                } else { //Is not boss room
                    for (int i = 0; i < rooms[currentQuadrant].length; i++) {
                        if (!rooms[currentQuadrant][i].isCleared()) {
                            if (rooms[currentQuadrant][i].checkEnter(drc)) {
                                availNum = i; //See if enterable found
                            }
                        }
                    }
                }
                if (availNum != -1) { //If enterable found
                    Room found = rooms[currentQuadrant][availNum]; //Play appropriate sound
                    if (found instanceof ItemRoom)
                        AudioController.playItemDoorSee();
                    if (found instanceof EnemyRoom)
                        AudioController.playEnemyDoorSee();
                    if (found instanceof PuzzleRoom)
                        AudioController.playPuzzleDoorSee();
                }
            }
    }

    //Private assist function that preps all rooms in the currentQuadrant
    private void prepCurrentQuadrant(){
        if(currentQuadrant != 4) {
            for (int i = 0; i < rooms[currentQuadrant].length; i++) {
                int numRooms = rooms[currentQuadrant].length;
                rooms[currentQuadrant][i].primeMap(getRoomMapX(i, numRooms), getRoomMapY(i, numRooms), numRooms);
            }
        }
    }

    private void prepQuadrant(int j){
        if(j != 4) {
            for (int i = 0; i < rooms[j].length; i++) {
                int numRooms = rooms[j].length;
                rooms[j][i].primeMap(getRoomMapX(i, numRooms), getRoomMapY(i, numRooms), numRooms);
            }
        }
    }

    //Handles the drawing of the Boss Room's door
    private void drawBossRoomDoor(){
        int ent = bRoom.getEntrance();
            if ((ent / 2) % 2 == 0)
                batch.draw(atlas.findRegion("room/map_door_hr"), bossDoorXLoc(ent), bossDoorYLoc(ent), 30, 5);
            if ((ent / 2) % 2 == 1)
                batch.draw(atlas.findRegion("room/map_door_vr"), bossDoorXLoc(ent), bossDoorYLoc(ent), 5, 30);
    }

    //Gets the X location for the boss room door on the map
    protected int bossDoorXLoc(int entrance) {
        int ent = entrance;
        //Left coord is 507, width is 236
        int lX = 0;
        int sz = 500;
        if (ent == 4)
            return lX + (sz/4)-32;
        if (ent == 5)
            return lX + (sz*3/4)-32;
        if (ent == 6)
            return lX+16;
        if (ent == 7)
            return lX+16;
        if (ent == 0)
            return lX + (sz/4)-32;
        if (ent == 1)
            return lX + (sz*3/4)-32;
        if (ent == 2)
            return lX + (sz)-80;
        if (ent == 3)
            return lX + (sz)-80;
        return 0;
        }
    //Gets the Y location for the boss room door on the map
    protected int bossDoorYLoc(int entrance){
        int ent = entrance;
        int bY = 0;
        int sz = 500;
        if (ent == 4)
            return bY + (sz)-80;
        if (ent == 5)
            return bY + (sz)-80;
        if (ent == 6)
            return bY + (sz*3/4)-32;
        if (ent == 7)
            return bY + (sz/4)-32;
        if (ent == 0)
            return bY + 16;
        if (ent == 1)
            return bY + 16;
        if (ent == 2)
            return bY + (sz*3/4)-32;
        if (ent == 3)
            return bY + (sz/4)-32;
        return 0;
    }

    public void handleDownInput(int input){
        if(input == Input.Keys.TAB){
            showAll = true;
            previousQuadrant = currentQuadrant;
        }
    }

    public void handleUpInput(int input){
        if(input == Input.Keys.TAB){
            showAll = false;
            if(previousQuadrant != hiddenQuadrant)
             currentQuadrant = previousQuadrant;
        }
    }

    public void handleMouseInput(int x, int y){
        floor.checkCleared();
        int nQ = checkButtonHovered(x,500-y); //Get quadrant based on mouse hover
        if(nQ != -1){ //If a quadrant is hovered
            if(selectedQuadrant && currentQuadrant == nQ){
                selectedQuadrant = false;
                ignoreVision = false;
            }else{
                selectedQuadrant = true;
            }
            currentQuadrant = nQ; //Store quadrant
            prepCurrentQuadrant();
        }
    } //Handles mouse input regarding map functionality
    protected int checkButtonHovered(int x, int y){
        for(int i = 0; i < mapButtons.length; i ++){ //Loop over array
            if(x >= mapButtons[i].x && x <= mapButtons[i].x+16) { //Check X Bounds
                if(y >= mapButtons[i].y && y <= mapButtons[i].y + 16){ //Check Y Bounds
                    return i; //Return loc
                }
            }
        }
        return -1;
    } //Returns an int representing a quadrant based on given x,y pos
    public void setCurrentHidden(int quadrant){
        hiddenQuadrant = quadrant;
    }

    //Disables player interaction with map
    public void disable(){
        disabled = true;
    }
    //Enables player interaction with map
    public void enable(){
        disabled = false;
    }

    public float getRoomMapX(int i, int numRooms){
        if(numRooms > 2){
            if(i >= 2) {
                return getRoomMapX(i % 2, numRooms - 2);
            }else{
                return getRoomMapX(i % 2, 2);
            }
        }if(numRooms == 2){
            return 90f + 80f*(float)i;
        }else{
            return 125f;
        }
    }

    public float getRoomMapY(int i, int numRooms){
        if(numRooms > 2 ){
            if(i >= 2){
                return getRoomMapY(i/2, numRooms - 2) + 38f;
            }else{
                return getRoomMapY(i, 2) - 38f;
            }
        }if(numRooms == 2){
            return 125f;
        }else{
            return 125f;
        }
    }
}

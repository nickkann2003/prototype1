package nick.nrk.utils;

import com.badlogic.gdx.Gdx;
import nick.nrk.Entities.Behaviors.Charger;
import nick.nrk.Entities.Behaviors.Melee;
import nick.nrk.Entities.Behaviors.Turret;
import nick.nrk.Entities.EArchtype;
import nick.nrk.Entities.Type;
import sun.tools.asm.CatchData;

import java.util.ArrayList;

public final class Constants {
    //Constant for Pixels per Meter, used for Box2D calculations
    public static final float PPM = 16;

    public static final float w = Gdx.graphics.getWidth(); //Width of the screen
    public static final float h = Gdx.graphics.getHeight(); //Height of the screen

    public static final float KBFriction = 3200/PPM; //Rate at which knockback decreases

    public static final float roomWidth = 250; //Width of a room
    public static final float roomHeight = 250; //Height of a room

    //Inventory Location
    public static final float INVENTORY_X = 510;
    public static final float INVENTORY_Y = 200;

    //Filter bits, used to determine filter of a fixture
    public static final short CATEGORY_PLAYER = 1;
    public static final short CATEGORY_ENEMY = 2;
    public static final short CATEGORY_WORLD = 4;
    public static final short CATEGORY_PLAYER_PROJECTILE = 8;
    public static final short CATEGORY_ENEMY_PROJECTILE = 16;
    public static final short CATEGORY_IMMUNE = 32;

    //Masks, applied to fixtures to determine what they can collide with
    public static final short MASK_PLAYER = ~CATEGORY_PLAYER_PROJECTILE;
    public static final short MASK_ENEMY = ~CATEGORY_ENEMY_PROJECTILE;
    public static final short MASK_WORLD = -1;
    public static final short MASK_PLAYER_PROJECTILE = ~CATEGORY_PLAYER & ~CATEGORY_PLAYER_PROJECTILE & ~CATEGORY_WORLD;
    public static final short MASK_ENEMY_PROJECTILE = ~CATEGORY_ENEMY_PROJECTILE & ~CATEGORY_ENEMY_PROJECTILE;
    public static final short MASK_IMMUNE = CATEGORY_WORLD;

    //Container for all enemy types
    public static final ArrayList<EArchtype> tier1EnemyTypes = new ArrayList<EArchtype>() {
        {
            add(EArchtype.BASIC);
            add(EArchtype.MELEE);
            add(EArchtype.RANGED2);
            add(EArchtype.TURRET1);
            add(EArchtype.SPRAYER);
            add(EArchtype.MELEE2);
            add(EArchtype.HIGHTIER);
            add(EArchtype.RANGED);
        }
    };

    public static final ArrayList<EArchtype> tier2EnemyTypes = new ArrayList<EArchtype>() {
        {
            add(EArchtype.SWARMER);
            add(EArchtype.BASIC2);
            add(EArchtype.TURRET2);
            add(EArchtype.TANK);
            add(EArchtype.SCROBLIN);
            add(EArchtype.FINAL);
        }
    };
}

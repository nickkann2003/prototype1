package nick.nrk;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import nick.nrk.utils.Constants;
import org.graalvm.compiler.virtual.phases.ea.PartialEscapeClosure;

import java.util.ArrayList;

import static nick.nrk.utils.Constants.PPM;

public final class BodyFactory {
    /*
    * Final class used for the creation of bodies within the world
     */

    private static World world;

    public BodyFactory(){}
    public static void giveWorld(World w){
        world = w;
    } //Gives this Final class the world to create things in


    public static Body createEdgeBody(float x, float y, BodyDef.BodyType bodyType, float width, float height){
        Body bod; //Holder body
        BodyDef bodyDef = new BodyDef(); //Body definition
        bodyDef.type = bodyType; //Given type
        FixtureDef fixtureDef = new FixtureDef(); //Fixture def
        EdgeShape edgeShape = new EdgeShape(); //Edge shape
        bodyDef.position.set(x/PPM, y/PPM); //Set position to middle of wall
        edgeShape.set(-width/2/PPM,-height/2/PPM,width/2/PPM,height/2/PPM); //Create shape
        fixtureDef.shape = edgeShape; //Set fixture shape to shape
        fixtureDef.filter.categoryBits = Constants.CATEGORY_WORLD;
        fixtureDef.filter.maskBits = Constants.MASK_WORLD;
        bod = world.createBody(bodyDef); //Create body in world
        bod.createFixture(fixtureDef); //Give body fixture
        edgeShape.dispose(); //Dispose of shape
        return bod; //return reference to body
    } //Creates an Edge body within the world

    //Secondary edge body function, used for walls to get position for projectile collison
    public static Body createEdgeBody(float x, float y, BodyDef.BodyType bodyType, float width, float height, Vector2 hitPosition){
        Body bod; //Holder body
        BodyDef bodyDef = new BodyDef(); //Body definition
        bodyDef.type = bodyType; //Given type
        FixtureDef fixtureDef = new FixtureDef(); //Fixture def
        EdgeShape edgeShape = new EdgeShape(); //Edge shape
        bodyDef.position.set(x/PPM, y/PPM); //Set position to middle of wall
        edgeShape.set(-width/2/PPM,-height/2/PPM,width/2/PPM,height/2/PPM); //Create shape
        fixtureDef.shape = edgeShape; //Set fixture shape to shape
        fixtureDef.filter.categoryBits = Constants.CATEGORY_WORLD;
        fixtureDef.filter.maskBits = Constants.MASK_WORLD;
        bod = world.createBody(bodyDef); //Create body in world
        bod.setUserData(hitPosition);
        bod.createFixture(fixtureDef); //Give body fixture
        edgeShape.dispose(); //Dispose of shape
        return bod; //return reference to body
    } //Creates an Edge body within the world

    public static Body createDoorBody(float x, float y, float width, float height, String userDat){ //Creates a door body within the world
        Body bod; //Holder body
        BodyDef bodyDef = new BodyDef(); //Definition
        bodyDef.type = BodyDef.BodyType.StaticBody; //Type static
        FixtureDef fixtureDef = new FixtureDef(); //FDef
        EdgeShape edgeShape = new EdgeShape(); //Edge
        bodyDef.position.set(x/PPM, y/PPM); //Set location
        edgeShape.set(-width/2/PPM, -height/2/PPM, width/2/PPM, height/2/PPM); //Set size
        fixtureDef.shape = edgeShape; //Set shape
        fixtureDef.isSensor = true; //SET SENSOR
        fixtureDef.filter.categoryBits = Constants.CATEGORY_WORLD;
        fixtureDef.filter.maskBits = Constants.MASK_WORLD;
        bod = world.createBody(bodyDef); //Create body in world with def
        bod.setUserData(userDat); //Set bodies user data
        bod.createFixture(fixtureDef); //Create fixture
        edgeShape.dispose(); //Dispose of shape
        return bod; //Return reference
    }

    public static Body[] createDoors(int x1, int y1){ //Creates all door bodies within the room
        int x = x1*250; //X/Y are passed as integer locations of the room
        int y = y1*250; //To get pixel location, multiply by size of the room
        Body[] doors = new Body[4];

        doors[0] = createDoorBody((x+125), (y+250), 20, 0, "UP");
        doors[1] = createDoorBody((x), (y+125), 0, 20, "LEFT");
        doors[2] = createDoorBody((x+125), (y), 20, 0, "DOWN");
        doors[3] = createDoorBody((x+250), (y+125), 0, 20, "RIGHT");

        return doors;
    }

    public static ArrayList<Body> createDoorsArrayList(int x1, int y1){ //Creates all door bodies within the room
        float x = x1*250; //X/Y are passed as integer locations of the room
        float y = y1*250; //To get pixel location, multiply by size of the room
        ArrayList<Body> doors = new ArrayList<Body>(4);

        doors.add(createDoorBody((x+125), (y+250), 50, 0, "UP"));
        doors.add(createDoorBody((x), (y+125), 0, 50, "LEFT"));
        doors.add(createDoorBody((x+125), (y), 50, 0, "DOWN"));
        doors.add(createDoorBody((x+250), (y+125), 0, 50, "RIGHT"));

        return doors;
    }

    public static ArrayList<Body> createBossExitDoor(){
        ArrayList<Body> door = new ArrayList(1);
        door.add(createDoorBody(250,499,20, 0, "NEXT"));
        return door;
    }

    //Creates map sensors for a room, which switch the map to the next room when triggered
    public static ArrayList<Body> createRoomMapSensors(int roomX, int roomY){
        ArrayList<Body> roomMapSensors = new ArrayList(4);
        int x = roomX*250;
        int y = roomY*250;
        roomMapSensors.add(createSensorBody((x+125), (y+245), 40, 20, "SEEUP"));
        roomMapSensors.add(createSensorBody((x+5), (y+125), 20, 40, "SEELEFT"));
        roomMapSensors.add(createSensorBody((x+125), (y+5), 40, 20, "SEEDOWN"));
        roomMapSensors.add(createSensorBody((x+245), (y+125), 20, 40, "SEERIGHT"));
        return roomMapSensors;
    }

    //Functions the same way as the createDoorBody function, but creates a rectangle instead of an edge
    public static Body createSensorBody(float x, float y, float width, float height, Object userDat){ //Creates a door body within the world
        Body bod; //Holder body
        BodyDef bodyDef = new BodyDef(); //Definition
        bodyDef.type = BodyDef.BodyType.StaticBody; //Type static
        FixtureDef fixtureDef = new FixtureDef(); //FDef
        PolygonShape box = new PolygonShape();
        bodyDef.position.set(x/PPM, y/PPM); //Set location
        box.set(new float[] {-width/2/PPM, -height/2/PPM, -width/2/PPM, height/2/PPM, width/2/PPM, height/2/PPM, width/2/PPM, -height/2/PPM}); //Set size
        fixtureDef.shape = box; //Set shape
        fixtureDef.isSensor = true; //SET SENSOR
        fixtureDef.filter.categoryBits = Constants.CATEGORY_WORLD;
        fixtureDef.filter.maskBits = Constants.MASK_WORLD;
        bod = world.createBody(bodyDef); //Create body in world with def
        bod.setUserData(userDat); //Set bodies user data
        bod.createFixture(fixtureDef); //Create fixture
        box.dispose(); //Dispose of shape
        return bod; //Return reference
    }

    //Creates enemy bodies
    public static Body createEnemyBody(float x, float y, float width, float height, Object enemy){
        Body bod; //Holder body
        BodyDef bodyDef = new BodyDef(); //Definition
        bodyDef.type = BodyDef.BodyType.DynamicBody; //Type
        FixtureDef fixtureDef = new FixtureDef(); //FDef
        FixtureDef enemyCollider = new FixtureDef();
        CircleShape box = new CircleShape(); //Shape
        bodyDef.position.set(x/PPM, y/PPM); //Set location
        box.setRadius(width/2/PPM);
        //box.set(new float[] {-width/2/PPM, -height/2/PPM, -width/2/PPM, height/2/PPM, width/2/PPM, height/2/PPM, width/2/PPM, -height/2/PPM}); //Set size
        fixtureDef.shape = box; //Set shape
        fixtureDef.isSensor = false; //Not Sensor
        fixtureDef.density = 1f;
        fixtureDef.filter.categoryBits = Constants.CATEGORY_ENEMY;
        fixtureDef.filter.maskBits = Constants.MASK_ENEMY;
        /*
        box.setRadius(box.getRadius()*7f/8f);
        enemyCollider.shape = box;
        enemyCollider.isSensor = false;
        enemyCollider.density = 1f;
        enemyCollider.filter.categoryBits = Constants.CATEGORY_ENEMY;
        enemyCollider.filter.maskBits = Constants.MASK_ENEMY;
        */
        bod = world.createBody(bodyDef); //Create body in world with def
        bod.setUserData(enemy); //Set bodies user data
        bod.createFixture(fixtureDef); //Create fixture
        //bod.createFixture(enemyCollider);
        box.dispose(); //Dispose of shape
        return bod; //Return reference
    }

    //Creates enemy projeciltes
    public static Body createEnemyProjectile(float x, float y, float radius, Object projectile){
        Body bod; //Holder body
        BodyDef bodyDef = new BodyDef(); //Definition
        bodyDef.type = BodyDef.BodyType.DynamicBody; //Type
        FixtureDef fixtureDef = new FixtureDef(); //FDef
        CircleShape circle = new CircleShape(); //Shape
        bodyDef.position.set(x/PPM, y/PPM); //Set location
        circle.setRadius(radius/PPM); //Set size
        fixtureDef.isSensor = true;
        fixtureDef.shape = circle; //Set shape
        fixtureDef.density = 1f;
        fixtureDef.filter.categoryBits = Constants.CATEGORY_ENEMY_PROJECTILE;
        fixtureDef.filter.maskBits = Constants.MASK_ENEMY_PROJECTILE;
        bod = world.createBody(bodyDef); //Create body in world with def
        bod.setUserData(projectile); //Set bodies user data
        bod.createFixture(fixtureDef); //Create fixture
        circle.dispose(); //Dispose of shape
        return bod; //Return reference
    }

    //Creates player projeciltes
    public static Body createPlayerProjectile(float x, float y, float radius, Object projectile){
        Body bod; //Holder body
        BodyDef bodyDef = new BodyDef(); //Definition
        bodyDef.type = BodyDef.BodyType.DynamicBody; //Type
        FixtureDef fixtureDef = new FixtureDef(); //FDef
        FixtureDef wallCollider = new FixtureDef();
        CircleShape circle = new CircleShape(); //Shape
        bodyDef.position.set(x/PPM, y/PPM); //Set location
        circle.setRadius(radius/PPM); //Set size

        fixtureDef.shape = circle; //Set shape
        fixtureDef.isSensor = true;
        fixtureDef.density = 1f;
        fixtureDef.filter.categoryBits = Constants.CATEGORY_PLAYER_PROJECTILE;
        fixtureDef.filter.maskBits = Constants.MASK_PLAYER_PROJECTILE;

        wallCollider.shape = circle;
        wallCollider.isSensor = false;
        wallCollider.density = 1f;
        wallCollider.filter.categoryBits = Constants.CATEGORY_IMMUNE;
        wallCollider.filter.maskBits = Constants.MASK_IMMUNE;

        bod = world.createBody(bodyDef); //Create body in world with def
        bod.setUserData(projectile); //Set bodies user data
        bod.createFixture(fixtureDef); //Create fixture
        bod.createFixture(wallCollider);
        circle.dispose(); //Dispose of shape
        return bod; //Return reference
    }

    //Creates item bodies to exist in the world
    public static Body createItemBody(float x, float y, Object item){
        Body bod;
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        FixtureDef fixtureDef = new FixtureDef(); //FDef
        CircleShape circle = new CircleShape(); //Shape
        bodyDef.position.set((x)/PPM, (y)/PPM); //Set location
        circle.setRadius(16f/PPM); //Set size
        fixtureDef.shape = circle; //Set shape
        fixtureDef.isSensor = false; //Not Sensor
        fixtureDef.density = 1f;
        fixtureDef.filter.categoryBits = Constants.CATEGORY_WORLD;
        fixtureDef.filter.maskBits = Constants.MASK_WORLD;
        bod = world.createBody(bodyDef); //Create body in world with def
        bod.setUserData(item); //Set bodies user data
        bod.createFixture(fixtureDef); //Create fixture
        circle.dispose(); //Dispose of shape
        return bod; //Return reference

    }
}

package nick.nrk;

/*
* Class to manage hitboxes of all walls
* Has 2 modes: Normal and Boss
* Normal mode is standard 2x2
* Boss mode is one large room
 */

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import nick.nrk.utils.Constants;

import static nick.nrk.utils.Constants.PPM;

public class Border {
    private Body[] walls = new Body[4]; //Array of walls
    private boolean walled = false; //Are walls active
    private Body[] cross = new Body[2]; //Array of cross
    private boolean crossed = false; //Is cross active
    private Body[] doors = new Body[16];
    private boolean doored = false;
    private World world; //Reference to world

    private BodyDef.BodyType typeStatic = BodyDef.BodyType.StaticBody;

    public Border(World world){ //Base constructor, creates borders
        this.world = world;
        createWalls();
        createCross();
    }

    public void clear(){ //Public function for destroying cross (debug)
        deactivateCross();
    }

    public void cross(){ //Public function for creating cross (debug)
        activateCross();
    }

    private void activateCross(){
        for (int i = 0; i < cross.length; i++) {
            cross[i].setActive(true);
        }
    } //Sets cross bodies to active
    private void deactivateCross(){
        for (int i = 0; i < cross.length; i++) {
            cross[i].setActive(false);
        }
    } //Sets cross bodies to Inactive

    private void createCross(){
        if(!crossed) {
            cross[0] = BodyFactory.createEdgeBody(250, 250, typeStatic, 500, 0, new Vector2(-1, 250));
            cross[1] = BodyFactory.createEdgeBody(250,250, typeStatic, 0, 500, new Vector2(250, -1));
            crossed = true;
        }
    } //Creates the cross shape that functions as a border between rooms
    private void createWalls(){
        if(!walled) {
            walls[0] = BodyFactory.createEdgeBody(0,250, typeStatic, 0, 500, new Vector2(0, -1));
            walls[1] = BodyFactory.createEdgeBody(250,0, typeStatic, 500, 0, new Vector2(-1, 0));
            walls[2] = BodyFactory.createEdgeBody(500,250, typeStatic, 0, 500, new Vector2(500, -1));
            walls[3] = BodyFactory.createEdgeBody(250,500, typeStatic, 500, 0, new Vector2(-1, 500));
            walled = true;
        }
    } //Creates the four walls acting as the outside border

}

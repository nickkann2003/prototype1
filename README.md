## TO RUN THE PROJECT:

Go into the project folder

Go into runnables folder

Select a version to run


## About this project

This is a project from a year-long independent study done by me, Nicholas Kannenberg, during my senior year of highschool.

The project is entirely java, created using the LibGDX framework. The files in the runnables folder are fully functional standalone downloads for easy testing.

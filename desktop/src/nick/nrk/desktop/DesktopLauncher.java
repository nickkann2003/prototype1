package nick.nrk.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import nick.nrk.FinalPrototype1;


public class DesktopLauncher {
	public static final int w = 750;
	public static final int h = 500;

	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		/*
		* HOW TO EXPORT RUNNABLE
		* In command line, cd into root folder
		* ./gradlew desktop:dist
		* Wait for it to finish
		* Go into desktop>build>libs and the jar will be there
		* Move it to the runnables folder
		* Refactor>Rename
		 */

		config.title = "D150R13NT3D";
		config.width = w;
		config.height = h;
		config.resizable = true;

		// Pack all textures into an Atlas
		// REMOVE CODE BEFORE FINAL RELEASE
		// For development only
		// When game is finished, pre pack final textures into one file
		// to make startup more manageable


/*
		TexturePacker.Settings settings = new TexturePacker.Settings();
		settings.pot = true;
		settings.fast = true;
		settings.combineSubdirectories = true;
		settings.paddingX = 1;
		settings.paddingY = 1;
		settings.edgePadding = true;
		TexturePacker.process(settings, "core/assets/raw_textures", "core/assets", "textures");
*/


		new LwjglApplication(new FinalPrototype1(), config);
	}
}
